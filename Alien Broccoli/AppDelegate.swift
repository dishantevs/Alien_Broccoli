//
//  AppDelegate.swift
//  Alien Broccoli
//
//  Created by Apple on 28/09/20.
//

/*
 status:  0= Not Asign, 1= Assign,  2= On the Way, 3=Delivered, 4= Address or user not available

 0= hai means order ho gaya hai but koi driver assiagn nahi hua hai

 1= Driver assign ho gaya hai  //  driver accept kar dliya hai

 2=  driver deliver dene ke liye chal diya hai

 3= poduct delivered


 
 
 action: followbrand
 userId:
 brandId:
 status:  //1=Follow, 2=Unfollow
 
 
 
 
 action: likebrand
 userId:
 brandId:
 status:  //1=Like, 2=Dislike



 
 
 

 */

import UIKit

import Stripe

import GoogleMaps

import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

var window:UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        
        // stripe
        Stripe.setDefaultPublishableKey("pk_test_IeR8DmaKtT6Gi5W7vvySoCiO")
        
        // P : // AIzaSyBnr4lP43PZlGZkGEC0A7KDXJk14V7IlCA
        // D : // AIzaSyD13JtmhaK08-IfmLLjG_3Wzr6iQZAkO1g
        
        
        // "AIzaSyD13JtmhaK08-IfmLLjG_3Wzr6iQZAkO1g"
        // google
        GMSServices.provideAPIKey("AIzaSyDEYP9nenPV-zJjlCSbPuyf9eYSOfBdKlk")
        // GMSPlacesClient.provideAPIKey("YOUR_API_KEY")
        
        
        // MARK:- REGISTER NOTIFICAITON -
        self.registerForRemoteNotifications(application)
        self.firebaseTokenIs()
        
        return true
    }

    @objc func registerForRemoteNotifications(_ application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            
          // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
            
        } else {
            
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
            
        }

        application.registerForRemoteNotifications()

    }
    
    @objc func firebaseTokenIs() {
        InstanceID.instanceID().instanceID { (result, error) in
          if let error = error {
            print("Error fetching remote instance ID: \(error)")
          } else if let result = result {
            print("Remote instance ID token: \(result.token)")
            
            let defaults = UserDefaults.standard
            defaults.set("\(result.token)", forKey: "deviceFirebaseToken")
            // self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
          }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error = ",error.localizedDescription)
    }
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    
    // MARK:- WHEN APP IS OPEN -
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //print("User Info = ",notification.request.content.userInfo)
        completionHandler([.alert, .badge, .sound])
     
        print("User Info dishu = ",notification.request.content.userInfo)
        let dict = notification.request.content.userInfo
        
        if dict["type"] == nil {
            
        } else {
            
            if dict["type"] as! String == "booking" {
              
                let storyboard = UIStoryboard(name: "Main", bundle: nil)

                let destinationController = storyboard.instantiateViewController(withIdentifier:"DDashbaordId") as? DDashbaord
                destinationController?.dictOfNotificationPopup = notification.request.content.userInfo as NSDictionary
                let frontNavigationController = UINavigationController(rootViewController: destinationController!)

                let rearViewController = storyboard.instantiateViewController(withIdentifier:"MenuControllerVCId") as? MenuControllerVC

                let mainRevealController = SWRevealViewController()

                mainRevealController.rearViewController = rearViewController
                mainRevealController.frontViewController = frontNavigationController
            
            DispatchQueue.main.async {
                UIApplication.shared.keyWindow?.rootViewController = mainRevealController
            }
            
            // window?.rootViewController = mainRevealController
            window?.makeKeyAndVisible()
                
            }
        }
        
        
        
        
        
    }
    
    // MARK:- WHEN APP IS IN BACKGROUND - ( after click popup ) -
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // print("User Info = ",response.notification.request.content.userInfo)
        
    }

}

