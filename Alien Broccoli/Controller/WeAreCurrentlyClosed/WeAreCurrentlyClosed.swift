//
//  WeAreCurrentlyClosed.swift
//  Alien Broccoli
//
//  Created by Apple on 29/09/20.
//

import UIKit

class WeAreCurrentlyClosed: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .systemGreen
    }
}
