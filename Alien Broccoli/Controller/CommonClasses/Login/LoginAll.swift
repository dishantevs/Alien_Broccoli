//
//  LoginAll.swift
//  ExpressPlus
//
//  Created by Apple on 05/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class LoginAll: UIViewController, UITextFieldDelegate {

    var myDeviceTokenIs:String!
    
    var strSelectProfile:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "LOGIN"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.setTitle("|||", for: .normal)
        }
    }
    
    @IBOutlet weak var txtEmail:UITextField! {
        didSet {
            txtEmail.layer.cornerRadius = 6
            txtEmail.clipsToBounds = true
            txtEmail.setLeftPaddingPoints(40)
            txtEmail.layer.borderColor = UIColor.clear.cgColor
            txtEmail.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtPassword:UITextField! {
        didSet {
            txtPassword.layer.cornerRadius = 6
            txtPassword.clipsToBounds = true
            txtPassword.setLeftPaddingPoints(40)
            txtPassword.layer.borderColor = UIColor.clear.cgColor
            txtPassword.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var btnSignUp:UIButton! {
        didSet {
            btnSignUp.setTitleColor(.black, for: .normal)
        }
    }
    @IBOutlet weak var btnForgotPassword:UIButton!
    
    @IBOutlet weak var btnSignInSubmit:UIButton! {
        didSet {
            btnSignInSubmit.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            btnSignInSubmit.layer.cornerRadius = 6
            btnSignInSubmit.clipsToBounds = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.addTarget(self, action: #selector(sideBarMenuClick), for: .touchUpInside)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        btnSignUp.addTarget(self, action: #selector(signUpClickMethod), for: .touchUpInside)
        btnSignInSubmit.addTarget(self, action: #selector(signInValidationCheck), for: .touchUpInside)
        btnForgotPassword.addTarget(self, action: #selector(forgotPasswordClickMethod), for: .touchUpInside)
        
        self.txtEmail.delegate = self
        self.txtPassword.delegate = self
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.remeberMe()
         // self.pushToCompleteprofile()
    }
    
    @objc func remeberMe() {
        
        // MARK:- PUSH -
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        
            if person["ssnImage"] as! String == "" || person["AutoInsurance"] as! String == "" || person["drivlingImage"] as! String == "" {
                    
                    let alert = UIAlertController(title: String("Documents Incomplete"), message: String("Please Complete your document's verification."), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        
                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CUploadDocumentsRegId") as? CUploadDocumentsReg
                        self.navigationController?.pushViewController(settingsVCId!, animated: false)
                        
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                        
                        let defaults = UserDefaults.standard
                        defaults.setValue("", forKey: "keyLoginFullData")
                        defaults.setValue(nil, forKey: "keyLoginFullData")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                                              
                    let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UsernewCategoriesId")
                    self.navigationController?.pushViewController(push, animated: false)
                    
                }
            }
        
        
    }
    
    @objc func pushToCompleteprofile() {
    
        // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDStateCountryCityId") as? PDStateCountryCity
        // self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        /*// MARK:- PUSH -
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                                   
            if person["role"] as! String == "Driver" {
                                       
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DDashbaordId") as? DDashbaord
                self.navigationController?.pushViewController(settingsVCId!, animated: false)
                
            } else {
                
                /*if person["zipCode"] as! String == "" {
                    
                    let alert = UIAlertController(title: String("Location Incomplete"), message: String("Your Location Profile not complete. Please Complete your profile."), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        
                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CAfterRegistrationId") as? CAfterRegistration
                        self.navigationController?.pushViewController(settingsVCId!, animated: false)
                        
                    }))
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                        
                        let defaults = UserDefaults.standard
                        defaults.setValue("", forKey: "keyLoginFullData")
                        defaults.setValue(nil, forKey: "keyLoginFullData")
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                 } else*/ if person["ssnImage"] as! String == "" || person["AutoInsurance"] as! String == "" || person["drivlingImage"] as! String == "" {
                    
                    let alert = UIAlertController(title: String("Documents Incomplete"), message: String("Please Complete your document's verficition."), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        
                        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CUploadDocumentsRegId") as? CUploadDocumentsReg
                        self.navigationController?.pushViewController(settingsVCId!, animated: false)
                        
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                        
                        let defaults = UserDefaults.standard
                        defaults.setValue("", forKey: "keyLoginFullData")
                        defaults.setValue(nil, forKey: "keyLoginFullData")
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    
                    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CDashboardId") as? CDashboard
                    self.navigationController?.pushViewController(settingsVCId!, animated: false)
                    
                }
                
            }
                                       
        }*/
    }
    
    // MARK:- SIGN UP CLICK -
    @objc func signUpClickMethod() {
        // strSelectProfile
        
        if strSelectProfile == "driver" {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RegistrationAllId") as? RegistrationAll
            push!.strPanelIs = "Driver"
            self.navigationController?.pushViewController(push!, animated: true)
            
        } else {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CRegistrationId") as? CRegistration
            push!.strPanelIs = "Member"
            self.navigationController?.pushViewController(push!, animated: true)
        }
        
         
        
        
        
        
    }
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK:- FORGOT PASSWORD -
    @objc func forgotPasswordClickMethod() {
        // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPasswordId") as? ForgotPassword
        // self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- CHECK VALIDATION BEFORE LOGIN -
    @objc func signInValidationCheck() {
        
        
        if self.txtEmail.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Email should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else if self.txtPassword.text! == "" {
            let alert = UIAlertController(title: String("Error!"), message: String("Password should not be Empty."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                 
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            
            self.signInSubmitClickMethod()
        }
        
    }
    
    
    // @objc func signInSubmitClickMethod() {
        
        // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DDashbaordId") as? DDashbaord
        // self.navigationController?.pushViewController(settingsVCId!, animated: true)
    // }
    
    
    // MARK:- WEBSERVICE ( SIGN IN ) -
    @objc func signInSubmitClickMethod() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        
        // Create UserDefaults
        let defaults = UserDefaults.standard
        if let myString = defaults.string(forKey: "deviceFirebaseToken") {
            self.myDeviceTokenIs = myString

        }
        else {
            self.myDeviceTokenIs = "111111111111111111111"
        }
        
        self.view.endEditing(true)
        
        /*
         driver side: developer14deepakgupta@gmail.com
         password deepak123
         both side
         */
        
        /*
         let forgotPasswordP = LoginParam(action: "login",
         email: "dishantrajput38@gmail.com",
         password: "12345678",
         deviceToken: String(self.myDeviceTokenIs),
         device: "iOS")
         */
        
        // mobilex@gmail.com
        // 123456 or 12345678
        
        
        let forgotPasswordP = LoginParam(action: "login",
                                         email: String(txtEmail.text!),//"drios@gmail.com",
                                         password: String(txtPassword.text!),//"12345678",
                                          deviceToken: String(self.myDeviceTokenIs),
                                         device: "iOS")
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: forgotPasswordP,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                          print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var dict: Dictionary<AnyHashable, Any>
                            dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                             let defaults = UserDefaults.standard
                             defaults.setValue(dict, forKey: "keyLoginFullData")
                            
                            
                            // MARK:- PUSH -
                            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
                            
                                if person["role"] as! String == "Driver" {
                                
                                    self.driverDashboardPush()
                                    
                                } else {
                                    
                                    /*if person["zipCode"] as! String == "" {
                                        
                                        let alert = UIAlertController(title: String("Location Incomplete"), message: String("Your Location Profile not complete. Please Complete your profile."), preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                            
                                            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CAfterRegistrationId") as? CAfterRegistration
                                            self.navigationController?.pushViewController(settingsVCId!, animated: false)
                                            
                                        }))
                                        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                                            
                                            let defaults = UserDefaults.standard
                                            defaults.setValue("", forKey: "keyLoginFullData")
                                            defaults.setValue(nil, forKey: "keyLoginFullData")
                                            
                                        }))
                                        self.present(alert, animated: true, completion: nil)
                                        
                                     } else*/ if person["ssnImage"] as! String == "" || person["AutoInsurance"] as! String == "" || person["drivlingImage"] as! String == "" {
                                        
                                        let alert = UIAlertController(title: String("Documents Incomplete"), message: String("Please Complete your document's verification."), preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                            
                                            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CUploadDocumentsRegId") as? CUploadDocumentsReg
                                            self.navigationController?.pushViewController(settingsVCId!, animated: false)
                                            
                                        }))
                                        
                                        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                                            
                                            let defaults = UserDefaults.standard
                                            defaults.setValue("", forKey: "keyLoginFullData")
                                            defaults.setValue(nil, forKey: "keyLoginFullData")
                                            
                                        }))
                                        
                                        self.present(alert, animated: true, completion: nil)
                                        
                                    } else {
                                                                  
                                        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UsernewCategoriesId")
                                        self.navigationController?.pushViewController(push, animated: false)
                                        
                                    }
                                }
                            }
                            
                            // 10,196
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
    
    @objc func driverDashboardPush() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DDashbaordId") as? DDashbaord
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
 
    @objc func userDashboardPush() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CDashboardId") as? CDashboard
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    /*
    @objc func pushScreenWB() {
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
             print(person as Any)
            /*
             ["fullName": ios2020, "AccountHolderName": , "state": , "email": ios2020@gmail.com, "socialType": , "middleName": , "longitude": , "companyBackground": , "BankName": , "lastName": , "latitude": , "AccountNo": , "socialId": , "accountType": , "gender": , "ssnImage": , "role": Member, "country": , "address": Wadala West Mumbai, "deviceToken": , "firebaseId": , "contactNumber": 8287632340, "drivlingImage": , "device": iOS, "dob": , "logitude": , "AutoInsurance": , "userId": 127, "wallet": 0, "image": , "zipCode": , "RoutingNo": , "foodTag": ]
             */
            
            if person["role"] as! String == "Member" {
                
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "UPDashboardId") as? UPDashboard
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
                
            } else if person["role"] as! String == "Driver" {
                
                if person["AccountHolderName"] as! String == "" {
                    let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDCompleteProfileId") as? PDCompleteProfile
                    // settingsVCId!.dictGetRegistrationData = person as NSDictionary
                    self.navigationController?.pushViewController(settingsVCId!, animated: false)
                } else if person["cardholderId"] as! String == "" {
                      let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDStateCountryCityId") as? PDStateCountryCity
                     self.navigationController?.pushViewController(settingsVCId!, animated: false)
                } else {
                     let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                     self.navigationController?.pushViewController(settingsVCId!, animated: false)
                }
                
                // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDDashboardId") as? PDDashboard
                // self.navigationController?.pushViewController(settingsVCId!, animated: true)
                
            } else if person["role"] as! String == "Restaurant" {
                
                let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RHomeId") as? RHome
                self.navigationController?.pushViewController(settingsVCId!, animated: true)
                
            } else {
                
            }
        }
    }
    */
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
