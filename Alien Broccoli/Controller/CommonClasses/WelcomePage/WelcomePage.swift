//
//  WelcomePage.swift
//  Alien Broccoli
//
//  Created by Apple on 29/10/20.
//

import UIKit

class WelcomePage: UIViewController {

    @IBOutlet weak var lblWelcomeMessage:UILabel! {
        didSet {
            lblWelcomeMessage.text = "Hello, Welcome to Alien Broccoli\n Continue as a"
        }
    }
    
    @IBOutlet weak var btnUser:UIButton! {
        didSet {
            btnUser.layer.cornerRadius = 8
            btnUser.clipsToBounds = true
            btnUser.backgroundColor = .white
            btnUser.layer.borderColor = UIColor.lightGray.cgColor
            btnUser.layer.borderWidth = 0.8
            btnUser.setTitleColor(.black, for: .normal)
            btnUser.setTitle("User", for: .normal)
        }
    }
    @IBOutlet weak var btnDriver:UIButton! {
        didSet {
            btnDriver.layer.cornerRadius = 8
            btnDriver.clipsToBounds = true
            btnDriver.backgroundColor = NAVIGATION_BACKGROUND_COLOR//.systemGreen
            btnDriver.setTitleColor(.black, for: .normal)
            btnDriver.setTitle("Driver", for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnUser.addTarget(self, action: #selector(userClickMethod), for: .touchUpInside)
        self.btnDriver.addTarget(self, action: #selector(driverClickMethod), for: .touchUpInside)
        
    }
    
    @objc func userClickMethod() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginAllId") as? LoginAll
        settingsVCId!.strSelectProfile = "user"
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    @objc func driverClickMethod() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginAllId") as? LoginAll
        settingsVCId!.strSelectProfile = "driver"
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
            
            
            
     }
    
    
    
    
    
    
}
