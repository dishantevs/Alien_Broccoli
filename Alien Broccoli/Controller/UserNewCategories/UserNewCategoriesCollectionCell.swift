//
//  UserNewCategoriesCollectionCell.swift
//  Alien Broccoli
//
//  Created by Apple on 29/12/20.
//

import UIKit

class UserNewCategoriesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 16
            viewCellBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel! {
        didSet {
            lblTitle.textColor = .white
        }
    }
    
    @IBOutlet weak var imgImage:UIImageView!
    
}
