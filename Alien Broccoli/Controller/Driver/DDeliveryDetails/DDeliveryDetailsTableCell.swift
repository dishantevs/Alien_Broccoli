//
//  DDeliveryDetailsTableCell.swift
//  Alien Broccoli
//
//  Created by Apple on 30/09/20.
//

import UIKit

class DDeliveryDetailsTableCell: UITableViewCell {

    @IBOutlet weak var viewDateBG:UIView! {
        didSet {
            viewDateBG.layer.borderWidth = 0.8
            viewDateBG.layer.borderColor = UIColor.lightGray.cgColor
            viewDateBG.layer.cornerRadius = 6
            viewDateBG.clipsToBounds = true
            viewDateBG.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var imgProduct:UIImageView! {
        didSet {
            imgProduct.backgroundColor = .brown
            imgProduct.layer.cornerRadius = 6
            imgProduct.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblProductName:UILabel! {
        didSet {
            lblProductName.text = "Full Spectrum CBD Oil 100 mg"
        }
    }
    @IBOutlet weak var lblProductFlavour:UILabel! {
        didSet {
            lblProductFlavour.text = "Flavor : Berry"
        }
    }
    @IBOutlet weak var lblProductQuantity:UILabel! {
        didSet {
            lblProductQuantity.text = "Quantity : 1 | Price : "
        }
    }
    @IBOutlet weak var lblProductPrice:UILabel! {
        didSet {
            lblProductPrice.text = "$ 250"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
