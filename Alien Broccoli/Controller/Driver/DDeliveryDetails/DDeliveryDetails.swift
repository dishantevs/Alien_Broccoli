//
//  DDeliveryDetails.swift
//  Alien Broccoli
//
//  Created by Apple on 30/09/20.
//

import UIKit
import Alamofire

class DDeliveryDetails: UIViewController, UITextViewDelegate {

    // var dictGetRequestDetails:NSDictionary!
    
    let cellReuseIdentifier = "dDeliveryDetailsTableCell"
    
    var productId:String!
    
    var dict: Dictionary<AnyHashable, Any> = [:]
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
       
    var page : Int! = 1
    var loadMore : Int! = 1
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "DELIVERY DETAILS"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = .white
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var btnDelivered:UIButton! {
        didSet {
            btnDelivered.backgroundColor = .systemGreen
            btnDelivered.layer.cornerRadius = 6
            btnDelivered.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnNotDelivered:UIButton! {
        didSet {
            btnNotDelivered.layer.cornerRadius = 6
            btnNotDelivered.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var txtViewComments:UITextView! {
        didSet {
            txtViewComments.text = ""
            txtViewComments.layer.cornerRadius = 6
            txtViewComments.clipsToBounds = true
            txtViewComments.layer.borderColor = UIColor.lightGray.cgColor
            txtViewComments.layer.borderWidth = 0.8
        }
    }
    
    @IBOutlet weak var btnCall:UIButton! {
        didSet {
            btnCall.tintColor = .systemBlue
        }
    }
    
    @IBOutlet weak var imgprofile:UIImageView! {
        didSet {
            imgprofile.backgroundColor = .brown
            imgprofile.layer.cornerRadius = 6
            imgprofile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblShippingName:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // MARK:- DISMISS KEYBOARD WHEN CLICK OUTSIDE -
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        // MARK:- VIEW UP WHEN CLICK ON TEXT FIELD -
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.tbleView.separatorColor = .clear
        // print(dictGetRequestDetails as Any)
        
        /*
         ShippingName = purnima;
         ShippingPhone = 9636965658;
         created = "2020-10-07 20:15:00";
         productDetails =     (
                     {
                 Quantity = 1;
                 price = "80.0";
                 productId = 21;
             }
         );
         purcheseId = 4;
         shippingAddress = "6, Ramprastha Colony, Block C, Chander Nagar, Surya Nagar, Ghaziabad, New";
         shippingCity = Test;
         shippingCountry = "";
         shippingState = New;
         shippingZipcode = 204011;
         status = 1;
         totalAmount = 80;
         transactionId = "";
         */
        
        // imgprofile.sd_setImage(with: URL(string: (dictGetRequestDetails["AutoInsurance"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        /*
        
        
        
        
        // self.btnCall.setTitle((dictGetRequestDetails["ShippingPhone"] as! String), for: .normal)
        
        
        
        
        // arrListOfAllMyOrders
        var ar : NSArray!
        ar = (dictGetRequestDetails["productDetails"] as! Array<Any>) as NSArray
        self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
        
        self.tbleView.delegate = self
        self.tbleView.dataSource = self
        self.tbleView.reloadData()*/
        
        self.btnCall.addTarget(self, action: #selector(callClickMethod), for: .touchUpInside)
        self.btnDelivered.addTarget(self, action: #selector(successDeliveredPopup), for: .touchUpInside)
        
        self.orderHistoryDetailsClickMethod()
    }
    
    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    // MARK:- WEBSERVICE ( ORDER HISTORY DETAILS ) -
    @objc func orderHistoryDetailsClickMethod() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
            
        self.view.endEditing(true)
             
        let params = OrderHistoryDetails(action: "purchedetail",
                                    purcheseId: String(productId))
            
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                             print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                ERProgressHud.sharedInstance.hide()
                               
                                
                                self.dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                
                                self.lblShippingName.text = (self.dict["ShippingName"] as! String)
                                self.lblAddress.text = (self.dict["shippingAddress"] as! String)
                                
                                self.imgprofile.sd_setImage(with: URL(string: (self.dict["UserImage"] as! String)), placeholderImage: UIImage(named: "logo"))
                                
                                var ar : NSArray!
                                ar = (self.dict["productDetails"] as! Array<Any>) as NSArray
                                self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
            }
    
    @objc func callClickMethod() {
        let alert = UIAlertController(title: (self.dict["ShippingName"] as! String), message: (self.dict["ShippingPhone"] as! String), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Call", style: UIAlertAction.Style.default, handler: { action in
             
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: { action in
             
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func sideBarMenuClick() {
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                  
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    
    @objc func successDeliveredPopup() {

        let alert = UIAlertController(title: String("Delivered"), message: String("Successfully Delivered this product to '"+(self.dict["ShippingName"] as! String)+"'"), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Mark as Delivered", style: .default, handler: { action in
            self.successfullyDeliveredWB()
        }))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
         }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func successfullyDeliveredWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        // let x : Int = (productDetailsId!)
         //let myString = String(x)
        
        
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        // let str:String = person["role"] as! String
       
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
            let x2 : Int = self.dict["purcheseId"] as! Int
            let myString2 = String(x2)
            
            let params = SuccessfullyDelivered(action: "changebookingstatus",
                                               driverId: String(myString),
                                               bookingId: String(myString2),
                                               status: "3",
                                               message: String(self.txtViewComments.text!))
        print(params as Any)
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                           print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                         var strSuccess2 : String!
                         strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            let alert = UIAlertController(title: String("Success"), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                           
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
    }
}


extension DDeliveryDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DDeliveryDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! DDeliveryDetailsTableCell
        
        cell.backgroundColor = .white
      
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        cell.lblProductName.text = (item!["productName"] as! String)
        
        if (item!["price"] as! String).contains("$") {
            cell.lblProductPrice.text = "Price : "+(item!["price"] as! String)
        } else {
            cell.lblProductPrice.text = "Price : $ "+(item!["price"] as! String)
        }
        
        cell.lblProductQuantity.text = "Q : "+(item!["Quantity"] as! String)
        
        
        cell.lblProductFlavour.text = "SKU : "+(item!["SKU"] as! String)
        
        
        cell.imgProduct.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        // let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116 // UITableView.automaticDimension
    }
    
}

extension DDeliveryDetails: UITableViewDelegate {
    
}
