//
//  DDashbaord.swift
//  Alien Broccoli
//
//  Created by Apple on 30/09/20.
//

import UIKit
import Alamofire
import SDWebImage

class DDashbaord: UIViewController {

    var dictOfNotificationPopup:NSDictionary!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "DASHBOARD"
        }
    }
    
    @IBOutlet weak var btnMenu:UIButton!
    
    @IBOutlet weak var btnStopSchedule:UIButton! {
        didSet {
            btnStopSchedule.layer.cornerRadius = 4
            btnStopSchedule.clipsToBounds = true
            btnStopSchedule.backgroundColor = UIColor.init(red: 240.0/255.0, green: 210.0/255.0, blue: 70.0/255.0, alpha: 1) // 240, 210, 70
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
    @IBOutlet weak var btnEdit:UIButton!
    @IBOutlet weak var btnGroup:UIButton!
    
    @IBOutlet weak var btnNewRequest:UIButton! {
        didSet {
            btnNewRequest.backgroundColor = APP_BUTTON_COLOR
            btnNewRequest.layer.cornerRadius = 6
            btnNewRequest.clipsToBounds = true
            btnNewRequest.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBOutlet weak var btnDeliveredHistory:UIButton! {
        didSet {
            btnDeliveredHistory.backgroundColor = APP_BUTTON_COLOR
            btnDeliveredHistory.layer.cornerRadius = 6
            btnDeliveredHistory.clipsToBounds = true
            btnDeliveredHistory.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBOutlet weak var imgVieww:UIImageView!
    
    @IBOutlet weak var switchh:UISwitch!
    
    @IBOutlet weak var bottomVieww:UIView! {
        didSet {
            bottomVieww.backgroundColor = .clear
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.sideBarMenuClick()
        
        self.btnGroup.addTarget(self, action: #selector(jobHistoryClickMethod), for: .touchUpInside)
        self.btnEdit.addTarget(self, action: #selector(editClickMethod), for: .touchUpInside)
        self.btnNewRequest.addTarget(self, action: #selector(newRequestClickMethiod), for: .touchUpInside)
        self.btnDeliveredHistory.addTarget(self, action: #selector(bookingHistory), for: .touchUpInside)
        
        /*
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyLoginFullData")
        defaults.setValue(nil, forKey: "keyLoginFullData")
        */
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
            /*
            ["zipCode": , "accountType": , "contactNumber": 9652356238, "image": , "wallet": 0, "TotalCartItem": 0, "email": dishantrajput38@gmail.com, "socialType": , "socialId": , "state": , "BankName": , "device": iOS, "ssnImage": , "latitude": 28.5871097, "middleName": , "RoutingNo": , "userId": 55, "deviceToken": 111111111111111111111, "role": Driver, "AccountNo": , "dob": , "fullName": Mobile Gaming iPhone X, "gender": , "country": , "address": , "AutoInsurance": , "AccountHolderName": , "longitude": , "lastName": , "firebaseId": , "drivlingImage": , "logitude": ]
            (lldb) 
             */
            
            
            self.lblTitle.text      = (person["fullName"] as! String)
            
            imgVieww.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "logo"))
            
        } else {
            // self.plusDriverLogin()
        }
        
        self.switchh.addTarget(self, action: #selector(switchhClickMethod), for: .valueChanged)
        // self.btnStopSchedule.addTarget(self, action: #selector(pushToDashboardPD), for: .touchUpInside)
        
        
        
        // print(dictOfNotificationPopup as Any)
        
        if dictOfNotificationPopup != nil {
            
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PopupViewController") as? PopupViewController
            settingsVCId!.dictnotificationDetails = dictOfNotificationPopup
            self.dictOfNotificationPopup = nil
            settingsVCId!.customBlurEffectStyle = .dark
            settingsVCId!.customAnimationDuration = 1.0 //TimeInterval(animationDurationTextField.text!)
            settingsVCId!.customInitialScaleAmmount = 0.7 // CGFloat(Double(initialScaleAmmountTextField.text!)!) // https://www.youtube.com/watch?v=TH_JRjJtNSw
             self.navigationController?.pushViewController(settingsVCId!, animated: false)
            
        }
    }
    
    @objc func newRequestClickMethiod() {
          let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DDeliveryRequestId") as? DDeliveryRequest
          self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
        
        // self.present(settingsVCId!, animated: true, completion: nil)
    }
    
    @objc func bookingHistory() {
         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DDeliveredHistoryId") as? DDeliveredHistory
         self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    // MARK:- JOB HISTORY -
    @objc func jobHistoryClickMethod() {

        // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDJobRequestId") as? PDJobRequest
        // self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    // MARK:- EDIT -
    @objc func editClickMethod() {
        
         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CEditProfileId") as? CEditProfile
         self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    @objc func switchhClickMethod() {
        
        if switchh.isOn {
            print("on")
            // self.availaibilityOnOrOff(strOnOff: "1")
        } else {
            print("off")
            // self.availaibilityOnOrOff(strOnOff: "0")
        }
        
    }
    
    @objc func sideBarMenuClick() {
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    // MARK:- PUSH TO SPOT SCHEDULING -
    @objc func pushToDashboardPD() {
         // let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PDSpotSchedulingId") as? PDSpotScheduling
         // self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    /*
    // MARK:- WEBSERVICE ( PLUS DRIVER LOGIN ) -
    @objc func availaibilityOnOrOff(strOnOff:String) {
           
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
       
        let urlString = BASE_URL_EXPRESS_PLUS
                  
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! RegistrationTableCell
        
        /*
         [action] => onoff
         [userId] => 123
         [availableStatus] => 0
         */
        
        var parameters:Dictionary<AnyHashable, Any>!
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            let x2 : Int = person["userId"] as! Int
            let driverId = String(x2)
            
            if strOnOff == "0" {
                parameters = [
                          "action"              : "onoff",
                          "userId"              : String(driverId),
                          "availableStatus"     : String("0")
                ]
            } else {
                parameters = [
                          "action"              : "onoff",
                          "userId"              : String(driverId),
                          "availableStatus"     : String("1")
                ]
            }
        }
        print("parameters-------\(String(describing: parameters))")
                      
        Alamofire.request(urlString, method: .post, parameters: parameters as? Parameters).responseJSON {
                        response in
                  
            switch(response.result) {
                              case .success(_):
                                if let data = response.result.value {

                                    let JSON = data as! NSDictionary
                                   print(JSON as Any)
                                  
                                  var strSuccess : String!
                                  strSuccess = JSON["status"]as Any as? String
                                  
                                    // var strSuccessAlert : String!
                                    // strSuccessAlert = JSON["msg"]as Any as? String
                                  
                                  if strSuccess == String("success") {
                                   print("yes")
                                    
                                     ERProgressHud.sharedInstance.hide()
                                    
                                    var strSuccess2 : String!
                                    strSuccess2 = JSON["msg"]as Any as? String
                                    
                                    let alert = UIAlertController(title: String(strSuccess), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
                                        // self.dismiss(animated: true, completion: nil)
                                        
                                        
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    
                                  }
                                  else {
                                   print("no")
                                    ERProgressHud.sharedInstance.hide()
                                   
                                  }
                              }

                              case .failure(_):
                                  print("Error message:\(String(describing: response.result.error))")
                                  
                                  ERProgressHud.sharedInstance.hide()
                                  
                                  let alertController = UIAlertController(title: nil, message: "Server Issue", preferredStyle: .actionSheet)
                                  
                                  let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          NSLog("OK Pressed")
                                      }
                                  
                                  alertController.addAction(okAction)
                                  
                                  self.present(alertController, animated: true, completion: nil)
                                  
                                  break
                        }
        }
    }
    */
    
}
