//
//  DDeliveredTableCell.swift
//  Alien Broccoli
//
//  Created by Apple on 30/09/20.
//

import UIKit

class DDeliveredTableCell: UITableViewCell {

    @IBOutlet weak var viewDateBG:UIView! {
        didSet {
            viewDateBG.layer.cornerRadius = 8
            viewDateBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblDateUp:UILabel!
    @IBOutlet weak var lblDateDown:UILabel!
    
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPhoneNumber:UILabel!
    @IBOutlet weak var lblAddress:UILabel! {
        didSet {
            lblAddress.text = "i am aaddresrxtcfvygbh erxctrvybuh njwerxctvyg bhnj wzexrctrvybun exrctvyb"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
