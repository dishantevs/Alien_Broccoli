//
//  DDeliveryRequest.swift
//  Alien Broccoli
//
//  Created by Apple on 30/09/20.
//

import UIKit
import Alamofire

class DDeliveryRequest: UIViewController {

    let cellReuseIdentifier = "dDeliveryRequestTableCell"
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
       
    
    var page : Int! = 1
    var loadMore : Int! = 1
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "DELIVERY REQUEST"
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            // btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.sideBarMenuClick()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.deliveryRequestWB()
    }
    
    @objc func sideBarMenuClick() {
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                  
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
    @objc func deliveryRequestWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        // let x : Int = (productDetailsId!)
         //let myString = String(x)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
        // let str:String = person["role"] as! String
       
           let x : Int = person["userId"] as! Int
           let myString = String(x)
            
        let params = DriverPuchaseList(action: "purcheslist",
                                       userId: String(myString),
                                       userType: "Driver",
                                       status: "1")
        print(params as Any)
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                          print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // self.totalItemsInCart()
                            
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
    }
    
    
    
    
    
    
    
    
    
    
}


extension DDeliveryRequest: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DDeliveryRequestTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! DDeliveryRequestTableCell
        
        cell.backgroundColor = .white
      
         let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
       
        cell.lblName.text = (item!["ShippingName"] as! String)
        cell.lblPhoneNumber.text = (item!["ShippingPhone"] as! String)
        cell.lblAddress.text = (item!["shippingAddress"] as! String)
        
        
        
        let fullNameArr = (item!["created"] as! String).components(separatedBy: "-")

        // let expMonth    = fullNameArr[0]
        let dateDown = fullNameArr[1]
        let dateUp = fullNameArr[2]
        
        // print(expMonth as Any)
        // print(dateDown as Any)
        // print(dateUp as Any)
        
        let removeSpaceFromDate = dateUp.components(separatedBy: " ")
        let finalDateUpIs    = removeSpaceFromDate[0]
        cell.lblDateUp.text = String(finalDateUpIs)
        
        
        if String(dateDown) == "1" || String(dateDown) == "01"  {
            
            cell.lblDateDown.text = "JAN"
            
        } else if String(dateDown) == "2"  || String(dateDown) == "02" {
            
            cell.lblDateDown.text = "FEB"
            
        } else if String(dateDown) == "3" || String(dateDown) == "03" {
            
            cell.lblDateDown.text = "MAR"
            
        } else if String(dateDown) == "4" || String(dateDown) == "04" {
            
            cell.lblDateDown.text = "APR"
            
        } else if String(dateDown) == "5" || String(dateDown) == "05" {
            
            cell.lblDateDown.text = "MAY"
            
        } else if String(dateDown) == "6" || String(dateDown) == "06" {
            
            cell.lblDateDown.text = "JUN"
            
        } else if String(dateDown) == "7" || String(dateDown) == "07" {
            
            cell.lblDateDown.text = "JUL"
            
        } else if String(dateDown) == "8" || String(dateDown) == "08" {
            
            cell.lblDateDown.text = "AUG"
            
        } else if String(dateDown) == "9" || String(dateDown) == "09" {
            
            cell.lblDateDown.text = "SEP"
            
        } else if String(dateDown) == "10" {
            
            cell.lblDateDown.text = "OCT"
            
        } else if String(dateDown) == "11" {
            
            cell.lblDateDown.text = "NOV"
            
        } else if String(dateDown) == "12" {
            
            cell.lblDateDown.text = "DEC"
            
        }
        
        
        
        
        
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        let x : Int = (item!["purcheseId"] as! Int)
        let myString = String(x)
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DDeliveryDetailsId") as? DDeliveryDetails
        settingsVCId!.productId = String(myString)
        // settingsVCId!.dictGetRequestDetails = item as NSDictionary?
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension DDeliveryRequest: UITableViewDelegate {
    
}
