//
//  StoreAllDetails.swift
//  Alien Broccoli
//
//  Created by apple on 18/06/21.
//

import UIKit
import Alamofire
import SDWebImage
import GoogleMaps

class StoreAllDetails: UIViewController , GMSMapViewDelegate {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .black
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "DETAILS"
                lblNavigationTitle.textColor = .black
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    // MARK:- ARRAY -
    var arrListOfAllBrands:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var detailsArray = ["Menu" , "Details" , "Deals" , "Reviews" , "Media"]
    var detailsArrayImage = ["dmenu" , "ddetails" , "ddeal" , "dReview" , "dmedia"]
    
    var dictGetStoreAllDetails:NSDictionary!
    
    var dict: Dictionary<AnyHashable, Any> = [:]
    
    var openTimeStringIs:String!
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.clipsToBounds = true
            viewCellBG.layer.masksToBounds = false
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.layer.backgroundColor = UIColor.white.cgColor
            viewCellBG.layer.borderColor = UIColor.clear.cgColor
            viewCellBG.layer.shadowColor = UIColor.black.cgColor
            viewCellBG.layer.shadowOffset = CGSize(width: 0, height: 0)
            viewCellBG.layer.shadowOpacity = 0.4
            viewCellBG.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 8
            imgProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var viewOnlyBlack:UIView! {
        didSet {
            // viewOnlyBlack.backgroundColor = .black
            
            let gradientLayer:CAGradientLayer = CAGradientLayer()
            gradientLayer.frame.size = viewOnlyBlack.frame.size
            gradientLayer.colors =
                [UIColor.white.cgColor,UIColor.black.withAlphaComponent(1).cgColor]
              //Use diffrent colors
            viewOnlyBlack.layer.addSublayer(gradientLayer)
            
        }
    }
    
    @IBOutlet weak var viewDataSetup:UIView! {
        didSet {
            viewDataSetup.backgroundColor = .white
            
        }
    }
    @IBOutlet weak var btnStarOne:UIButton! {
        didSet {
            btnStarOne.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarTwo:UIButton! {
        didSet {
            btnStarTwo.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarThree:UIButton! {
        didSet {
            btnStarThree.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarFour:UIButton! {
        didSet {
            btnStarFour.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarFive:UIButton! {
        didSet {
            btnStarFive.tintColor = .systemYellow
            
        }
    }
    
    @IBOutlet weak var lblShopName:UILabel! {
        didSet {
            lblShopName.textColor = .black
            lblShopName.backgroundColor = .clear
        }
    }
    @IBOutlet weak var btnPhoneNumber:UIButton! {
        didSet {
            btnPhoneNumber.setTitleColor(.black, for: .normal)
            btnPhoneNumber.backgroundColor = .clear
        }
    }
    @IBOutlet weak var lblAddress:UILabel! {
        didSet {
            lblAddress.textColor = .black
            lblAddress.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var lblDistance:UILabel! {
        didSet {
            lblDistance.textColor = .white
            lblDistance.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            lblDistance.text = " 1.7km "
            lblDistance.layer.cornerRadius = 4
            lblDistance.clipsToBounds = true
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    var arrListOfALLImages:NSMutableArray! = []
    
    @IBOutlet weak var scrollViewBanner:UIScrollView! {
        didSet {
            scrollViewBanner.backgroundColor = .systemTeal
        }
    }
    
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        self.screenSize = UIScreen.main.bounds
        self.screenWidth = screenSize.width
        self.screenHeight = screenSize.height
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        // print(self.dictGetStoreAllDetails as Any)
        
        /*
         OpenNow = "15:30-15:30";
         address = "New MG Road";
         id = 5;
         "image_1" = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624018968_great-ladies-fashion-garment-shop-interior-display-furniture-5.jpg";
         "image_2" = "";
         "image_3" = "";
         "image_4" = "";
         "image_5" = "";
         "image_6" = "";
         latitude = "32.110859";
         likeme = 1;
         logo = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624018775_retail-clothing-store-207.jpg";
         longitude = "76.536255";
         name = "Multi Store";
         phone = 989864321;
         rating = 3;
         zipcode = 201301;
         */
        
        self.gradientNavigation()
        
        self.getAndParseAllImages()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.gradientNavigation()
    }
    
    @objc func getAndParseAllImages() {
        //arrListOfALLImages
        
        for index in 0...6 {
            
            if index == 0 {
                
                if (self.dictGetStoreAllDetails["logo"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetStoreAllDetails["logo"] as! String))
                }
                
            } else if index == 1 {
                
                if (self.dictGetStoreAllDetails["image_1"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetStoreAllDetails["image_1"] as! String))
                }
                
            } else if index == 2 {
                
                if (self.dictGetStoreAllDetails["image_2"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetStoreAllDetails["image_2"] as! String))
                }
                
            } else if index == 3 {
                
                if (self.dictGetStoreAllDetails["image_3"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetStoreAllDetails["image_3"] as! String))
                }
                
            } else if index == 4 {
                
                if (self.dictGetStoreAllDetails["image_4"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetStoreAllDetails["image_4"] as! String))
                }
                
            } else if index == 5 {
                
                if (self.dictGetStoreAllDetails["image_5"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetStoreAllDetails["image_5"] as! String))
                }
                
            } else if index == 6 {
                
                if (self.dictGetStoreAllDetails["image_6"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetStoreAllDetails["image_6"] as! String))
                }
                
            }
            
        }
        
        // print(self.arrListOfALLImages.count as Any)
        // print(self.arrListOfALLImages as Any)
        
        self.putAllImagesOnScrollView()
        
    }
    
    @objc func putAllImagesOnScrollView() {
        
        self.scrollViewBanner.frame =  CGRect(x: 0, y: 0, width:self.viewCellBG.frame.size.width , height: self.viewCellBG.frame.size.height)
        self.scrollViewBanner.backgroundColor = .systemTeal
        self.scrollViewBanner.showsHorizontalScrollIndicator = false
        self.scrollViewBanner.isPagingEnabled = true
        self.viewCellBG.addSubview(self.scrollViewBanner)
        
        for index in 0..<self.arrListOfALLImages.count {
            
            frame.origin.x = self.scrollViewBanner.frame.size.width * CGFloat(index)
            frame.size = self.scrollViewBanner.frame.size
            
            let imageView = UIImageView()
            
            // imageView.image = UIImage(named: self.arrListOfALLImages[index] as! String)
            imageView.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            imageView.sd_setImage(with: URL(string: self.arrListOfALLImages[index] as! String), placeholderImage: UIImage(named: "logo"))
            
            imageView.frame = frame
            imageView.backgroundColor = .yellow
            
            self.scrollViewBanner.contentSize.width = self.scrollViewBanner.frame.width * CGFloat(index + 1)
            self.scrollViewBanner.addSubview(imageView)
            
        }
        
        // self.scrollViewBanner.contentSize = CGSize(width: self.scrollViewBanner.frame.size.width * CGFloat(self.arrAddQuestions.count), height: self.scrollViewBanner.frame.size.height)
        self.scrollViewBanner.delegate = self
        
        self.parseDataOfThatDispensaries()
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        print(Int(self.scrollViewBanner.contentOffset.x / CGFloat(4)))
    }
    
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
        
    }
    
    @objc func parseDataOfThatDispensaries() {
    
        /*self.lblShopName.text = (dictGetStoreAllDetails!["name"] as! String)
        self.lblAddress.text = (dictGetStoreAllDetails!["address"] as! String)
        
        if (dictGetStoreAllDetails!["phone"] as! String) == "" {
            self.btnPhoneNumber.setTitle("", for: .normal)
        } else {
            self.btnPhoneNumber.setTitle((dictGetStoreAllDetails!["phone"] as! String), for: .normal)
        }
        
        
        self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        self.imgProfile.sd_setImage(with: URL(string: (dictGetStoreAllDetails!["logo"] as! String)), placeholderImage: UIImage(named: "logo"))
        */
        if (dictGetStoreAllDetails!["rating"] as! String) == "0" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreAllDetails!["rating"] as! String) == "1" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreAllDetails!["rating"] as! String) == "2" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreAllDetails!["rating"] as! String) == "3" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreAllDetails!["rating"] as! String) == "4" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreAllDetails!["rating"] as! String) == "5" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star.fill"), for: .normal)
            
        } else {
            
            self.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        }
        
        self.ShopAllTimee()
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.white.cgColor
        let colorBottom = UIColor.black.cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
        self.imgProfile.layer.insertSublayer(gradientLayer, at:0)
    }
    
    @objc func storeDetailsList() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        let x2 : Int = (self.dictGetStoreAllDetails["id"] as! Int)
        let myString2 = String(x2)
        
        let params = StoreDetails(action: "storedetails",
                                  storeId:String(myString2))
        
        print(params as Any)
            
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                    switch response.result {
                    case let .success(value):
                            
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                            
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                               
                            self.dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                   
                                /*var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllStoresList.addObjects(from: ar as! [Any])*/
                                
                            
    
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                                
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                                
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                        }
                            
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                            
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
    }
    
    
    
    @objc func ShopAllTimee() {
        
        /*
         Fri = 1;
         FriTime = 0;
         Mon = 1;
         MonTime = 0;
         OpenNow = "15:30-23:30";
         Sat = 1;
         SatTime = 0;
         Sun = 1;
         SunTime = 0;
         Thu = 1;
         ThuTime = 0;
         Tue = 1;
         TueTime = 0;
         Wed = 1;
         WedTime = 0;
         address = "New west area";
         id = 4;
         "image_1" = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624272115_Online-Medical-Store-Baranagar.jpg";
         "image_2" = "";
         "image_3" = "";
         "image_4" = "";
         "image_5" = "";
         "image_6" = "";
         latitude = "25.957800";
         likeme = 1;
         logo = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624272115_Online-Medical-Store-Baranagar.jpg";
         longitude = "80.149803";
         name = "Medical Store";
         phone = 8934567876;
         pickup = 0;
         rating = 3;
         zipcode = 201301;
         */
        
        // monday
        var mondayString:String!
        
        if self.dictGetStoreAllDetails["Mon"] is String {
            
            mondayString = (self.dictGetStoreAllDetails["Mon"] as! String)
            
        } else if self.dictGetStoreAllDetails["Mon"] is Int {
          
            let x2 : Int = (self.dictGetStoreAllDetails["Mon"] as! Int)
            let myString2 = String(x2)
            mondayString = myString2
            
        } else {
       
            let temp:NSNumber = self.dictGetStoreAllDetails["Mon"] as! NSNumber
            let tempString = temp.stringValue
            mondayString = tempString
          
        }
        
        // print(mondayString as Any)
        
        if mondayString == "1" {
            mondayString = "Monday : "+(self.dictGetStoreAllDetails["MonTime"] as! String)
        } else {
            mondayString = "Monday : Close"
        }
        
        // tuesday
        var tuesdayString:String!
        
        if self.dictGetStoreAllDetails["Tue"] is String {
            
            tuesdayString = (self.dictGetStoreAllDetails["Tue"] as! String)
            
        } else if self.dictGetStoreAllDetails["Tue"] is Int {
          
            let x2 : Int = (self.dictGetStoreAllDetails["Tue"] as! Int)
            let myString2 = String(x2)
            tuesdayString = myString2
            
        } else {
       
            let temp:NSNumber = self.dictGetStoreAllDetails["Tue"] as! NSNumber
            let tempString = temp.stringValue
            tuesdayString = tempString
          
        }
        
        // print(tuesdayString as Any)
        
        if tuesdayString == "1" {
            tuesdayString = "Tuesday : "+(self.dictGetStoreAllDetails["TueTime"] as! String)
        } else {
            tuesdayString = "Tuesday : Close"
        }
        
        
        
        // wednesday
        var wednesdayString:String!
        
        if self.dictGetStoreAllDetails["Wed"] is String {
            
            wednesdayString = (self.dictGetStoreAllDetails["Wed"] as! String)
            
        } else if self.dictGetStoreAllDetails["Wed"] is Int {
          
            let x2 : Int = (self.dictGetStoreAllDetails["Wed"] as! Int)
            let myString2 = String(x2)
            wednesdayString = myString2
            
        } else {
       
            let temp:NSNumber = self.dictGetStoreAllDetails["Wed"] as! NSNumber
            let tempString = temp.stringValue
            wednesdayString = tempString
          
        }
        
        // print(wednesdayString as Any)
        
        if wednesdayString == "1" {
            wednesdayString = "Wednesday : "+(self.dictGetStoreAllDetails["WedTime"] as! String)
        } else {
            wednesdayString = "Wednesday : Close"
        }
        
        
        // thursday
        var thursdayString:String!
        
        if self.dictGetStoreAllDetails["Thu"] is String {
            
            thursdayString = (self.dictGetStoreAllDetails["Thu"] as! String)
            
        } else if self.dictGetStoreAllDetails["Thu"] is Int {
          
            let x2 : Int = (self.dictGetStoreAllDetails["Thu"] as! Int)
            let myString2 = String(x2)
            thursdayString = myString2
            
        } else {
       
            let temp:NSNumber = self.dictGetStoreAllDetails["Thu"] as! NSNumber
            let tempString = temp.stringValue
            thursdayString = tempString
          
        }
        
        // print(thursdayString as Any)
        
        if thursdayString == "1" {
            thursdayString = "Thursday : "+(self.dictGetStoreAllDetails["ThuTime"] as! String)
        } else {
            thursdayString = "Thursday : Close"
        }
        
        
        // friday
        var fridayString:String!
        
        if self.dictGetStoreAllDetails["Fri"] is String {
            
            fridayString = (self.dictGetStoreAllDetails["Fri"] as! String)
            
        } else if self.dictGetStoreAllDetails["Fri"] is Int {
          
            let x2 : Int = (self.dictGetStoreAllDetails["Fri"] as! Int)
            let myString2 = String(x2)
            fridayString = myString2
            
        } else {
       
            let temp:NSNumber = self.dictGetStoreAllDetails["Fri"] as! NSNumber
            let tempString = temp.stringValue
            fridayString = tempString
          
        }
        
        // print(fridayString as Any)
        
        if fridayString == "1" {
            fridayString = "Friday : "+(self.dictGetStoreAllDetails["FriTime"] as! String)
        } else {
            fridayString = "Friday : Close"
        }
        
        
        
        // saturday
        var saturdayString:String!
        
        if self.dictGetStoreAllDetails["Sat"] is String {
            
            saturdayString = (self.dictGetStoreAllDetails["Sat"] as! String)
            
        } else if self.dictGetStoreAllDetails["Sat"] is Int {
          
            let x2 : Int = (self.dictGetStoreAllDetails["Sat"] as! Int)
            let myString2 = String(x2)
            saturdayString = myString2
            
        } else {
       
            let temp:NSNumber = self.dictGetStoreAllDetails["Sat"] as! NSNumber
            let tempString = temp.stringValue
            saturdayString = tempString
          
        }
        
        // print(saturdayString as Any)
        
        if saturdayString == "1" {
            saturdayString = "Saturday : "+(self.dictGetStoreAllDetails["SatTime"] as! String)
        } else {
            saturdayString = "Saturday : Close"
        }
        
        
        
        // sunday
        var sundayString:String!
        
        if self.dictGetStoreAllDetails["Sun"] is String {
            
            sundayString = (self.dictGetStoreAllDetails["Sun"] as! String)
            
        } else if self.dictGetStoreAllDetails["Sun"] is Int {
          
            let x2 : Int = (self.dictGetStoreAllDetails["Sun"] as! Int)
            let myString2 = String(x2)
            sundayString = myString2
            
        } else {
       
            let temp:NSNumber = self.dictGetStoreAllDetails["Sun"] as! NSNumber
            let tempString = temp.stringValue
            sundayString = tempString
          
        }
        
        // print(sundayString as Any)
        
        if sundayString == "1" {
            sundayString = "Sunday : "+(self.dictGetStoreAllDetails["SunTime"] as! String)
        } else {
            sundayString = "Sunday : Close"
        }
        
        let addWeekday = String(mondayString)+"\n"+String(tuesdayString)+"\n"+String(wednesdayString)+"\n"+String(thursdayString)+"\n"+String(fridayString)+"\n"+String(saturdayString)+"\n"+String(sundayString)
        
        self.openTimeStringIs = String(addWeekday)
        
        self.tbleView.delegate = self
        self.tbleView.dataSource = self
        self.tbleView.reloadData()
        
    }
}


extension StoreAllDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:StoreAllDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: "storeAllDetailsTableCell") as! StoreAllDetailsTableCell
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        // cell.lblTitle.text = self.detailsArray[indexPath.row]
        
        /*
         OpenNow = "15:30-15:30";
         address = "New MG Road";
         id = 5;
         "image_1" = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624018968_great-ladies-fashion-garment-shop-interior-display-furniture-5.jpg";
         "image_2" = "";
         "image_3" = "";
         "image_4" = "";
         "image_5" = "";
         "image_6" = "";
         latitude = "32.110859";
         likeme = 1;
         logo = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624018775_retail-clothing-store-207.jpg";
         longitude = "76.536255";
         name = "Multi Store";
         phone = 989864321;
         rating = 3;
         zipcode = 201301;
         */
        
        if indexPath.row == 0 {
            
            cell.lblTitle.text = "Name"
            cell.lblSubTitle.text = (self.dictGetStoreAllDetails["name"] as! String)
            cell.btnSystem.isHidden = true
            cell.lblSubTitle.isHidden = false
            
        } else if indexPath.row == 1 {
            
            cell.lblTitle.text = "Phone"
            cell.lblSubTitle.text = (self.dictGetStoreAllDetails["phone"] as! String)
            cell.btnSystem.isHidden = false
            cell.btnSystem.setImage(UIImage(systemName: "phone"), for: .normal)
            cell.btnSystem.tintColor = NAVIGATION_BACKGROUND_COLOR
            cell.lblSubTitle.isHidden = false
            cell.btnSystem.addTarget(self, action: #selector(callMethodClick), for: .touchUpInside)
            
        } else if indexPath.row == 2 {
            
            cell.lblTitle.text = "Address"
            cell.lblSubTitle.text = (self.dictGetStoreAllDetails["address"] as! String)
            cell.btnSystem.isHidden = true
            cell.lblSubTitle.isHidden = false
            
        } else if indexPath.row == 3 {
            
            // (self.dictGetStoreAllDetails["OpenNow"] as! String)
            
            cell.lblTitle.text = "Open Time"
            cell.lblSubTitle.text = "\n"+self.openTimeStringIs
            cell.btnSystem.isHidden = true
            cell.lblSubTitle.isHidden = false
            
        } else if indexPath.row == 4 {
            
            cell.lblTitle.text = "Location"
            cell.lblSubTitle.isHidden = false
            cell.lblSubTitle.text = "address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address address "
            let camera = GMSCameraPosition.camera(withLatitude: 28.7041, longitude: 77.1025, zoom: 2.0)
            let mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 30, width: cell.viewBG.frame.size.width, height: 300-30), camera: camera)
            mapView.delegate = self
            
            cell.viewBG.addSubview(mapView)
            cell.btnSystem.isHidden = true
            
            let lat = (self.dictGetStoreAllDetails["latitude"] as! String)
            let myFloatLat = (lat as NSString).doubleValue
            
            let long = (self.dictGetStoreAllDetails["longitude"] as! String)
            let myFloatLong = (long as NSString).doubleValue
            
            // Creates a marker in the center of the map.
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: myFloatLat, longitude: myFloatLong)
            marker.title = (self.dictGetStoreAllDetails["name"] as! String)
            marker.snippet = ""
                 // marker.isTappable = true
            // marker.snippet = "Australia"
            marker.map = mapView
            
        } else {
            
        }
        
        cell.backgroundColor = .white
        
        return cell
        
    }

    @objc func callMethodClick() {
        
        if let url = URL(string: "tel://\((self.dictGetStoreAllDetails["phone"] as! String) )"),
           UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        /*if indexPath.row == 0 {
            //
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreProductsId") as? StoreProducts
            push!.dictGetCanabbiesItemDetails = self.dict as NSDictionary
            self.navigationController?.pushViewController(push!, animated: true)
        }*/
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        /*if indexPath.row == 3 {
            return 300
        } else {*/
            return UITableView.automaticDimension
        // }
    }
    
}

extension StoreAllDetails: UITableViewDelegate {
    
}
