//
//  StoreDealsProduct.swift
//  Alien Broccoli
//
//  Created by apple on 21/06/21.
//

import UIKit
import Alamofire
import SDWebImage

class StoreDealsProduct: UIViewController {
    
    var dashboardTitle = ["CBD Oil","CBD Gummies","CBD Capsules","CBD Topicals","CBD Candy","CBD Edibles"]
    var dashboardImage = ["1","2","3","4","5","6"]
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var headerName:String!
    var brandIdIs:String!
    var dictGetCanabbiesItemDetails:NSDictionary!
    
    @IBOutlet weak var lblCartCount:UILabel! {
        didSet {
            self.lblCartCount.isHidden = true
        }
    }
    
    @IBOutlet weak var indicatorr:UIActivityIndicatorView! {
        didSet {
            indicatorr.color = .white
        }
    }
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SHOP NOW"
        }
    }
    
    // MARK:- COLLECTION VIEW SETUP -
    @IBOutlet weak var clView: UICollectionView! {
        didSet {
               //collection
            // clView!.dataSource = self
            // clView!.delegate = self
            clView!.backgroundColor = .white
            clView.isPagingEnabled = false
        }
    }
    
    @IBOutlet weak var btnMenu:UIButton! {
        didSet {
            btnMenu.tintColor = .black
        }
    }
    
    @IBOutlet weak var btnCart:UIButton! {
        didSet {
            btnCart.backgroundColor = .clear
            btnCart.tintColor = .black
            btnCart.isHidden = true
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnMenu.addTarget(self, action: #selector(sidebackBarMenuClick), for: .touchUpInside)
        self.btnCart.addTarget(self, action: #selector(cartClickMwthod), for: .touchUpInside)
        
        
        
        // print(self.dictGetCanabbiesItemDetails as Any)
        
        /*
         OpenNow = "10:00-20:00";
         address = "Sector-11";
         id = 7;
         "image_1" = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624016805_POS.png";
         "image_2" = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624016805_3.jpg";
         "image_3" = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624016805_2.jpg";
         "image_4" = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624016805_Cake.jpg";
         "image_5" = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624016805_toornament-api-logo.png";
         "image_6" = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624016805_1_1.jpg";
         latitude = "28.775461";
         likeme = 1;
         logo = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624016805_111.png";
         longitude = "72.325681";
         name = "Book Store";
         phone = 7894563210;
         rating = 3;
         zipcode = 201301;
         */
        // self.sideBarMenuClick()
        
        self.lblNavigationTitle.text = (self.dictGetCanabbiesItemDetails["name"] as! String)
        self.lblNavigationTitle.textColor = .black
        
        self.ProductListWB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.gradientNavigation()
        
    }
    
    
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
        
    }
    
    @objc func cartClickMwthod() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CTotalCartListId") as? CTotalCartList
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    @objc func sidebackBarMenuClick() {
        
        self.navigationController?.popViewController(animated: true)
        
        /*
        if revealViewController() != nil {
            btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
               
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        */
    }
    
}


extension StoreDealsProduct: UICollectionViewDelegate {
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    //Write Delegate Code Here
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "storeDealsProductCollectionCell", for: indexPath as IndexPath) as! StoreDealsProductCollectionCell
           
        /*
         SKU = i30933;
         cateroryName = Cloth;
         deal = 1;
         description = "";
         followers = "";
         image = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/products/1621861484_1.jpg";
         price = 21;
         productId = 32;
         productName = "Legend Leggings black";
         quantity = 23;
         rating = 3;
         specialPrice = 0;
         */
        
        // MARK:- CELL CLASS -
        cell.layer.cornerRadius = 6
        cell.clipsToBounds = true
        cell.backgroundColor = .white
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.borderWidth = 0.6
        
        // cell.viewCellBG.backgroundColor = .systemGreen // APP_BUTTON_COLOR
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        print(item as Any)
        
        cell.lblTitle.text  = (item!["productName"] as! String)
        
        
        
        if item!["price"] is String {
                          
            print("Yes, it's a String")
          
            if (item!["price"] as! String) == "" {
                
                cell.lblSubTitle.text = "$0"
                
            } else if (item!["price"] as! String) == "0" {
                
                cell.lblSubTitle.text = "$0"
                
            } else {
                
                cell.lblSubTitle.text = "$"+(item!["price"] as! String)
                
            }

        } else if item!["price"] is Int {
          
            print("It is Integer")
          
            let x2 : Int = (item!["price"] as! Int)
            let myString2 = String(x2)
            
            if myString2 == "" {
                
                cell.lblSubTitle.text = "$0"
                
            } else if myString2 == "0" {
                
                cell.lblSubTitle.text = "$0"
                
            } else {
                
                cell.lblSubTitle.text = "$"+myString2
                
            }
          
        } else {
        //some other check
          print("i am ")
          
            let temp:NSNumber = item!["price"] as! NSNumber
            let tempString = temp.stringValue
            
            if tempString == "" {
                
                cell.lblSubTitle.text = "$0"
                
            } else if tempString == "0" {
                
                cell.lblSubTitle.text = "$0"
                
            } else {
                
                cell.lblSubTitle.text = "$"+tempString
                
            }
          
        }
        
        // SPECIAL PRICE
        if item!["specialPrice"] is String {
                          
            print("Yes, it's a String")
          
            if (item!["specialPrice"] as! String) == "" {
                
                cell.lblSpecialPrice.text = "$0"
                
            } else if (item!["specialPrice"] as! String) == "0" {
                
                cell.lblSpecialPrice.text = "$0"
                
            } else {
                
                cell.lblSpecialPrice.text = "$"+(item!["specialPrice"] as! String)
                
            }

        } else if item!["specialPrice"] is Int {
          
            print("It is Integer")
          
            let x2 : Int = (item!["specialPrice"] as! Int)
            let myString2 = String(x2)
            
            if myString2 == "" {
                
                cell.lblSpecialPrice.text = "$0"
                
            } else if myString2 == "0" {
                
                cell.lblSpecialPrice.text = "$0"
                
            } else {
                
                cell.lblSpecialPrice.text = "$"+myString2
                
            }
          
        } else {
        //some other check
          print("i am ")
          
            let temp:NSNumber = item!["specialPrice"] as! NSNumber
            let tempString = temp.stringValue
            
            if tempString == "" {
                
                cell.lblSpecialPrice.text = "$0"
                
            } else if tempString == "0" {
                
                cell.lblSpecialPrice.text = "$0"
                
            } else {
                
                cell.lblSpecialPrice.text = "$"+tempString
                
            }
          
        }
        
        cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgProfile.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "logo"))
        cell.imgProfile.backgroundColor = .clear
        
        cell.lblTitle.textColor = .lightGray
       
        cell.lblSubTitle.font = UIFont.init(name: "Avenir Next Bold", size: 22)
        cell.lblSubTitle.textColor = .systemRed
        cell.lblSubTitle.textAlignment = .center
        
        cell.lblSpecialPrice.font = UIFont.init(name: "Avenir Next Bold", size: 22)
        cell.lblSpecialPrice.textColor = .black
        cell.lblSpecialPrice.textAlignment = .center
        
        cell.viewBlackBG.backgroundColor = .white
        cell.viewBlackBG.layer.borderWidth = 0.8
        cell.viewBlackBG.layer.borderColor = UIColor.white.cgColor
        
        return cell
    }
    
    // MARK:- WEBSERVICE ( CATEGORIES ) -
    @objc func ProductListWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        
        let x : Int = (dictGetCanabbiesItemDetails["id"] as! Int)
        let myString = String(x)
        
        let params = DispensariesStoresProduct(action: "productlist",
                                               storeId: String(myString),
                                               deal: String("1"),
                                               pageNo: String("0"))
        print(params as Any)
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            // ERProgressHud.sharedInstance.hide()
                           
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // self.indicatorr.stopAnimating()
                            // self.indicatorr.hidesWhenStopped = true

                            if self.arrListOfAllMyOrders.count == 0 {
                                ERProgressHud.sharedInstance.hide()
                                
                                let alert = UIAlertController(title: String("No Deals Found"), message: nil, preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                
                            } else {
                                
                                self.totalItemsInCart()
                                
                            }
                             
                            /*let x : Int = JSON["TotalCartItem"] as! Int
                            let myString = String(x)
                            
                            if myString == "0" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else if myString == "" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else {
                                self.lblCartCount.isHidden = false
                                self.lblCartCount.text = String(myString)
                                
                                self.btnCart.isHidden = false
                                self.btnCart.setImage(UIImage(systemName: "cart.fill"), for: .normal)
                            }
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            self.clView.delegate = self
                            self.clView.dataSource = self
                            self.clView.reloadData()*/
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
        
    }
    
    
    
    // MARK:- WEBSERVICE ( TOTAL ITEMS IN CART ) -
    @objc func totalItemsInCart() {
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
            
        let params = CartList(action: "getcarts",
                              userId: String(myString))
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            // var ar : NSArray!
                            // ar = (JSON["data"] as! Array<Any>) as NSArray
                            // self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // var strCartCount : String!
                            // strCartCount = JSON["TotalCartItem"]as Any as? String
                            // print(strCartCount as Any)
                            
                            // cart
                            
                            let x : Int = JSON["TotalCartItem"] as! Int
                            let myString = String(x)
                            
                            if myString == "0" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else if myString == "" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else {
                                self.lblCartCount.isHidden = false
                                self.lblCartCount.text = String(myString)
                                
                                self.btnCart.isHidden = false
                                self.btnCart.setImage(UIImage(systemName: "cart.fill"), for: .normal)
                            }
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            self.clView.delegate = self
                            self.clView.dataSource = self
                            self.clView.reloadData()
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        self.indicatorr.stopAnimating()
                        self.indicatorr.hidesWhenStopped = true
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
         }
    }
    
}

extension StoreDealsProduct: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        // print(item as Any)
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CSelectedOrderDetailsId") as? CSelectedOrderDetails
        push!.dictGetProductDetails = item as NSDictionary?
        push!.productCategoryName = (item!["productName"] as! String)
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- DASHBOARD PUSH -
    @objc func ordersListingPage() {
         let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "COrderDetailsId") as? COrderDetails
         self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- DISMISS KEYBOARD -
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.clView {
            self.view.endEditing(true)
        }
    }
    
}

extension StoreDealsProduct: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var sizes: CGSize
        sizes = CGSize(width: 190, height: 252)
        return sizes
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        let result = UIScreen.main.bounds.size
        if result.height == 667.000000 { // 8
            return 2
        } else if result.height == 812.000000 { // 11 pro
            return 4
        } else {
            return 10
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
}
