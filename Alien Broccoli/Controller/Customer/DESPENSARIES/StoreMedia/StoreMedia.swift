//
//  StoreMedia.swift
//  Alien Broccoli
//
//  Created by apple on 21/06/21.
//

import UIKit
import Alamofire
import SDWebImage

class StoreMedia: UIViewController {
    
    var dashboardTitle = ["Home Games","GleeStar","My Marijuana Maps","Laughing","What's the tea","How to do","Token of love","Shop","Combat","Wheels"]
    var dashboardImage = ["game1","star","marijuana","laughing","hot-tea","tick","cardiogram","suitcase(1)","combat","wheel"]
    
    // MARK:- ARRAY -
    var arrListOfAllMyStoresMedia:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var dictGetShopDetailsForMedia:NSDictionary!
    
    @IBOutlet weak var lblCartCount:UILabel! {
        didSet {
            self.lblCartCount.isHidden = true
        }
    }
    
    @IBOutlet weak var indicatorr:UIActivityIndicatorView! {
        didSet {
            indicatorr.color = .white
            indicatorr.isHidden = true
        }
    }
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "MEDIA"
            lblNavigationTitle.textColor = .black
        }
    }
    
    // MARK:- COLLECTION VIEW SETUP -
    @IBOutlet weak var clView: UICollectionView! {
        didSet {
               //collection
            // clView!.dataSource = self
            // clView!.delegate = self
            clView!.backgroundColor = .clear
            clView.isPagingEnabled = false
        }
    }
    
    @IBOutlet weak var btnMenu:UIButton! {
        didSet {
            btnMenu.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnCart:UIButton! {
        didSet {
            btnCart.backgroundColor = .clear
            btnCart.tintColor = .black
            btnCart.isHidden = true
        }
    }
    
    var arrListOfALLImages:NSMutableArray! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor  = .white
        
         self.btnMenu.addTarget(self, action: #selector(sidebackBarMenuClick), for: .touchUpInside)
        self.btnCart.addTarget(self, action: #selector(cartClickMwthod), for: .touchUpInside)
        
        
        
        // self.sidebackBarMenuClick()
        
        // print(self.dictGetShopDetailsForMedia as Any)
        
        self.getAndParseAllImages()
        
    }
    
    @objc func getAndParseAllImages() {
        //arrListOfALLImages
        
        for index in 0...6 {
            
            if index == 0 {
                
                if (self.dictGetShopDetailsForMedia["logo"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetShopDetailsForMedia["logo"] as! String))
                }
                
            } else if index == 1 {
                
                if (self.dictGetShopDetailsForMedia["image_1"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetShopDetailsForMedia["image_1"] as! String))
                }
                
            } else if index == 2 {
                
                if (self.dictGetShopDetailsForMedia["image_2"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetShopDetailsForMedia["image_2"] as! String))
                }
                
            } else if index == 3 {
                
                if (self.dictGetShopDetailsForMedia["image_3"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetShopDetailsForMedia["image_3"] as! String))
                }
                
            } else if index == 4 {
                
                if (self.dictGetShopDetailsForMedia["image_4"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetShopDetailsForMedia["image_4"] as! String))
                }
                
            } else if index == 5 {
                
                if (self.dictGetShopDetailsForMedia["image_5"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetShopDetailsForMedia["image_5"] as! String))
                }
                
            } else if index == 6 {
                
                if (self.dictGetShopDetailsForMedia["image_6"] as! String) == "" {
                    
                } else {
                    self.arrListOfALLImages.add((self.dictGetShopDetailsForMedia["image_6"] as! String))
                }
                
            }
            
        }
        
        self.clView.delegate = self
        self.clView.dataSource = self
        self.clView.reloadData()
        
        
        self.totalItemsInCart()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.gradientNavigation()
        
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
    }
    
    @objc func cartClickMwthod() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CTotalCartListId") as? CTotalCartList
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    @objc func sidebackBarMenuClick() {
        
         self.navigationController?.popViewController(animated: true)
        
        /*if revealViewController() != nil {
            btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
               
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }*/
        
    }
    
}

extension StoreMedia: UICollectionViewDelegate {
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrListOfALLImages.count
    }
    
    //Write Delegate Code Here
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "storeMediaCollectionCell", for: indexPath as IndexPath) as! StoreMediaCollectionCell
           
        /*
         SubCat =             (
         );
         id = 1;
         image = "";
         name = "CBD OIL";
         */
        
        // MARK:- CELL CLASS -
        cell.layer.cornerRadius = 16
        cell.clipsToBounds = true
        cell.backgroundColor = .white
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.borderWidth = 0.6
        
        cell.viewCellBG.backgroundColor = .black //.systemGreen // APP_BUTTON_COLOR
        
        cell.imgImage.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgImage.sd_setImage(with: URL(string: self.arrListOfALLImages[indexPath.row] as! String), placeholderImage: UIImage(named: "logo"))
        
        // cell.imgImage.image = UIImage(named: self.arrListOfALLImages[indexPath.row] as! String)
        
        return cell
    }
    
    
    
    // MARK:- WEBSERVICE ( TOTAL ITEMS IN CART ) -
    @objc func totalItemsInCart() {
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
            
        let params = CartList(action: "getcarts",
                              userId: String(myString))
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            // var ar : NSArray!
                            // ar = (JSON["data"] as! Array<Any>) as NSArray
                            // self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // var strCartCount : String!
                            // strCartCount = JSON["TotalCartItem"]as Any as? String
                            // print(strCartCount as Any)
                            
                            // cart
                            
                            let x : Int = JSON["TotalCartItem"] as! Int
                            let myString = String(x)
                            
                            if myString == "0" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else if myString == "" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else {
                                self.lblCartCount.isHidden = false
                                self.lblCartCount.text = String(myString)
                                
                                self.btnCart.isHidden = false
                                self.btnCart.setImage(UIImage(systemName: "cart.fill"), for: .normal)
                            }
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        self.indicatorr.stopAnimating()
                        self.indicatorr.hidesWhenStopped = true
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
}
    
    
    
}

extension StoreMedia: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
        /*if dashboardTitle[indexPath.row] == "Home Games" {
            
            self.pushToAnotherController(strStoryboardId: "GamesId")
            
        } else if dashboardTitle[indexPath.row] == "GleeStar" || dashboardTitle[indexPath.row] == "Laughing"  || dashboardTitle[indexPath.row] == "What's the tea"  || dashboardTitle[indexPath.row] == "How to do"  || dashboardTitle[indexPath.row] == "Token of love"  || dashboardTitle[indexPath.row] == "Combat"  || dashboardTitle[indexPath.row] == "Wheels" {
          
            self.comingSoonPROMPT()
            
        } else if dashboardTitle[indexPath.row] == "My Marijuana Maps" {
            
            self.pushToAnotherController(strStoryboardId: "CannabiesId")
            
        }*/
        
        
    }
    
    @objc func pushToAnotherController(strStoryboardId:String) {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: strStoryboardId)
        self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    @objc func comingSoonPROMPT() {
        
        let alert = UIAlertController(title: String("Coming Soon"), message: nil, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // MARK:- DASHBOARD PUSH -
    @objc func ordersListingPage() {
         let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "COrderDetailsId") as? COrderDetails
         self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- DISMISS KEYBOARD -
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.clView {
            self.view.endEditing(true)
        }
    }
    
}

extension StoreMedia: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var sizes: CGSize
                
        let result = UIScreen.main.bounds.size
        //NSLog("%f",result.height)
        
        
        if result.height == 480 {
            sizes = CGSize(width: 190, height: 220)
        }
        else if result.height == 568 {
            sizes = CGSize(width: 190, height: 220)
        }
        else if result.height == 667.000000 // 8
        {
            sizes = CGSize(width: 180, height: 220)
        }
        else if result.height == 736.000000 // 8 plus
        {
            sizes = CGSize(width: 180, height: 180)
        }
        else if result.height == 812.000000 // 11 pro
        {
            sizes = CGSize(width: 180, height: 220)
        }
        else if result.height == 896.000000 // 11 , 11 pro max
        {
            sizes = CGSize(width: 180, height: 220)
        }
        else
        {
            sizes = CGSize(width: self.view.frame.size.width, height: 350)
        }
        
        
        return sizes
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        let result = UIScreen.main.bounds.size
        if result.height == 667.000000 { // 8
            return 2
        } else if result.height == 812.000000 { // 11 pro
            return 4
        } else {
            return 10
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        // var sizes: CGSize
                
        let result = UIScreen.main.bounds.size
        if result.height == 667.000000 { // 8
            return UIEdgeInsets(top: 20, left: 4, bottom: 10, right: 4)
        } else if result.height == 736.000000 { // 8 plus
            return UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20)
        } else if result.height == 896.000000 { // 11 plus
            return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        } else if result.height == 812.000000 { // 11 pro
            return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        } else {
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }
        
        
            
    }
    
}


 

