//
//  StoreMediaCollectionCell.swift
//  Alien Broccoli
//
//  Created by apple on 21/06/21.
//

import UIKit

class StoreMediaCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 16
            viewCellBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel! {
        didSet {
            lblTitle.textColor = .white
        }
    }
    
    @IBOutlet weak var imgImage:UIImageView!
    
}
