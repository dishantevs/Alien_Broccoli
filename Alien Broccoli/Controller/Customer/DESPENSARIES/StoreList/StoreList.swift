//
//  StoreList.swift
//  Alien Broccoli
//
//  Created by apple on 18/06/21.
//

import UIKit
import Alamofire
import SDWebImage

class StoreList: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .black
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "DISPENSARIES"
                lblNavigationTitle.textColor = .black
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    // MARK:- ARRAY -
    var arrListOfAllStoresList:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var detailsArray = ["Menu" , "Details" , "Deals" , "Reviews" , "Media"]
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.clipsToBounds = true
            viewCellBG.layer.masksToBounds = false
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.layer.backgroundColor = UIColor.white.cgColor
            viewCellBG.layer.borderColor = UIColor.clear.cgColor
            viewCellBG.layer.shadowColor = UIColor.black.cgColor
            viewCellBG.layer.shadowOffset = CGSize(width: 0, height: 0)
            viewCellBG.layer.shadowOpacity = 0.4
            viewCellBG.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView!
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    var strViewMoreSectionNumber:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        if self.strViewMoreSectionNumber == "1" {
        
            self.viewMoreSection(strStoreFronts: "1", strCBDStores: "0", strDeals: "0")
            
        } else if self.strViewMoreSectionNumber == "2" {
            
            self.viewMoreSection(strStoreFronts: "0", strCBDStores: "1", strDeals: "0")
            
        } else if self.strViewMoreSectionNumber == "3" {
            
            self.viewMoreSection(strStoreFronts: "0", strCBDStores: "0", strDeals: "1")
            
        } else {
            
            self.onlyStoresWB()
            
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.gradientNavigation()
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.white.cgColor
        let colorBottom = UIColor.black.cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
        self.imgProfile.layer.insertSublayer(gradientLayer, at:0)
    }
    
    @objc func viewMoreSection(strStoreFronts:String,strCBDStores:String,strDeals:String) {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        let params = ViewMoreSection(action: "storelist",
                                     Storefronts: String(strStoreFronts),
                                     CBDStores: String(strCBDStores),
                                     deals: String(strDeals))
        
            print(params as Any)
            
            AF.request(BASE_URL_ALIEN_BROCCOLI,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                
                                ERProgressHud.sharedInstance.hide()
                                   
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllStoresList.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
         
    }
    
    @objc func onlyStoresWB() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        let params = OnlyStoresList(action: "storelist")
        
            print(params as Any)
            
            AF.request(BASE_URL_ALIEN_BROCCOLI,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                
                                ERProgressHud.sharedInstance.hide()
                                   
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllStoresList.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
         
    }
    
}

extension StoreList: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllStoresList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:StoreListTableCell = tableView.dequeueReusableCell(withIdentifier: "storeListTableCell") as! StoreListTableCell
          
        /*
         address = asdfasdf;
         id = 1;
         latitude = "28.775461";
         likeme = 1;
         logo = "";
         longitude = "72.325681";
         name = XYZ;
         phone = "";
         rating = 3;
         zipcode = 201301;
         */
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        let item = self.arrListOfAllStoresList[indexPath.row] as? [String:Any]
        
        cell.lblShopName.text = (item!["name"] as! String)
        cell.lblAddress.text = (item!["address"] as! String)
        
        if (item!["phone"] as! String) == "" {
            cell.btnPhoneNumber.setTitle("", for: .normal)
        } else {
            cell.btnPhoneNumber.setTitle((item!["phone"] as! String), for: .normal)
        }
        
        
        cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgProfile.sd_setImage(with: URL(string: (item!["logo"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        if (item!["rating"] as! String) == "0" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "1" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "2" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "3" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "4" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "5" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star.fill"), for: .normal)
            
        } else {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        }
        
        cell.backgroundColor = .white
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        
        let item = self.arrListOfAllStoresList[indexPath.row] as? [String:Any]
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreListDetailsId") as? StoreListDetails
        push!.dictGetStoreDetails = item as NSDictionary?
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 310
    }
    
}

extension StoreList: UITableViewDelegate {
    
}
