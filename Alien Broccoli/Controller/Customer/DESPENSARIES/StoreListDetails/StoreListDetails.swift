//
//  StoreListDetails.swift
//  Alien Broccoli
//
//  Created by apple on 18/06/21.
//

import UIKit
import Alamofire
import SDWebImage

class StoreListDetails: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .black
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "DETAILS"
                lblNavigationTitle.textColor = .black
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    // MARK:- ARRAY -
    var arrListOfAllBrands:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var detailsArray = ["Menu" , "Details" , "Deals" , "Reviews" , "Media"]
    var detailsArrayImage = ["dmenu" , "ddetails" , "ddeal" , "dReview" , "dmedia"]
    
    var dictGetStoreDetails:NSDictionary!
    
    var dict: Dictionary<AnyHashable, Any> = [:]
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.clipsToBounds = true
            viewCellBG.layer.masksToBounds = false
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.layer.backgroundColor = UIColor.white.cgColor
            viewCellBG.layer.borderColor = UIColor.clear.cgColor
            viewCellBG.layer.shadowColor = UIColor.black.cgColor
            viewCellBG.layer.shadowOffset = CGSize(width: 0, height: 0)
            viewCellBG.layer.shadowOpacity = 0.4
            viewCellBG.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 8
            imgProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var viewOnlyBlack:UIView! {
        didSet {
            // viewOnlyBlack.backgroundColor = .black
            
            let gradientLayer:CAGradientLayer = CAGradientLayer()
            gradientLayer.frame.size = viewOnlyBlack.frame.size
            gradientLayer.colors =
                [UIColor.white.cgColor,UIColor.black.withAlphaComponent(1).cgColor]
              //Use diffrent colors
            viewOnlyBlack.layer.addSublayer(gradientLayer)
            
        }
    }
    
    @IBOutlet weak var viewDataSetup:UIView! {
        didSet {
            viewDataSetup.backgroundColor = .white
            
        }
    }
    @IBOutlet weak var btnStarOne:UIButton! {
        didSet {
            btnStarOne.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarTwo:UIButton! {
        didSet {
            btnStarTwo.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarThree:UIButton! {
        didSet {
            btnStarThree.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarFour:UIButton! {
        didSet {
            btnStarFour.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarFive:UIButton! {
        didSet {
            btnStarFive.tintColor = .systemYellow
            
        }
    }
    
    @IBOutlet weak var lblShopName:UILabel! {
        didSet {
            lblShopName.textColor = .black
            lblShopName.backgroundColor = .clear
        }
    }
    @IBOutlet weak var btnPhoneNumber:UIButton! {
        didSet {
            btnPhoneNumber.setTitleColor(.black, for: .normal)
            btnPhoneNumber.backgroundColor = .clear
        }
    }
    @IBOutlet weak var lblAddress:UILabel! {
        didSet {
            lblAddress.textColor = .black
            lblAddress.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var lblDistance:UILabel! {
        didSet {
            lblDistance.textColor = .white
            lblDistance.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            lblDistance.text = " 1.7km "
            lblDistance.layer.cornerRadius = 4
            lblDistance.clipsToBounds = true
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        // print(self.dictGetStoreDetails as Any)
        
        /*
         OpenNow = "15:30-15:30";
         address = "New west area";
         id = 4;
         latitude = "25.957800";
         likeme = 1;
         logo = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624003055_Online-Medical-Store-Baranagar.jpg";
         longitude = "80.149803";
         name = "Medical Store";
         phone = "";
         rating = 3;
         zipcode = 201301;
         */
        
        
        
        self.parseDataOfThatDispensaries()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.gradientNavigation()
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
        
    }
    
    @objc func parseDataOfThatDispensaries() {
    
        self.lblShopName.text = (dictGetStoreDetails!["name"] as! String)
        self.lblAddress.text = (dictGetStoreDetails!["address"] as! String)
        
        if (dictGetStoreDetails!["phone"] as! String) == "" {
            self.btnPhoneNumber.setTitle("", for: .normal)
        } else {
            self.btnPhoneNumber.setTitle((dictGetStoreDetails!["phone"] as! String), for: .normal)
        }
        
        
        self.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        self.imgProfile.sd_setImage(with: URL(string: (dictGetStoreDetails!["logo"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        if (dictGetStoreDetails!["rating"] as! String) == "0" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreDetails!["rating"] as! String) == "1" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreDetails!["rating"] as! String) == "2" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreDetails!["rating"] as! String) == "3" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreDetails!["rating"] as! String) == "4" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (dictGetStoreDetails!["rating"] as! String) == "5" {
            
            self.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star.fill"), for: .normal)
            
        } else {
            
            self.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            self.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        }
        
        self.storeDetailsList()
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.white.cgColor
        let colorBottom = UIColor.black.cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
        self.imgProfile.layer.insertSublayer(gradientLayer, at:0)
    }
    
    @objc func storeDetailsList() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        let x2 : Int = (self.dictGetStoreDetails["id"] as! Int)
        let myString2 = String(x2)
        
        let params = StoreDetails(action: "storedetails",
                                  storeId:String(myString2))
        
            print(params as Any)
            
            AF.request(BASE_URL_ALIEN_BROCCOLI,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            // print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                
                                
                                self.dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                   
                                /*var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllStoresList.addObjects(from: ar as! [Any])*/
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
         
    }
}

extension StoreListDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:StoreListDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: "storeListDetailsTableCell") as! StoreListDetailsTableCell
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.lblTitle.text = self.detailsArray[indexPath.row]
        cell.imgIcons.image = UIImage(named: self.detailsArrayImage[indexPath.row])
        
        cell.backgroundColor = .white
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 0 {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreProductsId") as? StoreProducts
            push!.dictGetCanabbiesItemDetails = self.dict as NSDictionary
            self.navigationController?.pushViewController(push!, animated: true)
            
        } else if indexPath.row == 1 {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreAllDetailsId") as? StoreAllDetails
            push!.dictGetStoreAllDetails = self.dict as NSDictionary
            self.navigationController?.pushViewController(push!, animated: true)
            
        } else if indexPath.row == 2 {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreDealsProductId") as? StoreDealsProduct
            push!.dictGetCanabbiesItemDetails = self.dict as NSDictionary
            self.navigationController?.pushViewController(push!, animated: true)
            
        } else if indexPath.row == 4 {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreMediaId") as? StoreMedia
            push!.dictGetShopDetailsForMedia = self.dict as NSDictionary
            self.navigationController?.pushViewController(push!, animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension StoreListDetails: UITableViewDelegate {
    
}
