//
//  StoreListDetailsTableCell.swift
//  Alien Broccoli
//
//  Created by apple on 18/06/21.
//

import UIKit

class StoreListDetailsTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.layer.cornerRadius = 8
            viewBG.clipsToBounds = true
            viewBG.layer.masksToBounds = false
            viewBG.layer.cornerRadius = 8
            viewBG.layer.backgroundColor = UIColor.white.cgColor
            viewBG.layer.borderColor = UIColor.clear.cgColor
            viewBG.layer.shadowColor = UIColor.black.cgColor
            viewBG.layer.shadowOffset = CGSize(width: 0, height: 0)
            viewBG.layer.shadowOpacity = 0.4
            viewBG.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var imgIcons:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
