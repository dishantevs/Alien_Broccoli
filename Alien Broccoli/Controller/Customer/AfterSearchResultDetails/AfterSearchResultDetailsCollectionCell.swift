//
//  AfterSearchResultDetailsCollectionCell.swift
//  Alien Broccoli
//
//  Created by Apple on 29/12/20.
//

import UIKit

class AfterSearchResultDetailsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 0
            viewCellBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var imgProductImage:UIImageView!
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    
}
