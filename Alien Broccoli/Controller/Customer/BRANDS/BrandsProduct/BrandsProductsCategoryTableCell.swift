//
//  BrandsProductsCategoryTableCell.swift
//  Alien Broccoli
//
//  Created by apple on 22/06/21.
//

import UIKit

class BrandsProductsCategoryTableCell: UITableViewCell {

    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.backgroundColor = UIColor.white
        }
    }
    
    @IBOutlet weak var lblCategoryName:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
