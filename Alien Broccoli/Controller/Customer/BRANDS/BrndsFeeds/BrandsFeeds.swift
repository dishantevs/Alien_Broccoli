//
//  BrandsFeeds.swift
//  Alien Broccoli
//
//  Created by apple on 23/06/21.
//

import UIKit
import Alamofire
import SDWebImage

class BrandsFeeds: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .black
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "BRAND"
                lblNavigationTitle.textColor = .black
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    // MARK:- ARRAY -
    var arrListOfAllCategories:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var detailsArray = ["Menu" , "Details" , "Deals" , "Reviews" , "Media"]
    
    var brandIdIs3:String!
    
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.layer.cornerRadius = 8
            viewBG.clipsToBounds = true
            viewBG.layer.masksToBounds = false
            viewBG.layer.cornerRadius = 8
            viewBG.layer.backgroundColor = UIColor.white.cgColor
            viewBG.layer.borderColor = UIColor.clear.cgColor
            viewBG.layer.shadowColor = UIColor.black.cgColor
            viewBG.layer.shadowOffset = CGSize(width: 0, height: 0)
            viewBG.layer.shadowOpacity = 0.4
            viewBG.layer.shadowRadius = 4
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    var dictProductShopDetails:NSDictionary!
    
    var strAboutUs:String!
    var strInstagram:String!
    var strFacebook:String!
    var strTwitter:String!
    var strWebsite:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        self.lblNavigationTitle.text = "Feeds" // (self.dictProductShopDetails["name"] as! String)
        
        self.strAboutUs = ""
        self.strInstagram = ""
        self.strFacebook = ""
        self.strTwitter = ""
        self.strWebsite = ""
        
        self.getAllSocialDetailsOfThatProduct()
        
        // self.onlyStoresWB()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.gradientNavigation()
    }
    
    @objc func getAllSocialDetailsOfThatProduct() {
    
        let defaults = UserDefaults.standard
        
        if let myString = defaults.string(forKey: "keySaveBrandAboutUs") {
            print("defaults savedString: \(myString)")
            
            self.strAboutUs = myString
        }
        
        if let myString = defaults.string(forKey: "keySaveBrandInstagram") {
            print("defaults savedString: \(myString)")
            
            self.strInstagram = myString
            
        }
        
        if let myString = defaults.string(forKey: "keySaveBrandFacebook") {
            print("defaults savedString: \(myString)")
            
            self.strFacebook = myString
            
        }
        
        if let myString = defaults.string(forKey: "keySaveBrandTwitter") {
            print("defaults savedString: \(myString)")
            
            self.strTwitter = myString
            
        }
        
        if let myString = defaults.string(forKey: "keySaveBrandWebsite") {
            print("defaults savedString: \(myString)")
            
            self.strWebsite = myString
            
        }
        
        self.tbleView.delegate = self
        self.tbleView.dataSource = self
        self.tbleView.reloadData()
        
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.white.cgColor
        let colorBottom = UIColor.black.cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
    }
    
    // MARK:- STORE LIST VIA BRAND -
    @objc func onlyStoresWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        let params = CategoryList(action: "category")
        
            print(params as Any)
            
            AF.request(BASE_URL_ALIEN_BROCCOLI,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            // print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                ERProgressHud.sharedInstance.hide()
                                 
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllCategories.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
         
    }
    
}

extension BrandsFeeds: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:BrandsFeedsTableCell = tableView.dequeueReusableCell(withIdentifier: "brandsFeedsTableCell") as! BrandsFeedsTableCell
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        /*
         var strAboutUs:String!
         var strIe
         
         var strInstagram:String!
         var strFacebook:String!
         var strTwitter:String!
         var strWebsite:String!
         */
        
        if indexPath.row == 0 {
            
            cell.lblTitle.text = "About Us"
            cell.lblSubTitle.text = String(strAboutUs)
            cell.imgIcons.setImage(UIImage(named: "brandsNotes"), for: .normal)
            
        } else if indexPath.row == 1 {
            
            cell.lblTitle.text = "Instagram"
            cell.lblSubTitle.text = String(strInstagram)
            cell.imgIcons.setImage(UIImage(named: "brandsInstagram"), for: .normal)
            
        } else if indexPath.row == 2 {
            
            cell.lblTitle.text = "Facebook"
            cell.lblSubTitle.text = String(strFacebook)
            cell.imgIcons.setImage(UIImage(named: "brandsFacebook"), for: .normal)
            
        } else if indexPath.row == 3 {
            
            cell.lblTitle.text = "Twitter"
            cell.lblSubTitle.text = String(strTwitter)
            cell.imgIcons.setImage(UIImage(named: "brandsTwitter"), for: .normal)
            
        } else if indexPath.row == 4 {
            
            cell.lblTitle.text = "Website"
            cell.lblSubTitle.text = String(strWebsite)
            cell.imgIcons.setImage(UIImage(named: "brandsWebsite"), for: .normal)
            
        } else {
            
            cell.lblTitle.text = "About Us"
            cell.lblSubTitle.text = String(strAboutUs)
            
        }
        
        cell.backgroundColor = .white
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 1 {
            
            if let url = URL(string: strInstagram) {
                UIApplication.shared.open(url)
            }
            
        } else if indexPath.row == 2 {
            
            if let url = URL(string: strFacebook) {
                UIApplication.shared.open(url)
            }
            
        } else if indexPath.row == 3 {
            
            if let url = URL(string: strTwitter) {
                UIApplication.shared.open(url)
            }
            
        } else if indexPath.row == 4 {
            
            if let url = URL(string: strWebsite) {
                UIApplication.shared.open(url)
            }
            
        }
       
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension BrandsFeeds: UITableViewDelegate {
    
}
