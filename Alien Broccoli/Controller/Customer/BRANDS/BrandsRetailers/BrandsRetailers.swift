//
//  BrandsRetailers.swift
//  Alien Broccoli
//
//  Created by apple on 22/06/21.
//

import UIKit
import Alamofire
import SDWebImage

class BrandsRetailers: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .black
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "BRAND"
                lblNavigationTitle.textColor = .black
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    // MARK:- ARRAY -
    var arrListOfAllRetailersList:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var detailsArray = ["Menu" , "Details" , "Deals" , "Reviews" , "Media"]
    
    var brandIdIs5:String!
    
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.layer.cornerRadius = 8
            viewBG.clipsToBounds = true
            viewBG.layer.masksToBounds = false
            viewBG.layer.cornerRadius = 8
            viewBG.layer.backgroundColor = UIColor.white.cgColor
            viewBG.layer.borderColor = UIColor.clear.cgColor
            viewBG.layer.shadowColor = UIColor.black.cgColor
            viewBG.layer.shadowOffset = CGSize(width: 0, height: 0)
            viewBG.layer.shadowOpacity = 0.4
            viewBG.layer.shadowRadius = 4
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    var dictProductShopDetails:NSDictionary!
    
    var strAboutUs:String!
    var strInstagram:String!
    var strFacebook:String!
    var strTwitter:String!
    var strWebsite:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        self.lblNavigationTitle.text = "Retailers" // (self.dictProductShopDetails["name"] as! String)
        
        self.onlyStoresWB()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.gradientNavigation()
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.white.cgColor
        let colorBottom = UIColor.black.cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
    }
    
    // MARK:- STORE LIST VIA BRAND -
    @objc func onlyStoresWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        let params = ShopOfBrand(action: "storelistbybrand",
                                 brandId:String(brandIdIs5))
        
            print(params as Any)
            
            AF.request(BASE_URL_ALIEN_BROCCOLI,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                
                                ERProgressHud.sharedInstance.hide()
                                   
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllRetailersList.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
         
    }
    
    
}

extension BrandsRetailers: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllRetailersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:BrandsRetailersTableCell = tableView.dequeueReusableCell(withIdentifier: "brandsRetailersTableCell") as! BrandsRetailersTableCell
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        let item = self.arrListOfAllRetailersList[indexPath.row] as? [String:Any]
        
        cell.lblShopName.text = (item!["name"] as! String)
        cell.lblAddress.text = (item!["address"] as! String)
        
        cell.imgIcons.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgIcons.sd_setImage(with: URL(string: (item!["logo"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        if (item!["rating"] as! String) == "0" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "1" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "2" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "3" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "4" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "5" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star.fill"), for: .normal)
            
        } else {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        }
        
        cell.backgroundColor = .white
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let item = self.arrListOfAllRetailersList[indexPath.row] as? [String:Any]
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BrandsAllShopDetailsId") as? BrandsAllShopDetails
        push!.dictGetStoreDetails = item as NSDictionary?
        push!.brandIdIs2 = String(brandIdIs5)
        self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension BrandsRetailers: UITableViewDelegate {
    
}
