//
//  BrandsRetailersTableCell.swift
//  Alien Broccoli
//
//  Created by apple on 22/06/21.
//

import UIKit

class BrandsRetailersTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.layer.cornerRadius = 8
            viewBG.clipsToBounds = true
            viewBG.layer.masksToBounds = false
            viewBG.layer.cornerRadius = 8
            viewBG.layer.backgroundColor = UIColor.white.cgColor
            viewBG.layer.borderColor = UIColor.clear.cgColor
            viewBG.layer.shadowColor = UIColor.black.cgColor
            viewBG.layer.shadowOffset = CGSize(width: 0, height: 0)
            viewBG.layer.shadowOpacity = 0.4
            viewBG.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var imgIcons:UIImageView! {
        didSet {
            /*imgIcons.layer.cornerRadius = 8
            imgIcons.clipsToBounds = true
            imgIcons.layer.masksToBounds = false
            imgIcons.layer.cornerRadius = 8
            imgIcons.layer.backgroundColor = UIColor.white.cgColor
            imgIcons.layer.borderColor = UIColor.clear.cgColor
            imgIcons.layer.shadowColor = UIColor.black.cgColor
            imgIcons.layer.shadowOffset = CGSize(width: 0, height: 0)
            imgIcons.layer.shadowOpacity = 0.4
            imgIcons.layer.shadowRadius = 4*/
        }
    }
    
    @IBOutlet weak var btnStarOne:UIButton! {
        didSet {
            btnStarOne.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarTwo:UIButton! {
        didSet {
            btnStarTwo.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarThree:UIButton! {
        didSet {
            btnStarThree.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarFour:UIButton! {
        didSet {
            btnStarFour.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarFive:UIButton! {
        didSet {
            btnStarFive.tintColor = .systemYellow
            
        }
    }
    
    @IBOutlet weak var lblShopName:UILabel! {
        didSet {
            lblShopName.textColor = .black
            lblShopName.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var lblAddress:UILabel! {
        didSet {
            lblAddress.textColor = .black
            lblAddress.backgroundColor = .clear
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
