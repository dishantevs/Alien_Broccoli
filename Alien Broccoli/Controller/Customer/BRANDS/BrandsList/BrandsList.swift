//
//  BrandsList.swift
//  Alien Broccoli
//
//  Created by apple on 22/06/21.
//

import UIKit
import Alamofire
import SDWebImage
import SDWebImage

class BrandsList: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .black
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "BRANDS"
                lblNavigationTitle.textColor = .black
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    // MARK:- ARRAY -
    var arrListOfAllBrands:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var detailsArray = ["Menu" , "Details" , "Deals" , "Reviews" , "Media"]
    var detailsArrayImage = ["dmenu" , "ddetails" , "ddeal" , "dReview" , "dmedia"]
    
    var dictGetStoreDetails:NSDictionary!
    
    var dict: Dictionary<AnyHashable, Any> = [:]
    
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    @IBOutlet weak var txtSearch:UITextField! {
        didSet {
            txtSearch.layer.cornerRadius = 6
            txtSearch.clipsToBounds = true
            txtSearch.setLeftPaddingPoints(40)
            txtSearch.layer.borderColor = UIColor.lightGray.cgColor
            txtSearch.layer.borderWidth = 0.8
            txtSearch.attributedPlaceholder = NSAttributedString(string: "search...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtSearch.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var txtLocation:UITextField! {
        didSet {
            txtLocation.layer.cornerRadius = 6
            txtLocation.clipsToBounds = true
            txtLocation.setLeftPaddingPoints(40)
            txtLocation.layer.borderColor = UIColor.lightGray.cgColor
            txtLocation.layer.borderWidth = 0.8
            txtLocation.attributedPlaceholder = NSAttributedString(string: "location...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtLocation.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var viewScrollBG:UIView!
    
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    let dummyImageArray = ["hd1" , "hd2" , "hd3" , "hd4"]
        
    @IBOutlet weak var scrollViewBanner:UIScrollView! {
        didSet {
            scrollViewBanner.backgroundColor = .systemTeal
        }
    }
    
    @IBOutlet weak var btnLeftScroll:UIButton! {
        didSet {
            btnLeftScroll.tintColor = .white
            btnLeftScroll.layer.cornerRadius = 8
            btnLeftScroll.clipsToBounds = true
            btnLeftScroll.layer.masksToBounds = false
            btnLeftScroll.layer.cornerRadius = 8
            btnLeftScroll.layer.backgroundColor = UIColor.black.cgColor
            btnLeftScroll.layer.borderColor = UIColor.clear.cgColor
            btnLeftScroll.layer.shadowColor = UIColor.black.cgColor
            btnLeftScroll.layer.shadowOffset = CGSize(width: 0, height: 0)
            btnLeftScroll.layer.shadowOpacity = 0.4
            btnLeftScroll.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var btnRightScroll:UIButton! {
        didSet {
            btnRightScroll.tintColor = .white
            btnRightScroll.layer.cornerRadius = 8
            btnRightScroll.clipsToBounds = true
            btnRightScroll.layer.masksToBounds = false
            btnRightScroll.layer.cornerRadius = 8
            btnRightScroll.layer.backgroundColor = UIColor.black.cgColor
            btnRightScroll.layer.borderColor = UIColor.clear.cgColor
            btnRightScroll.layer.shadowColor = UIColor.black.cgColor
            btnRightScroll.layer.shadowOffset = CGSize(width: 0, height: 0)
            btnRightScroll.layer.shadowOpacity = 0.4
            btnRightScroll.layer.shadowRadius = 4
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .lightGray
        
        self.screenSize = UIScreen.main.bounds
        self.screenWidth = screenSize.width
        self.screenHeight = screenSize.height
        
        // print(self.dictGetStoreDetails as Any)
        
        /*
         OpenNow = "15:30-15:30";
         address = "New west area";
         id = 4;
         latitude = "25.957800";
         likeme = 1;
         logo = "http://demo2.evirtualservices.com/HoneyBudz/site/img/uploads/store/1624003055_Online-Medical-Store-Baranagar.jpg";
         longitude = "80.149803";
         name = "Medical Store";
         phone = "";
         rating = 3;
         zipcode = 201301;
         */
        
        self.parseDataOfThatDispensaries()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.gradientNavigation()
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
        
    }
    
    @objc func parseDataOfThatDispensaries() {

        self.allBrandsListWB()
    }
    
    @objc func bannerScrollable() {
        
        self.scrollViewBanner.frame =  CGRect(x: 0, y: 0, width:self.viewScrollBG.frame.size.width , height: self.viewScrollBG.frame.size.height)
        self.scrollViewBanner.backgroundColor = .clear
        self.scrollViewBanner.showsHorizontalScrollIndicator = false
        self.scrollViewBanner.isPagingEnabled = true
        self.viewScrollBG.addSubview(self.scrollViewBanner)
        
        for index in 0..<self.dummyImageArray.count {
            
            frame.origin.x = self.scrollViewBanner.frame.size.width * CGFloat(index)
            frame.size = self.scrollViewBanner.frame.size
            
            let imageView = UIImageView()
            imageView.image = UIImage(named: self.dummyImageArray[index])
            imageView.frame = frame
            
            self.scrollViewBanner.contentSize.width = self.scrollViewBanner.frame.width * CGFloat(index + 1)
            self.scrollViewBanner.addSubview(imageView)
            
        }
        
        // self.scrollViewBanner.contentSize = CGSize(width: self.scrollViewBanner.frame.size.width * CGFloat(self.arrAddQuestions.count), height: self.scrollViewBanner.frame.size.height)
        self.scrollViewBanner.delegate = self
        
        self.btnLeftScroll.addTarget(self, action: #selector(leftButtonTapped), for: .touchUpInside)
        self.btnRightScroll.addTarget(self, action: #selector(rightButtonTapped), for: .touchUpInside)
        
    }
    
    @objc func leftButtonTapped() {
         if self.scrollViewBanner.contentOffset.x > 0 {
            self.scrollViewBanner.contentOffset.x -=  self.viewScrollBG.bounds.width
         }
    }
    
    @objc func rightButtonTapped() {
        
        if self.scrollViewBanner.contentOffset.x < self.viewScrollBG.bounds.width * CGFloat(self.dummyImageArray.count-1) {
            self.scrollViewBanner.contentOffset.x +=  self.viewScrollBG.bounds.width
        }
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.white.cgColor
        let colorBottom = UIColor.black.cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
        // self.imgProfile.layer.insertSublayer(gradientLayer, at:0)
    }
    
    @objc func allBrandsListWB() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        /*if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
        
            let x : Int = person["userId"] as! Int
            let myString = String(x)*/
            
        let params = AllBrandList(action: "brandlist"/*,
                                  userId: String(myString)*/)
        
            print(params as Any)
            
            AF.request(BASE_URL_ALIEN_BROCCOLI,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                // self.dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                   
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllBrands.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                                self.bannerScrollable()
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
    // }
    }
    
    
    
    
    @objc func followThisBrand(strStatus:String,strBrandId:String) {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
        
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
        let params = BrandFollow(action: "followbrand",
                                 userId: String(myString),
                                 brandId: String(strBrandId),
                                 status: String(strStatus))
        
            print(params as Any)
            
            AF.request(BASE_URL_ALIEN_BROCCOLI,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                // self.dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                   
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllBrands.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                                self.bannerScrollable()
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
    }
    }
}

extension BrandsList: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllBrands.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:BrandsListTableCell = tableView.dequeueReusableCell(withIdentifier: "brandsListTableCell") as! BrandsListTableCell
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        let item = self.arrListOfAllBrands[indexPath.row] as? [String:Any]
        
        cell.lblTitle.text = (item!["name"] as! String)
        
        cell.imgIcons.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgIcons.sd_setImage(with: URL(string: (item!["logo"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        cell.btnFollow.tag = indexPath.row
        cell.btnFollow.addTarget(self, action: #selector(checkFollowButtonText), for: .touchUpInside)
        
        cell.backgroundColor = .white
        
        return cell
        
    }

    @objc func checkFollowButtonText(_ sender:UIButton) {
        
        let btn:UIButton = sender
        
        let item = self.arrListOfAllBrands[sender.tag] as? [String:Any]
        let x : Int = item!["id"] as! Int
        let myString = String(x)
        
        if btn.titleLabel?.text == "follow" {
            self.followThisBrand(strStatus: "1", strBrandId: String(myString))
        } else {
            self.followThisBrand(strStatus: "2", strBrandId: String(myString))
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let item = self.arrListOfAllBrands[indexPath.row] as? [String:Any]
        
        let defaults = UserDefaults.standard
        defaults.set((item!["description"] as! String), forKey: "keySaveBrandAboutUs")
        defaults.set((item!["Insta"] as! String), forKey: "keySaveBrandInstagram")
        defaults.set((item!["facebook"] as! String), forKey: "keySaveBrandFacebook")
        // defaults.set((item!["twitter"] as! String), forKey: "keySaveBrandTwitter")
        defaults.set((item!["website"] as! String), forKey: "keySaveBrandWebsite")
        
        let x2 : Int = (item!["id"] as! Int)
        let myString2 = String(x2)
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoresOfBrandsId") as? StoresOfBrands
        push!.brandIdIs = String(myString2)
        push!.brandNameIs = (item!["name"] as! String)
        self.navigationController?.pushViewController(push!, animated: true)
            
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}

extension BrandsList: UITableViewDelegate {
    
}
