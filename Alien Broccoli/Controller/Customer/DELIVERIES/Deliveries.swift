//
//  Deliveries.swift
//  Alien Broccoli
//
//  Created by apple on 21/06/21.
//

import UIKit
import Alamofire
import SDWebImage

class Deliveries: UIViewController {
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .black
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "DELIVERIES"
                lblNavigationTitle.textColor = .black
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    // MARK:- ARRAY -
    var arrListOfAllStoresList:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var detailsArray = ["Menu" , "Details" , "Deals" , "Reviews" , "Media"]
    
    var arrListOfAllDeliveriesDetails:NSMutableArray! = []
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.clipsToBounds = true
            viewCellBG.layer.masksToBounds = false
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.layer.backgroundColor = UIColor.white.cgColor
            viewCellBG.layer.borderColor = UIColor.clear.cgColor
            viewCellBG.layer.shadowColor = UIColor.black.cgColor
            viewCellBG.layer.shadowOffset = CGSize(width: 0, height: 0)
            viewCellBG.layer.shadowOpacity = 0.4
            viewCellBG.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView!
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    @IBOutlet weak var txtSearch:UITextField! {
        didSet {
            txtSearch.layer.cornerRadius = 6
            txtSearch.clipsToBounds = true
            txtSearch.setLeftPaddingPoints(40)
            txtSearch.layer.borderColor = UIColor.clear.cgColor
            txtSearch.layer.borderWidth = 0.8
            txtSearch.attributedPlaceholder = NSAttributedString(string: "search...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtSearch.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var txtLocation:UITextField! {
        didSet {
            txtLocation.layer.cornerRadius = 6
            txtLocation.clipsToBounds = true
            txtLocation.setLeftPaddingPoints(40)
            txtLocation.layer.borderColor = UIColor.clear.cgColor
            txtLocation.layer.borderWidth = 0.8
            txtLocation.attributedPlaceholder = NSAttributedString(string: "location...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtLocation.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var btnDelivery:UIButton! {
        didSet {
            btnDelivery.backgroundColor = .black
            btnDelivery.setTitle("Delivery", for: .normal)
            btnDelivery.setTitleColor(.white, for: .normal)
            
            let bottomBorder = CALayer()
            bottomBorder.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor;
            bottomBorder.borderWidth = 2
            bottomBorder.frame = CGRect(x: 0, y: btnDelivery.frame.height, width: btnDelivery.frame.width, height: 1)
            btnDelivery.layer.addSublayer(bottomBorder)
            
        }
    }
    
    @IBOutlet weak var btnPickup:UIButton! {
        didSet {
            btnPickup.backgroundColor = .black
            btnPickup.setTitle("Pickup", for: .normal)
            btnPickup.setTitleColor(.white, for: .normal)
        }
    }
    
    var staticSixArray = ["Order Online", "Open Now", "Medical Only"]
    
    // MARK:- COLLECTION VIEW SETUP -
    @IBOutlet weak var clViewForStaticButton: UICollectionView! {
        didSet {
            clViewForStaticButton!.dataSource = self
            clViewForStaticButton!.delegate = self
            clViewForStaticButton!.backgroundColor = .clear
            clViewForStaticButton.isPagingEnabled = false
        }
    }
    
    var arrayShowUpperCollectionViewData:NSMutableArray! = []
    
    var deliveryString:String!
    var pickupString:String!
    var orderOnlinePickup:String!
    var medicalPickup:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.view.backgroundColor = .white
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        self.gradientNavigation()
        
        self.deliveryString = "1"
        self.pickupString = "0"
        self.orderOnlinePickup = "0"
        self.medicalPickup = "0"
        
        for index in 0...2 {
            
            let someDict:[String:String] = [
                "name":String(staticSixArray[index]),
                "status":"0"
            ]
            
            self.arrayShowUpperCollectionViewData.add(someDict)
            
        }
        
        // print(self.arrayShowUpperCollectionViewData as Any)
        self.deliveriesDataWB()
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.white.cgColor
        let colorBottom = UIColor.black.cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
                
        self.imgProfile.layer.insertSublayer(gradientLayer, at:0)
    }
    
}

extension Deliveries: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrListOfAllDeliveriesDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:DeliveriesTableCell = tableView.dequeueReusableCell(withIdentifier: "deliveriesTableCell") as! DeliveriesTableCell
          
        /*
         address = asdfasdf;
         id = 1;
         latitude = "28.775461";
         likeme = 1;
         logo = "";
         longitude = "72.325681";
         name = XYZ;
         phone = "";
         rating = 3;
         zipcode = 201301;
         */
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        let item = self.arrListOfAllDeliveriesDetails[indexPath.row] as? [String:Any]
        
        cell.lblShopName.text = (item!["name"] as! String)
        cell.lblAddress.text = (item!["address"] as! String)
        
        if (item!["phone"] as! String) == "" {
            cell.btnPhoneNumber.setTitle("", for: .normal)
        } else {
            cell.btnPhoneNumber.setTitle((item!["phone"] as! String), for: .normal)
        }
        
        cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        cell.imgProfile.sd_setImage(with: URL(string: (item!["logo"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        if (item!["rating"] as! String) == "0" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "1" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "2" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "3" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "4" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        } else if (item!["rating"] as! String) == "5" {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star.fill"), for: .normal)
            
        } else {
            
            cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
            cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
            
        }
        
        cell.backgroundColor = .white
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let item = self.arrListOfAllDeliveriesDetails[indexPath.row] as? [String:Any]
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreListDetailsId") as? StoreListDetails
        push!.dictGetStoreDetails = item as NSDictionary?
        self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 310
    }
    
}

extension Deliveries: UITableViewDelegate {
    
}

// MARK:- COLLECTION VIEW -
extension Deliveries: UICollectionViewDelegate {
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrayShowUpperCollectionViewData.count
            
    }
    
    //Write Delegate Code Here
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "deliveriesCollectionCell", for: indexPath as IndexPath) as! DeliveriesCollectionCell
            
        /*cell.lblTitle.text = self.staticSixArray[indexPath.row]
         cell.lblTitle.textColor = .white
         cell.backgroundColor = .black
         cell.layer.cornerRadius = 4
         cell.clipsToBounds = true*/
            
        let item = self.arrayShowUpperCollectionViewData[indexPath.row] as? [String:Any]
        cell.lblTitle.text = (item!["name"] as! String)
        
        if (item!["status"] as! String) == "0" {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 2
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        } else {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = UIColor.black.cgColor
            
        }
        
        return cell
        
       
    }
    
    
    @objc func deliveriesDataWB() {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        let params = DeliveriesData(action: "storelist",
                                    Delivery: String(self.deliveryString),
                                    Medical: String(self.medicalPickup),
                                    OrderOnline: String(self.orderOnlinePickup))
        
            print(params as Any)
            
            AF.request(BASE_URL_ALIEN_BROCCOLI,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            // print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                ERProgressHud.sharedInstance.hide()
                               
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllDeliveriesDetails.addObjects(from: ar as! [Any])
                                
                                self.tbleView.delegate = self
                                self.tbleView.dataSource = self
                                self.tbleView.reloadData()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
         
    }
}

extension Deliveries: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("clicked")
        
        let item = self.arrayShowUpperCollectionViewData[indexPath.row] as? [String:Any]
        
        if (item!["status"] as! String) == "0" {
            
            self.arrayShowUpperCollectionViewData.removeObject(at: indexPath.row)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[indexPath.row]),
                "status":"1"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: indexPath.row)
            
            // print(self.arrayShowUpperCollectionViewData as Any)
            
        } else {
            
            self.arrayShowUpperCollectionViewData.removeObject(at: indexPath.row)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[indexPath.row]),
                "status":"0"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: indexPath.row)
            
            // print(self.arrayShowUpperCollectionViewData as Any)
            
        }
        
        /*
         self.deliveryString = "1"
         self.pickupString = "0"
         self.orderOnlinePickup = "0"
         self.medicalPickup = "0"
         */
        
        self.clViewForStaticButton.reloadData()
        
        // print(self.arrayShowUpperCollectionViewData as Any)
        
        if indexPath.row == 0 {
            
            if (item!["status"] as! String) == "0" {
                self.orderOnlinePickup = "1"
            } else {
                self.orderOnlinePickup = "0"
            }
            
        } else if indexPath.row == 1 {
            
            
            
        } else {
            
            if (item!["status"] as! String) == "0" {
                self.medicalPickup = "1"
            } else {
                self.medicalPickup = "0"
            }
            
        }
        
        self.deliveriesDataWB()
        
    }
    
}

extension Deliveries: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var sizes: CGSize
        sizes = CGSize(width: 100, height: 40)
        return sizes
          
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
            let result = UIScreen.main.bounds.size
            if result.height == 667.000000 { // 8
                return 2
            } else if result.height == 812.000000 { // 11 pro
                return 4
            } else {
                return 10
            }
            
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        
        if collectionView == self.clViewForStaticButton {
            return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        } else {
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }
        
        
    }
   
}
