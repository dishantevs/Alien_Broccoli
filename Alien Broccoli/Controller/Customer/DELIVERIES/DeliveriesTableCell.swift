//
//  DeliveriesTableCell.swift
//  Alien Broccoli
//
//  Created by apple on 21/06/21.
//

import UIKit

class DeliveriesTableCell: UITableViewCell {

    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.clipsToBounds = true
            viewCellBG.layer.masksToBounds = false
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.layer.backgroundColor = UIColor.white.cgColor
            viewCellBG.layer.borderColor = UIColor.clear.cgColor
            viewCellBG.layer.shadowColor = UIColor.black.cgColor
            viewCellBG.layer.shadowOffset = CGSize(width: 0, height: 0)
            viewCellBG.layer.shadowOpacity = 0.4
            viewCellBG.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 8
            imgProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var viewOnlyBlack:UIView! {
        didSet {
            // viewOnlyBlack.backgroundColor = .black
            
            let gradientLayer:CAGradientLayer = CAGradientLayer()
            gradientLayer.frame.size = viewOnlyBlack.frame.size
            gradientLayer.colors =
                [UIColor.white.cgColor,UIColor.black.withAlphaComponent(1).cgColor]
              //Use diffrent colors
            viewOnlyBlack.layer.addSublayer(gradientLayer)
            
        }
    }
    
    @IBOutlet weak var viewDataSetup:UIView! {
        didSet {
            viewDataSetup.backgroundColor = .white
            
        }
    }
    @IBOutlet weak var btnStarOne:UIButton! {
        didSet {
            btnStarOne.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarTwo:UIButton! {
        didSet {
            btnStarTwo.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarThree:UIButton! {
        didSet {
            btnStarThree.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarFour:UIButton! {
        didSet {
            btnStarFour.tintColor = .systemYellow
            
        }
    }
    @IBOutlet weak var btnStarFive:UIButton! {
        didSet {
            btnStarFive.tintColor = .systemYellow
            
        }
    }
    
    @IBOutlet weak var lblShopName:UILabel! {
        didSet {
            lblShopName.textColor = .black
            lblShopName.backgroundColor = .clear
        }
    }
    @IBOutlet weak var btnPhoneNumber:UIButton! {
        didSet {
            btnPhoneNumber.setTitleColor(.black, for: .normal)
            btnPhoneNumber.backgroundColor = .clear
        }
    }
    @IBOutlet weak var lblAddress:UILabel! {
        didSet {
            lblAddress.textColor = .black   
            lblAddress.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var lblDistance:UILabel! {
        didSet {
            lblDistance.textColor = .white
            lblDistance.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            lblDistance.text = " 1.7km "
            lblDistance.layer.cornerRadius = 4
            lblDistance.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
