//
//  SpecialDealsCollectionCell.swift
//  Alien Broccoli
//
//  Created by apple on 23/06/21.
//

import UIKit

class SpecialDealsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.layer.cornerRadius = 8
            imgProfile.clipsToBounds = true
            imgProfile.isUserInteractionEnabled = false
            imgProfile.layer.borderWidth = 0.8
            imgProfile.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel! {
        didSet {
            lblTitle.text = "American Canyon"
        }
    }
    
    @IBOutlet weak var lblSubTitle:UILabel! {
        didSet {
            lblSubTitle.text = ""
        }
    }
    
    @IBOutlet weak var viewBlackBG:UIView! {
        didSet {
            viewBlackBG.layer.cornerRadius = 4
            viewBlackBG.clipsToBounds = true
            viewBlackBG.layer.borderWidth = 0.8
            viewBlackBG.layer.borderColor = UIColor.lightGray.cgColor
            viewBlackBG.backgroundColor = .white
            
        }
    }
    
    @IBOutlet weak var lblCategory:UILabel!
    
    @IBOutlet weak var btnClick:UIButton! {
        didSet {
            btnClick.layer.cornerRadius = 4
            btnClick.clipsToBounds = true
            btnClick.backgroundColor = UIColor.init(red: 192.0/255.0, green: 188.0/255.0, blue: 161.0/255.0, alpha: 1)
            btnClick.setTitleColor(.black, for: .normal)
        }
    }
    
    @IBOutlet weak var lblSpecialPrice:UILabel! {
        didSet {
            
        }
    }
    
    
    @IBOutlet weak var btnStarOne:UIButton! {
        didSet {
            btnStarOne.tintColor = .systemYellow
        }
    }
    
    @IBOutlet weak var btnStarTwo:UIButton!{
        didSet {
            btnStarTwo.tintColor = .systemYellow
        }
    }
    @IBOutlet weak var btnStarThree:UIButton!{
        didSet {
            btnStarThree.tintColor = .systemYellow
        }
    }
    @IBOutlet weak var btnStarFour:UIButton!{
        didSet {
            btnStarFour.tintColor = .systemYellow
        }
    }
    @IBOutlet weak var btnStarFive:UIButton! {
        didSet {
            btnStarFive.tintColor = .systemYellow
        }
    }
    
    @IBOutlet weak var lblAddress:UILabel!
    
}
