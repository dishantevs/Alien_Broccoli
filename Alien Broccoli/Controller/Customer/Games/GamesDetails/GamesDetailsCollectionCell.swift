//
//  GamesDetailsCollectionCell.swift
//  Alien Broccoli
//
//  Created by apple on 21/05/21.
//

import UIKit

class GamesDetailsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblFoodCategory:UILabel! {
        didSet {
            lblFoodCategory.textColor = .white
        }
    }
    
}
