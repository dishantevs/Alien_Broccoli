//
//  GamesDetails.swift
//  Alien Broccoli
//
//  Created by apple on 21/05/21.
//

import UIKit
import Alamofire
import SDWebImage

class GamesDetails: UIViewController {

    var dictGetGamesData:NSDictionary!
    
    @IBOutlet weak var lblCartCount:UILabel! {
        didSet {
            self.lblCartCount.isHidden = true
        }
    }
    
    @IBOutlet weak var indicatorr:UIActivityIndicatorView! {
        didSet {
            indicatorr.color = .white
            indicatorr.isHidden = true
        }
    }
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "GAMES"
            lblNavigationTitle.textColor = .black
        }
    }
    
    @IBOutlet weak var btnMenu:UIButton! {
        didSet {
            btnMenu.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnCart:UIButton! {
        didSet {
            btnCart.backgroundColor = .clear
            btnCart.tintColor = .black
            btnCart.isHidden = true
        }
    }
    
    @IBOutlet weak var imgImage:UIImageView!
    
    @IBOutlet weak var clViewCategories: UICollectionView! {
        didSet {
               //collection
            clViewCategories!.dataSource = self
            clViewCategories!.delegate = self
            clViewCategories!.backgroundColor = .clear
            clViewCategories.isPagingEnabled = true
        }
    }
    
    @IBOutlet weak var viewBGDown:UIView! {
        didSet {
            viewBGDown.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }
    
    @IBOutlet weak var btnSaveAndContinue:UIButton! {
        didSet {
            btnSaveAndContinue.backgroundColor = .black
            btnSaveAndContinue.layer.cornerRadius = 6
            btnSaveAndContinue.clipsToBounds = true
            btnSaveAndContinue.setTitle("PLAY GAME", for: .normal)
            btnSaveAndContinue.setTitleColor(.white, for: .normal)
        }
    }
    
    // MARK:- ARRAY -
    var arrListOfAllCategories:NSMutableArray! = []
    var arrListOfAllCategoriesName:NSMutableArray! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnMenu.addTarget(self, action: #selector(sidebackBarMenuClick), for: .touchUpInside)
        self.btnCart.addTarget(self, action: #selector(cartClickMwthod), for: .touchUpInside)
        
        self.gradientNavigation()
        
        // self.sideBarMenuClick()
        
        // print(self.dictGetGamesData as Any)
        
        var ar : NSArray!
        ar = (self.dictGetGamesData["category"] as! Array<Any>) as NSArray
        self.arrListOfAllCategories.addObjects(from: ar as! [Any])
        
        // print(self.arrListOfAllCategories as Any)
        
        for indexx in 0..<self.arrListOfAllCategories.count {
            
            let item = self.arrListOfAllCategories[indexx] as? [String:Any]
            print(item as Any)
            
             self.arrListOfAllCategoriesName.add(item!["name"] as! String)
        }
        
        print(self.arrListOfAllCategoriesName as Any)
        
        self.imgImage.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
        self.imgImage.sd_setImage(with: URL(string: (self.dictGetGamesData["image"] as! String)), placeholderImage: UIImage(named: "logo"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // self.categoriesWB()
        
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
    }
    
    @objc func cartClickMwthod() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CTotalCartListId") as? CTotalCartList
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    @objc func sidebackBarMenuClick() {
        
        self.navigationController?.popViewController(animated: true)
        
        /*
        if revealViewController() != nil {
            btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
               
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        */
    }
    
    
    
    // MARK:- WEBSERVICE ( TOTAL ITEMS IN CART ) -
    @objc func totalItemsInCart() {
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
            
        let params = CartList(action: "getcarts",
                              userId: String(myString))
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            let x : Int = JSON["TotalCartItem"] as! Int
                            let myString = String(x)
                            
                            if myString == "0" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else if myString == "" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else {
                                self.lblCartCount.isHidden = false
                                self.lblCartCount.text = String(myString)
                                
                                self.btnCart.isHidden = false
                                self.btnCart.setImage(UIImage(systemName: "cart.fill"), for: .normal)
                            }
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        self.indicatorr.stopAnimating()
                        self.indicatorr.hidesWhenStopped = true
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
}
    
    
    
    
}


extension GamesDetails: UICollectionViewDelegate {
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrListOfAllCategoriesName.count
    }
    
    //Write Delegate Code Here
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gamesDetailsCollectionCell", for: indexPath as IndexPath) as! GamesDetailsCollectionCell
           
        /*
         SubCat =             (
         );
         id = 1;
         image = "";
         name = "CBD OIL";
         */
        
        // MARK:- CELL CLASS -
        cell.layer.cornerRadius = 16
        cell.clipsToBounds = true
        cell.backgroundColor = .white
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.borderWidth = 0.6
        
        cell.backgroundColor = .systemOrange
        
        // cell.viewCellBG.backgroundColor = .black //.systemGreen // APP_BUTTON_COLOR
        
        // self.arrListOfAllCategoriesName
        let item = arrListOfAllCategories[indexPath.row] as? [String:Any]
        
        cell.lblFoodCategory.text = (item!["name"] as! String)
        
        return cell
    }
   
    
}

extension GamesDetails: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        /*let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GamesId")
        self.navigationController?.pushViewController(push, animated: true)*/
    }
    
    
    
    
}

extension GamesDetails: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var sizes: CGSize
                
        let result = UIScreen.main.bounds.size
        //NSLog("%f",result.height)
        
        
        if result.height == 480 {
            sizes = CGSize(width: 190, height: 220)
        }
        else if result.height == 568 {
            sizes = CGSize(width: 190, height: 220)
        }
        else if result.height == 667.000000 // 8
        {
            sizes = CGSize(width: 180, height: 220)
        }
        else if result.height == 736.000000 // 8 plus
        {
            sizes = CGSize(width: 180, height: 180)
        }
        else if result.height == 812.000000 // 11 pro
        {
            sizes = CGSize(width: 180, height: 220)
        }
        else if result.height == 896.000000 // 11 , 11 pro max
        {
            sizes = CGSize(width: 180, height: 220)
        }
        else
        {
            sizes = CGSize(width: self.view.frame.size.width, height: 350)
        }
        
        
        return sizes
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        let result = UIScreen.main.bounds.size
        if result.height == 667.000000 { // 8
            return 2
        } else if result.height == 812.000000 { // 11 pro
            return 4
        } else {
            return 10
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        // var sizes: CGSize
                
        let result = UIScreen.main.bounds.size
        if result.height == 667.000000 { // 8
            return UIEdgeInsets(top: 20, left: 4, bottom: 10, right: 4)
        } else if result.height == 736.000000 { // 8 plus
            return UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20)
        } else if result.height == 896.000000 { // 11 plus
            return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        } else if result.height == 812.000000 { // 11 pro
            return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        } else {
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }
        
        
            
    }
    
}


 

