//
//  GamesCollectionCell.swift
//  Alien Broccoli
//
//  Created by apple on 21/05/21.
//

import UIKit

class GamesCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 6
            viewCellBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet weak var imgImage:UIImageView!
}
