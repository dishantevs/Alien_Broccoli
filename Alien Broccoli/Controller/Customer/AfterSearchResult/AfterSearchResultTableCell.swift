//
//  AfterSearchResultTableCell.swift
//  Alien Broccoli
//
//  Created by Apple on 29/12/20.
//

import UIKit

class AfterSearchResultTableCell: UITableViewCell {

    @IBOutlet weak var viewiewBG:UIView! {
        didSet {
            viewiewBG.layer.cornerRadius = 6
            viewiewBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var imgBGProfile:UIImageView!
    
    @IBOutlet weak var lblDistance:UILabel! {
        didSet {
            lblDistance.layer.cornerRadius = 6
            lblDistance.clipsToBounds = true
            lblDistance.backgroundColor = .systemGreen
            lblDistance.text = " 1.7 km "
            lblDistance.textColor = .black
        }
    }
    
    @IBOutlet weak var btnLike:UIButton!
    @IBOutlet weak var btnShare:UIButton!
    
    @IBOutlet weak var starOne:UIButton!
    @IBOutlet weak var starTwo:UIButton!
    @IBOutlet weak var starThree:UIButton!
    @IBOutlet weak var starFour:UIButton!
    @IBOutlet weak var starFive:UIButton!
    
    @IBOutlet weak var lblStoreName:UILabel!
    @IBOutlet weak var btnStoreNumber:UIButton!
    @IBOutlet weak var lblStoreAddress:UILabel! {
        didSet {
            lblStoreAddress.textColor = .white
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
