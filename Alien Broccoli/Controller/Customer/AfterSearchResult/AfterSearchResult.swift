//
//  AfterSearchResult.swift
//  Alien Broccoli
//
//  Created by Apple on 29/12/20.
//

import UIKit

class AfterSearchResult: UIViewController {

    let cellReuseIdentifier = "afterSearchResultTableCell"
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CANNABIS"
            lblNavigationTitle.textColor = .black
        }
    }
    
    @IBOutlet weak var btnMenu:UIButton! {
        didSet {
            btnMenu.tintColor = .white
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
     
    
}

extension AfterSearchResult: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10 // arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:AfterSearchResultTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! AfterSearchResultTableCell
          
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: cell.imgBGProfile.frame.width, height: cell.imgBGProfile.frame.height)
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.black.withAlphaComponent(1).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        cell.imgBGProfile.layer.addSublayer(gradientLayer)
        
        /*let item = self.arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        
        cell.lblProductDetails.text    = (item!["productName"] as! String)
         
        let x2 : Int = (item!["quantity"] as! Int)
        let myString2 = String(x2)
        
        let x22 : Int = (item!["productPrice"] as! Int)
        let myString22 = String(x22)
        
        cell.lblProductQuantityAndPrice.text = "Quantity : "+myString2+"\nPrice : $ "+myString22
        
        
        cell.imgProductImage.sd_setImage(with: URL(string: (item!["productImage"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        /*
         productId = 12;
         productImage = "";
         productName = "Green Roads Fruit and Hemp CBD Chews";
         productPrice = 80;
         productSKU = 76766;
         quantity = 1;
         userId = 71;
         */
        
        cell.btnDeleteItem.tag = indexPath.row
        cell.btnDeleteItem.addTarget(self, action: #selector(deleteCartFromItems), for: .touchUpInside)
        */
        
        return cell
        
    }

    @objc func deleteCartFromItems(_ sender:UIButton) {
        
        /*let item = self.arrListOfAllMyOrders[sender.tag] as? [String:Any]
        // print(item as Any)
        
        let alert = UIAlertController(title: String("Delete Item"), message: String("Are you sure you want to delete.")+"' "+(item!["productName"] as! String)+" '"+" from your cart ?",
                                      
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes, Delete", style: .default, handler: { action in
             
            let x : Int = (item!["productId"] as! Int)
            let myString = String(x)
            
            self.deleteItemFromCart(strinproductId: myString)
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
             
        }))
        
        self.present(alert, animated: true, completion: nil)*/
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AfterSearchResultDetailsId") as? AfterSearchResultDetails
        self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 230
    }
    
}

extension AfterSearchResult: UITableViewDelegate {
    
}
