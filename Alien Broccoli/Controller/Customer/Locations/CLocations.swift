//
//  CLocations.swift
//  Alien Broccoli
//
//  Created by Apple on 04/11/20.
//

import UIKit

import MapKit
import CoreLocation

import GoogleMaps

import Alamofire

class CLocations: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, GMSMapViewDelegate {

    var locationManager:CLLocationManager!
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var myDouble12:Double!
    var  myDouble22 : Double!
    @IBOutlet weak var mapView:MKMapView!
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "LOCATION"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = .white
            btnBack.setImage(UIImage(named: "menu"), for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.gradientNavigation()
        self.sideBarMenuClick()
        
        self.allLocationWB()
        
        
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
    }
    
    @objc func sidebackBarMenuClick() {
        
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
               
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    func createMapView() {
       
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
            // my default lat long
            let latDef = person["latitude"]
            let myFloatLatDef = (latDef as! NSString).doubleValue
        
            let longDef = person["longitude"]
            let myFloatLongDef = (longDef as! NSString).doubleValue
            
            // google maps
            let camera = GMSCameraPosition.camera(withLatitude: myFloatLatDef, longitude: myFloatLongDef, zoom: 2.0)
            let mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: self.view.frame.size.height), camera: camera)
            mapView.delegate = self
            self.view.addSubview(mapView)
        
        
            for locationCheckIndex in 0..<arrListOfAllMyOrders.count {
            
                let item = self.arrListOfAllMyOrders[locationCheckIndex] as? [String:Any]
            
            // test lat : -33.86
            // test long : 151.20
            
                let lat = item!["latitude"]
                let myFloatLat = (lat as! NSString).doubleValue
            
                let long = item!["longitude"]
                let myFloatLong = (long as! NSString).doubleValue
            
            
            // Creates a marker in the center of the map.
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: myFloatLat, longitude: myFloatLong)
                marker.title = (item!["location"] as! String)
            
            // marker.snippet = "Australia"
                marker.map = mapView
            
            }
            
        }
        
    }
    
    
    // MARK:- WEBSERVICE - ( ALL LOCATIONS ) -
    @objc func allLocationWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "fetching...")
            
        self.view.endEditing(true)
        
                
        let params = Locations(action: "location")
            
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                    switch response.result {
                    case let .success(value):
                            
                        let JSON = value as! NSDictionary
                        // print(JSON as Any)
                            
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                               
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            self.createMapView()
 
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                                
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                                
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                        }
                            
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                            
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
            
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        
        
    }
   
    
}
