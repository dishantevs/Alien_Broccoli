//
//  Cannabies.swift
//  Alien Broccoli
//
//  Created by apple on 24/05/21.
//

import UIKit
import Alamofire
import SDWebImage

class Cannabies: UIViewController, UITextFieldDelegate {

    var ar2 : NSArray!
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CANNABIS"
            lblNavigationTitle.textColor = .black
        }
    }
    
    @IBOutlet weak var txtSearch:UITextField! {
        didSet {
            txtSearch.layer.cornerRadius = 6
            txtSearch.clipsToBounds = true
            txtSearch.setLeftPaddingPoints(40)
            txtSearch.layer.borderColor = UIColor.clear.cgColor
            txtSearch.layer.borderWidth = 0.8
            txtSearch.attributedPlaceholder = NSAttributedString(string: "search...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtSearch.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var txtLocation:UITextField! {
        didSet {
            txtLocation.layer.cornerRadius = 6
            txtLocation.clipsToBounds = true
            txtLocation.setLeftPaddingPoints(40)
            txtLocation.layer.borderColor = UIColor.clear.cgColor
            txtLocation.layer.borderWidth = 0.8
            txtLocation.attributedPlaceholder = NSAttributedString(string: "location...",
                                         attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
            txtLocation.backgroundColor = .white
        }
    }
    
    // MARK:- COLLECTION VIEW SETUP -
    @IBOutlet weak var clViewForStaticButton: UICollectionView! {
        didSet {
            // clViewForStaticButton!.dataSource = self
            // clViewForStaticButton!.delegate = self
            clViewForStaticButton!.backgroundColor = .clear
            clViewForStaticButton.isPagingEnabled = false
        }
    }
    
    @IBOutlet weak var btnMenu:UIButton! {
        didSet {
            btnMenu.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnCart:UIButton! {
        didSet {
            btnCart.backgroundColor = .clear
            btnCart.tintColor = .black
            btnCart.isHidden = true
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    @IBOutlet weak var viewScrollBG:UIView!
    
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    let dummyImageArray = ["hd1" , "hd2" , "hd3" , "hd4"]
    
    var staticSixArray = ["Dispensaries", "Deliveries", "Maps", "Brands", "Products", "Specials"]
    
    @IBOutlet weak var btnLeftScroll:UIButton! {
        didSet {
            btnLeftScroll.tintColor = .white
            btnLeftScroll.layer.cornerRadius = 8
            btnLeftScroll.clipsToBounds = true
            btnLeftScroll.layer.masksToBounds = false
            btnLeftScroll.layer.cornerRadius = 8
            btnLeftScroll.layer.backgroundColor = UIColor.black.cgColor
            btnLeftScroll.layer.borderColor = UIColor.clear.cgColor
            btnLeftScroll.layer.shadowColor = UIColor.black.cgColor
            btnLeftScroll.layer.shadowOffset = CGSize(width: 0, height: 0)
            btnLeftScroll.layer.shadowOpacity = 0.4
            btnLeftScroll.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var btnRightScroll:UIButton! {
        didSet {
            btnRightScroll.tintColor = .white
            btnRightScroll.layer.cornerRadius = 8
            btnRightScroll.clipsToBounds = true
            btnRightScroll.layer.masksToBounds = false
            btnRightScroll.layer.cornerRadius = 8
            btnRightScroll.layer.backgroundColor = UIColor.black.cgColor
            btnRightScroll.layer.borderColor = UIColor.clear.cgColor
            btnRightScroll.layer.shadowColor = UIColor.black.cgColor
            btnRightScroll.layer.shadowOffset = CGSize(width: 0, height: 0)
            btnRightScroll.layer.shadowOpacity = 0.4
            btnRightScroll.layer.shadowRadius = 4
        }
    }
    
    @IBOutlet weak var scrollViewBanner:UIScrollView! {
        didSet {
            scrollViewBanner.backgroundColor = .systemTeal
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.screenSize = UIScreen.main.bounds
        self.screenWidth = screenSize.width
        self.screenHeight = screenSize.height
        
        self.btnMenu.addTarget(self, action: #selector(sidebackBarMenuClick), for: .touchUpInside)
        // self.btnCart.addTarget(self, action: #selector(cartClickMwthod), for: .touchUpInside)
        
        self.tbleView.separatorColor = .clear
        
        self.gradientNavigation()
        
        self.staticSixButtonsCreate()
        
        
        // LISTING WEBSERVICE CALLS HERE
        self.categoriesWB()
    }
    
    
    
    @objc func staticSixButtonsCreate() {
        self.clViewForStaticButton!.dataSource = self
        self.clViewForStaticButton!.delegate = self
        
        self.bannerScrollable()
        
    }
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
    }
    
    @objc func sidebackBarMenuClick() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func bannerScrollable() {
        
        self.scrollViewBanner.frame =  CGRect(x: 0, y: 0, width:self.viewScrollBG.frame.size.width , height: self.viewScrollBG.frame.size.height)
        self.scrollViewBanner.backgroundColor = .clear
        self.scrollViewBanner.showsHorizontalScrollIndicator = false
        self.scrollViewBanner.isPagingEnabled = true
        self.viewScrollBG.addSubview(self.scrollViewBanner)
        
        
        
        for index in 0..<self.dummyImageArray.count {
            
            frame.origin.x = self.scrollViewBanner.frame.size.width * CGFloat(index)
            frame.size = self.scrollViewBanner.frame.size
            
            let imageView = UIImageView()
            imageView.image = UIImage(named: self.dummyImageArray[index])
            imageView.frame = frame
            
            /*CGRect(x: 0, y: 0, width:
                                        self.scrollViewBanner.frame.width, height: self.scrollViewBanner.frame.height)*/
            self.scrollViewBanner.contentSize.width = self.scrollViewBanner.frame.width * CGFloat(index + 1)
            self.scrollViewBanner.addSubview(imageView)
            
        }
        
        // self.scrollViewBanner.contentSize = CGSize(width: self.scrollViewBanner.frame.size.width * CGFloat(self.arrAddQuestions.count), height: self.scrollViewBanner.frame.size.height)
        self.scrollViewBanner.delegate = self
        
        self.btnLeftScroll.addTarget(self, action: #selector(leftButtonTapped), for: .touchUpInside)
        self.btnRightScroll.addTarget(self, action: #selector(rightButtonTapped), for: .touchUpInside)
        
    }
    
    @objc func leftButtonTapped() {
         if self.scrollViewBanner.contentOffset.x > 0 {
            self.scrollViewBanner.contentOffset.x -=  self.viewScrollBG.bounds.width
         }
    }
    
    @objc func rightButtonTapped() {
        
        if self.scrollViewBanner.contentOffset.x < self.viewScrollBG.bounds.width * CGFloat(self.dummyImageArray.count-1) {
            self.scrollViewBanner.contentOffset.x +=  self.viewScrollBG.bounds.width
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        
        // print(Int(self.scrollViewBanner.contentOffset.x / CGFloat(4)))
    }
    
    // MARK:- WEBSERVICE ( CATEGORIES ) -
    @objc func categoriesWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        let forgotPasswordP = CannabisWB(action: "cannabas")
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: forgotPasswordP,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // print(self.arrListOfAllMyOrders as Any)
                            
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
        
    }
}



// MARK:- COLLECTION VIEW -
extension Cannabies: UICollectionViewDelegate {
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.clViewForStaticButton {
            
            return self.staticSixArray.count
            
        } else {
            
            let index = collectionView.tag
            let item = self.arrListOfAllMyOrders[index] as? [String:Any]
            // print(item as Any)
            
            var ar : NSArray!
            ar = (item!["data"] as! Array<Any>) as NSArray
            return ar.count
            
        }
        
        
        
    }
    
    //Write Delegate Code Here
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.clViewForStaticButton {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cannabiesStaticCollectionCell", for: indexPath as IndexPath) as! CannabiesStaticCollectionCell
            
            cell.lblTitle.text = self.staticSixArray[indexPath.row]
            cell.lblTitle.textColor = .white
            cell.backgroundColor = .black
            cell.layer.cornerRadius = 4
            cell.clipsToBounds = true
            
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cannibisCollectionCell", for: indexPath as IndexPath) as! CannibisCollectionCell
            
            /*print(indexPath.row as Any)
            print(indexPath.item as Any)
            print(indexPath.section as Any)*/
            
            let index = collectionView.tag
            let item = self.arrListOfAllMyOrders[index] as? [String:Any]
            
            ar2 = (item!["data"] as! Array<Any>) as NSArray
           
            let item2 = ar2[indexPath.row] as? [String:Any]
           
            
            if (item!["name"] as! String) == "Feature Brand" {
                
                cell.lblTitle.text = (item2!["name"] as! String)
                cell.lblTitle.textColor = .black
                cell.lblTitle.textAlignment = .left
                cell.lblTitle.backgroundColor = .clear
                cell.lblTitle.font = UIFont.init(name: "Avenir Next Bold", size: 18)
                
                if (item2!["followers"] as! String) == "0" {
                    cell.lblSubTitle.text = "0"+" follower"
                } else if (item2!["followers"] as! String) == "1" {
                    cell.lblSubTitle.text = "1"+" follower"
                } else if (item2!["followers"] as! String) == "" {
                    cell.lblSubTitle.text = "0"+" follower"
                } else {
                    cell.lblSubTitle.text = (item2!["followers"] as! String)+" followers"
                }
            
                cell.lblSubTitle.textAlignment = .left
                
                cell.lblTitle.textColor = .black
                cell.lblSubTitle.textColor = .lightGray
                
            } else if (item!["name"] as! String) == "Storefronts" {
                
                cell.lblTitle.text  = (item2!["name"] as! String)
                
                if (item2!["price"] as! String) == "" {
                    
                    cell.lblSubTitle.text = "$0"
                    
                } else if (item2!["price"] as! String) == "0" {
                    
                    cell.lblSubTitle.text = "$0"
                    
                } else {
                    
                    cell.lblSubTitle.text = (item2!["price"] as! String)
                    
                }
                
                cell.lblTitle.textColor = .lightGray
               
                cell.lblSubTitle.font = UIFont.init(name: "Avenir Next Bold", size: 22)
                cell.lblSubTitle.textColor = .black
                cell.lblSubTitle.textAlignment = .center
                
                cell.viewBlackBG.backgroundColor = .white
                cell.viewBlackBG.layer.borderWidth = 0.8
                cell.viewBlackBG.layer.borderColor = UIColor.white.cgColor
                
            } else if (item!["name"] as! String) == "CBD Stores" {
                
                cell.lblTitle.text = "California | 7 min"//(item!["address"] as! String)
                cell.lblTitle.textColor = .lightGray
                cell.lblTitle.textAlignment = .left
                cell.lblTitle.backgroundColor = .clear
                cell.lblTitle.font = UIFont.init(name: "Avenir Next Regular", size: 12)
                
                cell.lblSubTitle.text = (item2!["name"] as! String) // "Recreational"
                cell.lblSubTitle.font = UIFont.init(name: "Avenir Next Bold", size: 16)
                cell.lblSubTitle.textColor = .black
                cell.lblSubTitle.textAlignment = .left
                
                cell.lblCategory.text = (item2!["address"] as! String)
                cell.lblCategory.textAlignment = .left
                cell.lblCategory.textColor = .lightGray
                cell.lblCategory.font = UIFont.init(name: "Avenir Next Regular", size: 12)
                
                if (item2!["rating"] as! String) == "0" {
                    
                    cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                    
                } else if (item2!["rating"] as! String) == "1" {
                    
                    cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                    
                } else if (item2!["rating"] as! String) == "2" {
                    
                    cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                    
                } else if (item2!["rating"] as! String) == "3" {
                    
                    cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                    
                } else if (item2!["rating"] as! String) == "4" {
                    
                    cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                    
                } else if (item2!["rating"] as! String) == "5" {
                    
                    cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    cell.btnStarFive.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    
                } else {
                    
                    cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                    cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                    
                }
                
                cell.viewBlackBG.backgroundColor = .white
                cell.viewBlackBG.layer.borderWidth = 0.8
                cell.viewBlackBG.layer.borderColor = UIColor.white.cgColor
                
            } else if (item!["name"] as! String) == "Deals Nearby" {
                
                cell.lblTitle.text  = (item2!["name"] as! String)
                
                if item2!["price"] is String {
                                  
                    print("Yes, it's a String")
                  
                    cell.lblSubTitle.text = "$ "+(item2!["price"] as! String)

                } else if item2!["price"] is Int {
                  
                    print("It is Integer")
                  
                    let x2 : Int = (item2!["price"] as! Int)
                    let myString2 = String(x2)
                    cell.lblSubTitle.text = "$ "+myString2
                  
                } else {
                //some other check
                  print("i am ")
                  
                    let temp:NSNumber = item2!["price"] as! NSNumber
                    let tempString = temp.stringValue
                    cell.lblSubTitle.text = "$ "+tempString
                  
                }
                
                cell.lblTitle.font = UIFont.init(name: "Avenir Next Regular", size: 16)
                cell.lblTitle.textColor = .lightGray
               
                cell.lblSubTitle.font = UIFont.init(name: "Avenir Next Bold", size: 22)
                cell.lblSubTitle.textColor = .black
                cell.lblSubTitle.textAlignment = .center
                
                cell.viewBlackBG.backgroundColor = .white
                cell.viewBlackBG.layer.borderWidth = 0.8
                cell.viewBlackBG.layer.borderColor = UIColor.white.cgColor
                
            }
            
            cell.viewBlackBG.backgroundColor = .white
            cell.viewBlackBG.layer.borderWidth = 0.8
            cell.viewBlackBG.layer.borderColor = UIColor.white.cgColor
            
            cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.whiteLarge
            cell.imgProfile.sd_setImage(with: URL(string: (item2!["image"] as! String)), placeholderImage: UIImage(named: "logo"))
            cell.imgProfile.backgroundColor = .clear
            
            cell.backgroundColor = .white
            
            return cell
            
        }
        
       
    }
}

extension Cannabies: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("clicked")
        
        if collectionView == self.clViewForStaticButton {
            
            if self.staticSixArray[indexPath.row] == "Dispensaries" {
                
                /*let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MAPSId")
                self.navigationController?.pushViewController(push, animated: true)*/
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreListId")
                self.navigationController?.pushViewController(push, animated: true)
                
            } else if self.staticSixArray[indexPath.row] == "Deliveries" {
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DeliveriesId")
                self.navigationController?.pushViewController(push, animated: true)
                
            } else if self.staticSixArray[indexPath.row] == "Brands" {
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BrandsListId")
                self.navigationController?.pushViewController(push, animated: true)
                
            } else if self.staticSixArray[indexPath.row] == "Products" {
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProductsAllListId")
                self.navigationController?.pushViewController(push, animated: true)
                
            } else if self.staticSixArray[indexPath.row] == "Specials" {
                
                let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SpecialsDealsId")
                self.navigationController?.pushViewController(push, animated: true)
                
            }
            
        } else {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CannabiesProductsId") as? CannabiesProducts
            let index = collectionView.tag
            let item = self.arrListOfAllMyOrders[index] as? [String:Any]
            
            ar2 = (item!["data"] as! Array<Any>) as NSArray
           
            let item2 = ar2[indexPath.row] as? [String:Any]
            
            let x2 : Int = (item2!["id"] as! Int)
            let myString2 = String(x2)
            push!.brandIdIs = myString2
            
            push!.headerName = (item!["name"] as! String)
            
            self.navigationController?.pushViewController(push!, animated: true)
            
        }
        
        
    }
    
}

extension Cannabies: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
      
        /*var sizes: CGSize
                    
        let index = collectionView.tag
        let item = self.arrListOfAllMyOrders[index] as? [String:Any]
        
        if (item!["name"] as! String) == "Feature Brand" {
            
            sizes = CGSize(width: 200, height: 230)
            
        } else if (item!["name"] as! String) == "Storefronts" {
            
            sizes = CGSize(width: 200, height: 230)
            
        } else if (item!["name"] as! String) == "Storefonts" {
            
            sizes = CGSize(width: 200, height: 230)
            
        } else if (item!["name"] as! String) == "CBD Stores" {
            
            sizes = CGSize(width: 200, height: 282)
            
        } else if (item!["name"] as! String) == "products" {
            
            sizes = CGSize(width: 200, height: 230)
            
        } else if (item!["name"] as! String) == "Deals Nearby" {
            
            sizes = CGSize(width: 200, height: 230)
            
        } else {
            
            sizes = CGSize(width: 200, height: 272)
            
        }*/
           
        if collectionView == self.clViewForStaticButton {
            
            var sizes: CGSize
                    
            sizes = CGSize(width: 100, height: 40)
            
            return sizes
            
        } else {
            
            let index = collectionView.tag
            let item = self.arrListOfAllMyOrders[index] as? [String:Any]
            
            var sizes: CGSize
                        
            if (item!["name"] as! String) == "CBD Stores" {
                sizes = CGSize(width: 190, height: 320)
            } else {
                sizes = CGSize(width: 190, height: 252)
            }
            
            
            return sizes
            
        }
        
        
        
 
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
            let result = UIScreen.main.bounds.size
            if result.height == 667.000000 { // 8
                return 2
            } else if result.height == 812.000000 { // 11 pro
                return 4
            } else {
                return 10
            }
            
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        
        if collectionView == self.clViewForStaticButton {
            return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        } else {
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        }
        
        
    }
   
}



extension Cannabies: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrListOfAllMyOrders.count
        
        /*let item = self.arrListOfAllMyOrders[section] as? [String:Any]
        print(item as Any)
        
        var ar : NSArray!
        ar = (item!["data"] as! Array<Any>) as NSArray
        
        return ar.count*/
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if let cell = cell as? CannabiesTableCell {

            cell.clView.dataSource = self
            cell.clView.delegate = self
            cell.clView.tag = indexPath.section
            cell.clView.reloadData()

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         // return 10 // arrListOfAllMyOrders.count
        
        
        /*let item = self.arrListOfAllMyOrders[section] as? [String:Any]
        print(item as Any)
        
        var ar : NSArray!
        ar = (item!["data"] as! Array<Any>) as NSArray
        
        return ar.count*/
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:CannabiesTableCell = tableView.dequeueReusableCell(withIdentifier: "cannabiesTableCell") as! CannabiesTableCell
          
        /*let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: 0, y: 0, width: cell.imgBGProfile.frame.width, height: cell.imgBGProfile.frame.height)
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.black.withAlphaComponent(1).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        cell.imgBGProfile.layer.addSublayer(gradientLayer)*/
        
         /*let item = self.arrListOfAllMyOrders[indexPath.section] as? [String:Any]
         print(item as Any)
        
         var ar : NSArray!
         ar = (item!["data"] as! Array<Any>) as NSArray
        
         let item1 = ar[indexPath.row] as? [String:Any]
        
         cell.lblTitle.text = (item1!["name"] as! String)*/
        
        // cell.accessoryType = .disclosureIndicator
        
        

        cell.clView.tag = indexPath.row
        
        return cell
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        // let item = self.arrListOfAllMyOrders[indexPath.section] as? [String:Any]
        // var ar : NSArray!
        // ar = (item!["SubCat"] as! Array<Any>) as NSArray
        
        // let item1 = ar[indexPath.row] as? [String:Any]
        
        // let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ConversationStartersDetailsId") as? ConversationStartersDetails
        // push!.dictGetConversationStarters = item1 as NSDictionary?
        // self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        /*let index = collectionView.tag
        let item = self.arrListOfAllMyOrders[index] as? [String:Any]*/
        
        // else if (item!["name"] as! String) == "CBD Stores" {
        
        if indexPath.section == 2 {
            return 326
        } else {
            return 262
        }
        
        
    }
    
}

extension Cannabies: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? CannabiesTableCell {
            // cachedPosition[indexPath] = cell.collectionView.contentOffset
            print(cell.clView.tag as Any)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         let item = self.arrListOfAllMyOrders[section] as? [String:Any]
         // print(item as Any)
        
        // if section == 0
        
        let headerView = UIView.init(frame: CGRect.init(x: 60, y: 0, width: tableView.frame.width, height: 50))

        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width, height: headerView.frame.height)
        
        label.text = "  "+(item!["name"] as! String)
        label.backgroundColor = .white
        label.layer.cornerRadius = 0
        label.clipsToBounds = true
        label.font = UIFont.init(name: "Avenir Next Bold", size: 16)
        label.textColor = .black
        label.textAlignment = .left
        
        let button = UIButton()
        button.frame = CGRect.init(x: 120, y: 0, width: headerView.frame.width-140, height: headerView.frame.height)
        button.setTitle("view more", for: .normal)
        button.setTitleColor(.systemBlue, for: .normal)
        button.titleLabel?.font =  UIFont(name: "Avenir Next Bold", size: 12)
        button.backgroundColor = .white
        button.layer.cornerRadius = 0
        button.clipsToBounds = true
        
        button.tag = section
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        
        button.contentHorizontalAlignment = .right
        
        headerView.addSubview(label)
        headerView.addSubview(button)
        
        return headerView
    }
    
    @objc func buttonTapped(sender: UIButton) {
        let btn:UIButton = sender
        
        print(btn.tag as Any)
        //
        if btn.tag == 0 {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BrandsListId")
            self.navigationController?.pushViewController(push, animated: true)
            
        } else if btn.tag == 1 {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreListId") as? StoreList
            push!.strViewMoreSectionNumber = "1"
            self.navigationController?.pushViewController(push!, animated: true)
            
        } else if btn.tag == 2 {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreListId") as? StoreList
            push!.strViewMoreSectionNumber = "2"
            self.navigationController?.pushViewController(push!, animated: true)
            
        } else {
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoreListId") as? StoreList
            push!.strViewMoreSectionNumber = "3"
            self.navigationController?.pushViewController(push!, animated: true)
            
        }
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
}
