//
//  CannabiesTableCell.swift
//  Alien Broccoli
//
//  Created by apple on 24/05/21.
//

import UIKit

class CannabiesTableCell: UITableViewCell {

    @IBOutlet weak var clView:UICollectionView! {
        didSet {
            clView.isPagingEnabled = false
            clView.backgroundColor = .white
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
