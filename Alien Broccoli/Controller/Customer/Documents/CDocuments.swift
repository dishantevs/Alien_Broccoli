//
//  CDocuments.swift
//  Alien Broccoli
//
//  Created by Apple on 06/11/20.
//

import UIKit
import Alamofire

class CDocuments: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    let cellReuseIdentifier = "cDocumentsTableCell"
    
    var imgSelect:String!
    
    var imageStr1:String!
    var imgData1:Data!
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "UPLOAD DOCUMENTS"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = .white
            btnBack.setImage(UIImage(named: "menu"), for: .normal)
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.tableFooterView = UIView()
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .white
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.imgSelect = "0"
        
        self.tbleView.separatorColor = .clear
        
        self.sidebackBarMenuClick()
    }
    
    @objc func sidebackBarMenuClick() {
        
        if revealViewController() != nil {
            btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
               
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    }
}


extension CDocuments: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:CDocumentsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CDocumentsTableCell
          
        // let item = self.arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        
        cell.imgUploadImage.tag = 1
        cell.imgUploadImage2.tag = 2
        cell.imgUploadImage3.tag = 3
        
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            
            // photo id
            if person["ssnImage"] as! String == "" {
                
            } else {
                
                cell.imgUploadImage.sd_setImage(with: URL(string: (person["ssnImage"] as! String)), placeholderImage: UIImage(named: "logo"))
                
            }
            
            
            // address proof
            if person["AutoInsurance"] as! String == "" {
                
            } else {
                
                cell.imgUploadImage2.sd_setImage(with: URL(string: (person["AutoInsurance"] as! String)), placeholderImage: UIImage(named: "logo"))
                
            }
            
            // face id
            if person["drivlingImage"] as! String == "" {
                
            } else {
                
                cell.imgUploadImage3.sd_setImage(with: URL(string: (person["drivlingImage"] as! String)), placeholderImage: UIImage(named: "logo"))
                
            }
        }
      
        // image one
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(CDocuments.cellTappedMethod1(_:)))

        cell.imgUploadImage.isUserInteractionEnabled = true
        cell.imgUploadImage.addGestureRecognizer(tapGestureRecognizer1)
        
        
        
        // image two
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(CDocuments.cellTappedMethod2(_:)))

        cell.imgUploadImage2.isUserInteractionEnabled = true
        cell.imgUploadImage2.addGestureRecognizer(tapGestureRecognizer2)
        
        
        
        
        // image three
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(CDocuments.cellTappedMethod3(_:)))

        cell.imgUploadImage3.isUserInteractionEnabled = true
        cell.imgUploadImage3.addGestureRecognizer(tapGestureRecognizer3)
        
        return cell
        
    }

    @objc func cellTappedMethod1(_ sender:AnyObject) {
         print("you tap image number: \(sender.view.tag)")
        
        self.imgSelect = "1"
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera1()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery1()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    
    // two
    @objc func cellTappedMethod2(_ sender:AnyObject) {
         print("you tap image number: \(sender.view.tag)")
        
        self.imgSelect = "2"
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera1()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery1()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    // three
    @objc func cellTappedMethod3(_ sender:AnyObject) {
         print("you tap image number: \(sender.view.tag)")
        
        self.imgSelect = "3"
        
        let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
            print("User click Approve button")
            self.openCamera1()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
            print("User click Edit button")
            self.openGallery1()
        }))

        alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
            print("User click Delete button")
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    
    @objc func openCamera1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        // self.imageStrTwo = "0"
        // self.imageStrOne = "1"
        // self.imageStrThree = "0"
    }
    
    @objc func openGallery1() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
        
        // self.imageStrTwo = "0"
        // self.imageStrOne = "1"
        // self.imageStrThree = "0"
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CDocumentsTableCell
        
        // print(cell.imgUploadImage.tag)
        // print(cell.imgUploadImage2.tag)
        // print(cell.imgUploadImage3.tag)
        
        // print(self.imgSelect as Any)
        
        let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        if self.imgSelect == "1" {
            
            cell.imgUploadImage.image = image_data
            let imageData:Data = image_data!.pngData()!
            imageStr1 = imageData.base64EncodedString()
            self.dismiss(animated: true, completion: nil)
            imgData1 = image_data!.jpegData(compressionQuality: 0.2)!
            
        } else if self.imgSelect == "2" {
            
            cell.imgUploadImage2.image = image_data
            let imageData:Data = image_data!.pngData()!
            imageStr1 = imageData.base64EncodedString()
            self.dismiss(animated: true, completion: nil)
            imgData1 = image_data!.jpegData(compressionQuality: 0.2)!
            
        } else if self.imgSelect == "3" {
            
            cell.imgUploadImage3.image = image_data
            let imageData:Data = image_data!.pngData()!
            imageStr1 = imageData.base64EncodedString()
            self.dismiss(animated: true, completion: nil)
            imgData1 = image_data!.jpegData(compressionQuality: 0.2)!
            
        }
        
        self.uploadDocumentsWB()
    }
    
    
    @objc func uploadDocumentsWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Uploading...")
          //Set Your URL
           let api_url = BASE_URL_ALIEN_BROCCOLI
           guard let url = URL(string: api_url) else {
               return
           }

            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let str:String = person["role"] as! String
           
               let x : Int = person["userId"] as! Int
               let myString = String(x)
               
               
               
               var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
               urlRequest.httpMethod = "POST"
               urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")

           //Set Your Parameter
               let parameterDict = NSMutableDictionary()
               parameterDict.setValue("editprofile", forKey: "action")
               parameterDict.setValue(String(myString), forKey: "userId")

          // Now Execute
           AF.upload(multipartFormData: { multiPart in
               for (key, value) in parameterDict {
                   if let temp = value as? String {
                       multiPart.append(temp.data(using: .utf8)!, withName: key as! String)
                   }
                   if let temp = value as? Int {
                       multiPart.append("\(temp)".data(using: .utf8)!, withName: key as! String)
                   }
                   if let temp = value as? NSArray {
                       temp.forEach({ element in
                           let keyObj = key as! String + "[]"
                           if let string = element as? String {
                               multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                           } else
                               if let num = element as? Int {
                                   let value = "\(num)"
                                   multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                           }
                       })
                   }
               }
            
            if self.imgSelect == "1" {
                multiPart.append(self.imgData1, withName: "ssnImage", fileName: "editDocumentPhotoId.png", mimeType: "image/png")
            } else if self.imgSelect == "2" {
                multiPart.append(self.imgData1, withName: "AutoInsurance", fileName: "editDocumentAddressProof.png", mimeType: "image/png")
            } else if self.imgSelect == "3" {
                multiPart.append(self.imgData1, withName: "drivlingImage", fileName: "editDocumentFaceId.png", mimeType: "image/png")
            }
            
           }, with: urlRequest)
               .uploadProgress(queue: .main, closure: { progress in
                   //Current upload progress of file
                   print("Upload Progress: \(progress.fractionCompleted)")
               })
               .responseJSON(completionHandler: { data in

                          switch data.result {

                          case .success(_):
                           do {
                           
                           let dictionary = try JSONSerialization.jsonObject(with: data.data!, options: .fragmentsAllowed) as! NSDictionary
                             
                               print("Success!")
                               // print(dictionary)
                               
                            self.imgData1 = nil
                            
                            var dict: Dictionary<AnyHashable, Any>
                            dict = dictionary["data"] as! Dictionary<AnyHashable, Any>
                               
                            let defaults = UserDefaults.standard
                            defaults.setValue(dict, forKey: "keyLoginFullData")
                            ERProgressHud.sharedInstance.hide()
                               
                          }
                          catch {
                             // catch error.
                           print("catch error")
                           ERProgressHud.sharedInstance.hide()
                                 }
                           break
                               
                          case .failure(_):
                           print("failure")
                           ERProgressHud.sharedInstance.hide()
                           break
                           
                       }


               })
           
       }}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 810
    }
    
}

extension CDocuments: UITableViewDelegate {
    
}
