//
//  MAPS.swift
//  Alien Broccoli
//
//  Created by apple on 15/06/21.
//

import UIKit

import GoogleMaps
import Alamofire

class MAPS: UIViewController, GMSMapViewDelegate {
    
    let unCheckImageName = "YBTextPicker_unchecked.png"
    let checkImageName = "YBTextPicker_checked.png"
    
    // ***************************************************************** // nav
                    
        @IBOutlet weak var navigationBar:UIView! {
            didSet {
                navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            }
        }
            
        @IBOutlet weak var btnBack:UIButton! {
            didSet {
                btnBack.tintColor = .black
            }
        }
            
        @IBOutlet weak var lblNavigationTitle:UILabel! {
            didSet {
                lblNavigationTitle.text = "DISPENDARIES"
                lblNavigationTitle.textColor = .black
                lblNavigationTitle.backgroundColor = .clear
            }
        }
                    
    // ***************************************************************** // nav
    
    // MARK:- ARRAY -
    var arrListOfAllLocations:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    @IBOutlet weak var clViewForStaticButton: UICollectionView! {
        didSet {
            // clViewForStaticButton!.dataSource = self
            // clViewForStaticButton!.delegate = self
            clViewForStaticButton!.backgroundColor = .clear
            clViewForStaticButton.isPagingEnabled = false
        }
    }
    
    var staticSixArray = ["Order Online", "Open Now", "Curbside pickup", "Storefronts", "Delivery", "CBD Stores"]
    
    
    
    var arrayShowUpperCollectionViewData:NSMutableArray! = []
    
    
    
    
    
    @IBOutlet weak var btnfilter:UIButton!
    
    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    @IBOutlet weak var viewMapBG:UIView!
    @IBOutlet weak var viewFilterBG:UIView! {
        didSet {
            viewFilterBG.backgroundColor = .white
            viewFilterBG.layer.cornerRadius = 6
            viewFilterBG.clipsToBounds = true
            viewFilterBG.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            viewFilterBG.layer.borderWidth = 2
        }
    }
    
    var strStorefronts:String!
    var strDelivery:String!
    var strCBDStore:String!
    var strRecreational:String!
    var strMedical:String!
    
    var strOrderOnline:String!
    var strOpenNow:String!
    var strCurbsidePickup:String!
    var strSocialEquity:String!
    var str19Only:String!
    var str18Only:String!
    var strATM:String!
    var strVideos:String!
    var strAccessible:String!
    var str21Only:String!
    var strSecurity:String!
    
    @IBOutlet weak var btnStoreFronts:UIButton! {
        didSet {
            btnStoreFronts.backgroundColor = .clear
            btnStoreFronts.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    
    
    @IBOutlet weak var btnDelivery:UIButton!{
        didSet {
            btnDelivery.backgroundColor = .clear
            btnDelivery.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnCBDStores:UIButton!{
        didSet {
            btnCBDStores.backgroundColor = .clear
            btnCBDStores.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnRecreation:UIButton!{
        didSet {
            btnRecreation.backgroundColor = .clear
            btnRecreation.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnMedical:UIButton!{
        didSet {
            btnMedical.backgroundColor = .clear
            btnMedical.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    
    @IBOutlet weak var btnOrderOnline:UIButton!{
        didSet {
            btnOrderOnline.backgroundColor = .clear
            btnOrderOnline.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnOpenNow:UIButton!{
        didSet {
            btnOpenNow.backgroundColor = .clear
            btnOpenNow.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnCurbsidePickUp:UIButton!{
        didSet {
            btnCurbsidePickUp.backgroundColor = .clear
            btnCurbsidePickUp.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnSocialEquity:UIButton!{
        didSet {
            btnSocialEquity.backgroundColor = .clear
            btnSocialEquity.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btn19Only:UIButton!{
        didSet {
            btn19Only.backgroundColor = .clear
            btn19Only.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btn18Only:UIButton!{
        didSet {
            btn18Only.backgroundColor = .clear
            btn18Only.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnATM:UIButton!{
        didSet {
            btnATM.backgroundColor = .clear
            btnATM.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnVideos:UIButton!{
        didSet {
            btnVideos.backgroundColor = .clear
            btnVideos.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnAccessible:UIButton!{
        didSet {
            btnAccessible.backgroundColor = .clear
            btnAccessible.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btn21only:UIButton!{
        didSet {
            btn21only.backgroundColor = .clear
            btn21only.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    @IBOutlet weak var btnSecurity:UIButton!{
        didSet {
            btnSecurity.backgroundColor = .clear
            btnSecurity.setImage(UIImage(named: unCheckImageName), for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = .white
        
        
        
        self.clViewForStaticButton!.dataSource = self
        self.clViewForStaticButton!.delegate = self
        
        for index in 0...5 {
            
            let someDict:[String:String] = [
                "name":String(staticSixArray[index]),
                "status":"0"
            ]
            
            self.arrayShowUpperCollectionViewData.add(someDict)
            
        }
        
        print(self.arrayShowUpperCollectionViewData as Any)
        
        self.viewFilterBG.isHidden = true
        self.btnfilter.tag = 0
        self.btnfilter.addTarget(self, action: #selector(filterClickMethod), for: .touchUpInside)
        
        self.manageData(strOrderOnlineData: "0",
                        strOpenNowData: "0",
                        strCurbsidePickup: "0",
                        strStorefronts: "0",
                        strDelivery: "0",
                        strCBDStores: "0")
        
         
    }
    
    @objc func filterClickMethod() {
        
        if self.btnfilter.tag == 0 {
            
            // self.viewFilterBG.isHidden = false
            
            UIView.transition(with: self.viewFilterBG, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.viewFilterBG.isHidden = false
                              })
                        
            self.btnfilter.tag = 1
            
        } else {
            
            UIView.transition(with: self.viewFilterBG, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.viewFilterBG.isHidden = true
                              })
            
            
            
            self.btnfilter.tag = 0
        }
        
        
    }
    
    @objc func manageData(strOrderOnlineData:String,
                          strOpenNowData:String,
                          strCurbsidePickup:String,
                          strStorefronts:String,
                          strDelivery:String,
                          strCBDStores:String) {
       
        self.strStorefronts = String(strStorefronts)
        self.strDelivery = String(strDelivery)
        self.strCBDStore = String(strCBDStores)
        self.strRecreational = "0"
        self.strMedical = "0"
        
        self.strOrderOnline = String(strOrderOnlineData)
        self.strOpenNow = String(strOpenNowData)
        self.strCurbsidePickup = String(strCurbsidePickup)
        self.strSocialEquity = "0"
        self.str19Only = "0"
        self.str18Only = "0"
        self.strATM = "0"
        self.strVideos = "0"
        self.strAccessible = "0"
        self.str21Only = "0"
        self.strSecurity = "0"
        
        self.btnStoreFronts.tag = 0
        self.btnDelivery.tag = 0
        self.btnCBDStores.tag = 0
        self.btnRecreation.tag = 0
        self.btnMedical.tag = 0
        
        self.btnOrderOnline.tag = 0
        self.btnOpenNow.tag = 0
        self.btnCurbsidePickUp.tag = 0
        self.btnSocialEquity.tag = 0
        self.btn19Only.tag = 0
        self.btn18Only.tag = 0
        self.btnATM.tag = 0
        self.btnVideos.tag = 0
        self.btnAccessible.tag = 0
        self.btn21only.tag = 0
        self.btnSecurity.tag = 0
        
        
        
        self.btnStoreFronts.addTarget(self, action: #selector(storeFrontClickMethod), for: .touchUpInside)
        self.btnDelivery.addTarget(self, action: #selector(deliveryClickMethod), for: .touchUpInside)
        self.btnCBDStores.addTarget(self, action: #selector(cbdStoreClickMethod), for: .touchUpInside)
        self.btnRecreation.addTarget(self, action: #selector(recreationalClickMethod), for: .touchUpInside)
        self.btnMedical.addTarget(self, action: #selector(medicalClickMethod), for: .touchUpInside)
        
        self.btnOrderOnline.addTarget(self, action: #selector(OrderOnlineClickMethod), for: .touchUpInside)
        self.btnOpenNow.addTarget(self, action: #selector(OpenNowClickMethod), for: .touchUpInside)
        self.btnCurbsidePickUp.addTarget(self, action: #selector(CurbsidePickUpClickMethod), for: .touchUpInside)
        self.btnSocialEquity.addTarget(self, action: #selector(SocialEquityClickMethod), for: .touchUpInside)
        self.btn19Only.addTarget(self, action: #selector(ninteenOnlyClickMethod), for: .touchUpInside)
        self.btn18Only.addTarget(self, action: #selector(eighteenOnlyClickMethod), for: .touchUpInside)
        self.btnATM.addTarget(self, action: #selector(ATMClickMethod), for: .touchUpInside)
        self.btnVideos.addTarget(self, action: #selector(VideosClickMethod), for: .touchUpInside)
        self.btnAccessible.addTarget(self, action: #selector(AccessibleClickMethod), for: .touchUpInside)
        self.btn21only.addTarget(self, action: #selector(twentyoneOnlyClickMethod), for: .touchUpInside)
        self.btnSecurity.addTarget(self, action: #selector(SecurityClickMethod), for: .touchUpInside)
        
        
        self.sendValueToWBforMultiplePinsonMap()
    }
    
    
    
    // MARK:- STORE FRONT -
    @objc func storeFrontClickMethod() {
        
        if self.btnStoreFronts.tag == 0 {
            
            self.btnStoreFronts.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnStoreFronts.tag = 1
            self.strStorefronts = "1"
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 3)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[3]),
                "status":"1"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 3)
            
        } else {
            
            self.btnStoreFronts.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnStoreFronts.tag = 0
            self.strStorefronts = "0"
            
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 3)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[3]),
                "status":"0"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 3)
            
        }
        
        self.clViewForStaticButton.reloadData()
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    
    // MARK:- DELIVERY -
    @objc func deliveryClickMethod() {
        
        if self.btnDelivery.tag == 0 {
            
            self.btnDelivery.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnDelivery.tag = 1
            self.strDelivery = "1"
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 4)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[4]),
                "status":"1"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 4)
            
        } else {
            
            self.btnDelivery.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnDelivery.tag = 0
            self.strDelivery = "0"
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 4)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[4]),
                "status":"0"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 4)
            
        }
        
        self.clViewForStaticButton.reloadData()
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    
    // MARK:- CBD -
    @objc func cbdStoreClickMethod() {
        
        if self.btnCBDStores.tag == 0 {
            
            self.btnCBDStores.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnCBDStores.tag = 1
            self.strCBDStore = "1"
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 5)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[5]),
                "status":"1"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 5)
            
        } else {
            
            self.btnCBDStores.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnCBDStores.tag = 0
            self.strCBDStore = "0"
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 5)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[5]),
                "status":"1"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 5)
            
        }
        
        self.clViewForStaticButton.reloadData()
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    
    // MARK:- RECREATIONAL -
    @objc func recreationalClickMethod() {
        
        if self.btnRecreation.tag == 0 {
            
            self.btnRecreation.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnRecreation.tag = 1
            self.strRecreational = "1"
            
        } else {
            
            self.btnRecreation.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnRecreation.tag = 0
            self.strRecreational = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    
    // MARK:- MEDICAL -
    @objc func medicalClickMethod() {
        
        if self.btnMedical.tag == 0 {
            
            self.btnMedical.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnMedical.tag = 1
            self.strMedical = "1"
            
        } else {
            
            self.btnMedical.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnMedical.tag = 0
            self.strMedical = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    
    
    // MARK:- ORDER ONLINE -
    @objc func OrderOnlineClickMethod() {
        
        if self.btnOrderOnline.tag == 0 {
            
            self.btnOrderOnline.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnOrderOnline.tag = 1
            self.strOrderOnline = "1"
            
            
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 0)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[0]),
                "status":"1"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 0)
            
            
            
            
        } else {
            
            self.btnOrderOnline.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnOrderOnline.tag = 0
            self.strOrderOnline = "0"
            
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 0)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[0]),
                "status":"0"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 0)
            
        }
        
        self.clViewForStaticButton.reloadData()
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- OPEN NOW -
    @objc func OpenNowClickMethod() {
        
        if self.btnOpenNow.tag == 0 {
            
            self.btnOpenNow.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnOpenNow.tag = 1
            self.strOpenNow = "1"
            
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 1)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[1]),
                "status":"1"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 1)
            
            
        } else {
            
            self.btnOpenNow.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnOpenNow.tag = 0
            self.strOpenNow = "0"
            
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 1)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[1]),
                "status":"0"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 1)
            
        }
        
        self.clViewForStaticButton.reloadData()
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- CURBSIDE PICKUP -
    @objc func CurbsidePickUpClickMethod() {
        
        if self.btnCurbsidePickUp.tag == 0 {
            
            self.btnCurbsidePickUp.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnCurbsidePickUp.tag = 1
            self.strCurbsidePickup = "1"
            
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 2)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[2]),
                "status":"1"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 2)
            
        } else {
            
            self.btnCurbsidePickUp.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnCurbsidePickUp.tag = 0
            self.strCurbsidePickup = "0"
            
            
            self.arrayShowUpperCollectionViewData.removeObject(at: 2)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[2]),
                "status":"0"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: 2)
            
        }
        
        self.clViewForStaticButton.reloadData()
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- SOCIAL EQUITY -
    @objc func SocialEquityClickMethod() {
        
        if self.btnSocialEquity.tag == 0 {
            
            self.btnSocialEquity.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnSocialEquity.tag = 1
            self.strSocialEquity = "1"
            
        } else {
            
            self.btnSocialEquity.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnSocialEquity.tag = 0
            self.strSocialEquity = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- 19 ONLY -
    @objc func ninteenOnlyClickMethod() {
        
        if self.btn19Only.tag == 0 {
            
            self.btn19Only.setImage(UIImage(named: checkImageName), for: .normal)
            self.btn19Only.tag = 1
            self.str19Only = "1"
            
        } else {
            
            self.btn19Only.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btn19Only.tag = 0
            self.str19Only = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- 18 ONLY -
    @objc func eighteenOnlyClickMethod() {
        
        if self.btn18Only.tag == 0 {
            
            self.btn18Only.setImage(UIImage(named: checkImageName), for: .normal)
            self.btn18Only.tag = 1
            self.str18Only = "1"
            
        } else {
            
            self.btn18Only.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btn18Only.tag = 0
            self.str18Only = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- ATM -
    @objc func ATMClickMethod() {
        
        if self.btnATM.tag == 0 {
            
            self.btnATM.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnATM.tag = 1
            self.strATM = "1"
            
        } else {
            
            self.btnATM.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnATM.tag = 0
            self.strATM = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- VIDEOS -
    @objc func VideosClickMethod() {
        
        if self.btnVideos.tag == 0 {
            
            self.btnVideos.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnVideos.tag = 1
            self.strVideos = "1"
            
        } else {
            
            self.btnVideos.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnVideos.tag = 0
            self.strVideos = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- ACCEESSIBLE -
    @objc func AccessibleClickMethod() {
        
        if self.btnAccessible.tag == 0 {
            
            self.btnAccessible.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnAccessible.tag = 1
            self.strAccessible = "1"
            
        } else {
            
            self.btnAccessible.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnAccessible.tag = 0
            self.strAccessible = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- 21 ONLY -
    @objc func twentyoneOnlyClickMethod() {
        
        if self.btn21only.tag == 0 {
            
            self.btn21only.setImage(UIImage(named: checkImageName), for: .normal)
            self.btn21only.tag = 1
            self.str21Only = "1"
            
        } else {
            
            self.btn21only.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btn21only.tag = 0
            self.str21Only = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    // MARK:- SECURITY -
    @objc func SecurityClickMethod() {
        
        if self.btnSecurity.tag == 0 {
            
            self.btnSecurity.setImage(UIImage(named: checkImageName), for: .normal)
            self.btnSecurity.tag = 1
            self.strSecurity = "1"
            
        } else {
            
            self.btnSecurity.setImage(UIImage(named: unCheckImageName), for: .normal)
            self.btnSecurity.tag = 0
            self.strSecurity = "0"
            
        }
        
        self.sendValueToWBforMultiplePinsonMap()
        
    }
    
    // MARK:- SEND VALUES TO WEBSERVIICE -
    @objc func sendValueToWBforMultiplePinsonMap() {
        
        self.allLocationWB(storefrontsParam: String(self.strStorefronts),
                           deliverParam: String(self.strDelivery),
                           doctorParam: String("0"),
                           recreationalParam: String(self.strRecreational),
                           medicalParam: String(self.strMedical),
                           cbdParam: String(self.strCBDStore),
                           orderOnlineParam: String(self.strOrderOnline),
                           curbsideParam: String(self.strCurbsidePickup),
                           socialEquity: String(self.strSocialEquity),
                           brandVerified: String("0"),
                           bestOfWeedMapParam: String("0"),
                           labtestedParam: String("0"),
                           atmParam: String(self.strATM),
                           videosParam: String(self.strVideos),
                           accessibleParam: String(self.strAccessible),
                           twentyOneParam: String(self.str21Only),
                           eighteenParam: String(self.str18Only),
                           nineteen: String(self.str19Only),
                           security: String(self.strSecurity))
        
    }
    
    func createMapView() {
       
            // google maps
        let camera = GMSCameraPosition.camera(withLatitude: 28.7041, longitude: 77.1025, zoom: 2.0)
        let mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height), camera: camera)
        mapView.delegate = self
        
        self.viewMapBG.addSubview(mapView)
        // mapView.clear()
        
        
        
        for locationCheckIndex in 0..<self.arrListOfAllLocations.count {
            
            let item = self.arrListOfAllLocations[locationCheckIndex] as? [String:Any]
            
            let lat = item!["latitude"]
            let myFloatLat = (lat as! NSString).doubleValue
            
            let long = item!["longitude"]
            let myFloatLong = (long as! NSString).doubleValue
            
            
            // Creates a marker in the center of the map.
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: myFloatLat, longitude: myFloatLong)
            marker.title = (item!["name"] as! String)
            marker.snippet = (item!["address"] as! String)
                 // marker.isTappable = true
            // marker.snippet = "Australia"
            marker.map = mapView
            
        }
            
        
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
     //Write your code here...
        print("clicked")
     }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("clicked 2")
        
        print(marker.title as Any)
        print(marker.snippet as Any)
        
        let alert = UIAlertController(title: String(marker.title!), message: String(marker.snippet!), preferredStyle: UIAlertController.Style.alert)
                       
        alert.addAction(UIAlertAction(title: "Open Profile", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            print("Action")
            
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DespensariesId")
            self.navigationController?.pushViewController(push, animated: true)
            
         }))
        
        alert.addAction(UIAlertAction(title: "Dimiss", style: UIAlertAction.Style.destructive, handler: {(action:UIAlertAction!) in
            print("Action")
            
         }))
                   
        self.present(alert, animated: true, completion: nil)
        
        return false
    }
     
    
    @objc func allLocationWB(storefrontsParam:String,
                             deliverParam:String,
                             doctorParam:String,
                             recreationalParam:String,
                             medicalParam:String,
                             cbdParam:String,
                             orderOnlineParam:String,
                             curbsideParam:String,
                             socialEquity:String,
                             brandVerified:String,
                             bestOfWeedMapParam:String,
                             labtestedParam:String,
                             atmParam:String,
                             videosParam:String,
                             accessibleParam:String,
                             twentyOneParam:String,
                             eighteenParam:String,
                             nineteen:String,
                             security:String) {
        
        
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "please wait...")
         
        self.view.endEditing(true)
        
        let params = ShowMultiplePinsOnMap(action: "storelist",
                                           Storefronts: String(storefrontsParam),
                                           Delivery: String(deliverParam),
                                           Doctors: String(doctorParam),
                                           Recreational: String(recreationalParam),
                                           Medical: String(medicalParam),
                                           CBDStores: String(cbdParam),
                                           OrderOnline: String(orderOnlineParam),
                                           CurbsidePickup: String(curbsideParam),
                                           SocialEquity: String(socialEquity),
                                           BrandVerified: String("0"),
                                           BestofWeedmaps: String("0"),
                                           Labtested: String("0"),
                                           ATM: String(atmParam),
                                           Videos: String(videosParam),
                                           AccessibleD: String(accessibleParam),
                                           twentyone: String(twentyOneParam),
                                           Security: String(security),
                                           eighteen: String(eighteenParam),
                                           ninteen: String(nineteen))
        
            print(params as Any)
            
            AF.request(BASE_URL_ALIEN_BROCCOLI,
                       method: .post,
                       parameters: params,
                       encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                        switch response.result {
                        case let .success(value):
                            
                            let JSON = value as! NSDictionary
                            print(JSON as Any)
                            
                            var strSuccess : String!
                            strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                            if strSuccess == String("success") {
                                print("yes")
                                 ERProgressHud.sharedInstance.hide()
                               
                                
                                ERProgressHud.sharedInstance.hide()
                                   
                                var ar : NSArray!
                                ar = (JSON["data"] as! Array<Any>) as NSArray
                                self.arrListOfAllLocations.addObjects(from: ar as! [Any])
                                
                                self.createMapView()
                                
                            } else {
                                print("no")
                                ERProgressHud.sharedInstance.hide()
                                
                                var strSuccess2 : String!
                                strSuccess2 = JSON["msg"]as Any as? String
                                
                                Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                            }
                            
                        case let .failure(error):
                            print(error)
                            ERProgressHud.sharedInstance.hide()
                            
                            Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                        }
                }
            
         
    }
    
    
}


// MARK:- COLLECTION VIEW -
extension MAPS: UICollectionViewDelegate {
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.staticSixArray.count
            
    }
    
    //Write Delegate Code Here
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mAPSCollectionViewCell", for: indexPath as IndexPath) as! MAPSCollectionViewCell
            
        /*
         self.strOrderOnlineText = "0"
         self.strOpenNow = "0"
         self.strCurbsidePickup = "0"
         self.strStoreFronts = "0"
         self.strDelivery = "0"
         self.strCBDStores = "0"
         */
        
        let item = self.arrayShowUpperCollectionViewData[indexPath.row] as? [String:Any]
        cell.lblTitle.text = (item!["name"] as! String)
        
        
        
        if (item!["status"] as! String) == "0" {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 2
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        } else {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = UIColor.black.cgColor
            
        }
        
        /*cell.lblTitle.text = self.staticSixArray[indexPath.row]
        
        if self.strOrderOnlineText == "0" {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        } else {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        }
        
        if self.strOpenNow == "0" {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        } else {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        }
        
        if self.strCurbsidePickup == "0" {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        } else {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        }
        
        if self.strStoreFronts == "0" {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        } else {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        }
        
        if self.strDelivery == "0" {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        } else {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        }
        
        if self.strCBDStores == "0" {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        } else {
            
            cell.lblTitle.textColor = .black
            cell.backgroundColor = NAVIGATION_BACKGROUND_COLOR
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.layer.borderWidth = 1
            cell.layer.borderColor = NAVIGATION_BACKGROUND_COLOR.cgColor
            
        }*/
        
        return cell
          
    }
}

extension MAPS: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("clicked")
        
        self.viewFilterBG.isHidden = false
        
        
        let item = self.arrayShowUpperCollectionViewData[indexPath.row] as? [String:Any]
        
        if (item!["status"] as! String) == "0" {
            
            self.arrayShowUpperCollectionViewData.removeObject(at: indexPath.row)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[indexPath.row]),
                "status":"1"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: indexPath.row)
            
            print(self.arrayShowUpperCollectionViewData as Any)
            
        } else {
            
            self.arrayShowUpperCollectionViewData.removeObject(at: indexPath.row)
            
            let myDictionary: [String:String] = [
                
                "name":String(staticSixArray[indexPath.row]),
                "status":"0"
                
            ]
            
            self.arrayShowUpperCollectionViewData.insert(myDictionary, at: indexPath.row)
            
            print(self.arrayShowUpperCollectionViewData as Any)
            
        }
        
        self.clViewForStaticButton.reloadData()
        
        /*
         self.strOrderOnlineText = "0"
         self.strOpenNow = "0"
         self.strCurbsidePickup = "0"
         self.strStoreFronts = "0"
         self.strDelivery = "0"
         self.strCBDStores = "0"
         */
        
        /*if self.staticSixArray[indexPath.row] == "Order Online" {
            self.strOrderOnlineText = "1"
            
            self.clViewForStaticButton.reloadData()
            
        }*/
            
        /*if self.staticSixArray[indexPath.row] == "Dispensaries" {
                
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MAPSId")
            self.navigationController?.pushViewController(push, animated: true)
        }*/
            
        
        
        
    }
    
}

extension MAPS: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
      
        var sizes: CGSize
        sizes = CGSize(width: 100, height: 40)
        return sizes
            
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
//            let result = UIScreen.main.bounds.size
//            if result.height == 667.000000 { // 8
//                return 2
//            } else if result.height == 812.000000 { // 11 pro
                return 4
//            } else {
//                return 10
//            }
            
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        
    }
   
}
