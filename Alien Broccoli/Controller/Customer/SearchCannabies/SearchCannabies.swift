//
//  SearchCannabies.swift
//  Alien Broccoli
//
//  Created by Apple on 29/12/20.
//

import UIKit
import GoogleMaps
import Alamofire

class SearchCannabies: UIViewController, GMSMapViewDelegate {

    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    @IBOutlet weak var viewBGOne:UIView! {
        didSet {
            viewBGOne.backgroundColor = .black
            viewBGOne.layer.cornerRadius = 16
            viewBGOne.clipsToBounds = true
        }
    }
    @IBOutlet weak var btnSelectViaProduct:UIButton! {
        didSet {
            btnSelectViaProduct.setTitle("Product, retailer, brands and more ", for: .normal)
            btnSelectViaProduct.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnSearchCannibis:UIButton! {
        didSet {
            btnSearchCannibis.layer.cornerRadius = 26
            btnSearchCannibis.clipsToBounds = true
            btnSearchCannibis.backgroundColor = .systemOrange
            btnSearchCannibis.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var btnSMyCurrentLocation:UIButton! {
        didSet {
            btnSMyCurrentLocation.setTitle("My Current Location", for: .normal)
            btnSMyCurrentLocation.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var viewBGTwo:UIView! {
        didSet {
            viewBGTwo.layer.cornerRadius = 16
            viewBGTwo.clipsToBounds = true
        }
    }
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "CANNABIS"
            lblNavigationTitle.textColor = .black
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnSearchCannibis.addTarget(self, action: #selector(searchCannabies), for: .touchUpInside)
        
        self.allLocationWB()
    }
    
    @objc func searchCannabies() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AfterSearchResultId") as? AfterSearchResult
        self.navigationController?.pushViewController(push!, animated: true)
    }

    @IBOutlet weak var btnMenu:UIButton! {
        didSet {
            btnMenu.tintColor = .white
        }
    }
    
    // MARK:- WEBSERVICE - ( ALL LOCATIONS ) -
    @objc func allLocationWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "fetching...")
            
        self.view.endEditing(true)
        
                
        let params = Locations(action: "location")
            
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                        // debugPrint(response.result)
                        
                    switch response.result {
                    case let .success(value):
                            
                        let JSON = value as! NSDictionary
                        // print(JSON as Any)
                            
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                            
                            // var strSuccess2 : String!
                            // strSuccess2 = JSON["msg"]as Any as? String
                            
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                               
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            self.createMapView()
 
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                                
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                                
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                                
                        }
                            
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                            
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
            
    }
    
    func createMapView() {
       
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
            // my default lat long
            let latDef = person["latitude"]
            let myFloatLatDef = (latDef as! NSString).doubleValue
        
            let longDef = person["longitude"]
            let myFloatLongDef = (longDef as! NSString).doubleValue
            
            // google maps
            let camera = GMSCameraPosition.camera(withLatitude: myFloatLatDef, longitude: myFloatLongDef, zoom: 2.0)
            let mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.viewBGTwo.frame.size.width, height: self.viewBGTwo.frame.size.height), camera: camera)
            mapView.delegate = self
            // self.view.addSubview(mapView)
            self.viewBGTwo.addSubview(mapView)
        
            for locationCheckIndex in 0..<arrListOfAllMyOrders.count {
            
                let item = self.arrListOfAllMyOrders[locationCheckIndex] as? [String:Any]
            
            // test lat : -33.86
            // test long : 151.20
            
                let lat = item!["latitude"]
                let myFloatLat = (lat as! NSString).doubleValue
            
                let long = item!["longitude"]
                let myFloatLong = (long as! NSString).doubleValue
            
            
            // Creates a marker in the center of the map.
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: myFloatLat, longitude: myFloatLong)
                marker.title = (item!["location"] as! String)
            
            // marker.snippet = "Australia"
                marker.map = mapView
            
            }
            
        }
        
    }
    
}
