//
//  CannabiesProducts.swift
//  Alien Broccoli
//
//  Created by apple on 14/06/21.
//

import UIKit
import Alamofire

class CannabiesProducts: UIViewController , UIScrollViewDelegate {
    
    var dashboardTitle = ["CBD Oil","CBD Gummies","CBD Capsules","CBD Topicals","CBD Candy","CBD Edibles"]
    var dashboardImage = ["1","2","3","4","5","6"]
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var headerName:String!
    var brandIdIs:String!
    var dictGetCanabbiesItemDetails:NSDictionary!
    
    @IBOutlet weak var lblCartCount:UILabel! {
        didSet {
            self.lblCartCount.isHidden = true
        }
    }
    
    @IBOutlet weak var indicatorr:UIActivityIndicatorView! {
        didSet {
            indicatorr.color = .white
        }
    }
    
    // MARK:- CUSTOM NAVIGATION BAR -
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    // MARK:- CUSTOM NAVIGATION TITLE -
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "SHOP NOW"
        }
    }
    
    // MARK:- COLLECTION VIEW SETUP -
    @IBOutlet weak var clView: UICollectionView! {
        didSet {
               //collection
            // clView!.dataSource = self
            // clView!.delegate = self
            clView!.backgroundColor = .white
            clView.isPagingEnabled = false
        }
    }
    
    @IBOutlet weak var btnMenu:UIButton! {
        didSet {
            btnMenu.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnCart:UIButton! {
        didSet {
            btnCart.backgroundColor = .clear
            btnCart.tintColor = .black
            btnCart.isHidden = true
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnMenu.addTarget(self, action: #selector(sidebackBarMenuClick), for: .touchUpInside)
        self.btnCart.addTarget(self, action: #selector(cartClickMwthod), for: .touchUpInside)
        
        self.gradientNavigation()
        
        // print(self.dictGetCanabbiesItemDetails as Any)
        
        // self.sideBarMenuClick()
        
        self.lblNavigationTitle.text = self.headerName
        self.lblNavigationTitle.textColor = .black
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.ProductListWB()
        
    }
    
    
    
    // MARK:- GRADIENT ANIMATOR -
    @objc func gradientNavigation() {
        
        let gradientView = GradientAnimator(frame: navigationBar.frame, theme: .NeonLife, _startPoint: GradientPoints.bottomLeft, _endPoint: GradientPoints.topRight, _animationDuration: 3.0)
        navigationBar.insertSubview(gradientView, at: 0)
        gradientView.startAnimate()
        
    }
    
    @objc func cartClickMwthod() {
        
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CTotalCartListId") as? CTotalCartList
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
    @objc func sidebackBarMenuClick() {
        
        self.navigationController?.popViewController(animated: true)
        
        /*
        if revealViewController() != nil {
            btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
               
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        */
    }
    
}


extension CannabiesProducts: UICollectionViewDelegate {
    
    //UICollectionViewDatasource methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
         return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    //Write Delegate Code Here
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cannabiesCollectionCell", for: indexPath as IndexPath) as! CannabiesCollectionCell
           
        /*
         SubCat =             (
         );
         id = 1;
         image = "";
         name = "CBD OIL";
         */
        
        // MARK:- CELL CLASS -
        cell.layer.cornerRadius = 6
        cell.clipsToBounds = true
        cell.backgroundColor = .white
        cell.layer.borderColor = UIColor.clear.cgColor
        cell.layer.borderWidth = 0.6
        
        // cell.viewCellBG.backgroundColor = .systemGreen // APP_BUTTON_COLOR
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        print(item as Any)
        
        if self.headerName == "Feature Brand" {
            
            cell.lblTitle.text  = (item!["productName"] as! String)
            cell.imgProfile.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "logo"))
            
            if (item!["followers"] as! String) == "0" {
                cell.lblSubTitle.text = "0"+" follower"
            } else if (item!["followers"] as! String) == "1" {
                cell.lblSubTitle.text = "1"+" follower"
            } else if (item!["followers"] as! String) == "" {
                cell.lblSubTitle.text = "0"+" follower"
            } else {
                cell.lblSubTitle.text = (item!["followers"] as! String)+" followers"
            }
        
            // cell.lblSubTitle.text = "0 follower"
            cell.lblSubTitle.textAlignment = .left
            cell.lblSubTitle.font = UIFont.init(name: "Avenir Next Regular", size: 12)
            
            cell.viewBlackBG.backgroundColor = .white
            
            cell.lblTitle.textColor = .black
            cell.lblSubTitle.textColor = .lightGray
            
        } else if self.headerName == "Storefronts" {
            
            cell.lblTitle.text  = (item!["productName"] as! String)
            cell.imgProfile.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "logo"))
            
            
            if item!["price"] is String {
                              
                print("Yes, it's a String")
              
                cell.lblSubTitle.text = "$ "+(item!["price"] as! String)

            } else if item!["totalAmount"] is Int {
              
                print("It is Integer")
              
                let x2 : Int = (item!["price"] as! Int)
                let myString2 = String(x2)
                cell.lblSubTitle.text = "$ "+myString2
              
            } else {
            //some other check
              print("i am ")
              
                let temp:NSNumber = item!["price"] as! NSNumber
                let tempString = temp.stringValue
                cell.lblSubTitle.text = "$ "+tempString
              
            }
            
            cell.lblTitle.textColor = .lightGray
           
            cell.lblSubTitle.font = UIFont.init(name: "Avenir Next Bold", size: 22)
            cell.lblSubTitle.textColor = .black
            cell.lblSubTitle.textAlignment = .center
            
            cell.viewBlackBG.backgroundColor = .white
            cell.viewBlackBG.layer.borderWidth = 0.8
            cell.viewBlackBG.layer.borderColor = UIColor.white.cgColor
            
        } else if self.headerName == "CBD Stores" {
            
            cell.lblTitle.text = "California | 7 min"//(item!["address"] as! String)
            cell.lblTitle.textColor = .lightGray
            cell.lblTitle.textAlignment = .left
            cell.lblTitle.backgroundColor = .clear
            cell.lblTitle.font = UIFont.init(name: "Avenir Next Regular", size: 12)
            
            cell.lblSubTitle.text = (item!["productName"] as! String) // "Recreational"
            cell.lblSubTitle.font = UIFont.init(name: "Avenir Next Bold", size: 16)
            cell.lblSubTitle.textColor = .black
            cell.lblSubTitle.textAlignment = .left
            
            cell.lblCategory.text = (item!["cateroryName"] as! String)
            cell.lblCategory.textAlignment = .left
            cell.lblCategory.textColor = .lightGray
            cell.lblCategory.font = UIFont.init(name: "Avenir Next Regular", size: 12)
            
            if (item!["rating"] as! String) == "0" {
                
                cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                
            } else if (item!["rating"] as! String) == "1" {
                
                cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                
            } else if (item!["rating"] as! String) == "2" {
                
                cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                
            } else if (item!["rating"] as! String) == "3" {
                
                cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                
            } else if (item!["rating"] as! String) == "4" {
                
                cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                
            } else if (item!["rating"] as! String) == "5" {
                
                cell.btnStarOne.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarTwo.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarThree.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarFour.setImage(UIImage(systemName: "star.fill"), for: .normal)
                cell.btnStarFive.setImage(UIImage(systemName: "star.fill"), for: .normal)
                
            } else {
                
                cell.btnStarOne.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarTwo.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarThree.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarFour.setImage(UIImage(systemName: "star"), for: .normal)
                cell.btnStarFive.setImage(UIImage(systemName: "star"), for: .normal)
                
            }
            
            cell.viewBlackBG.backgroundColor = .white
            cell.viewBlackBG.layer.borderWidth = 0.8
            cell.viewBlackBG.layer.borderColor = UIColor.white.cgColor
            
        } else if self.headerName == "Deals Nearby" {
            
            cell.lblTitle.text  = (item!["productName"] as! String)
            cell.imgProfile.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "logo"))
            
            
            if item!["price"] is String {
                              
                print("Yes, it's a String")
              
                cell.lblSubTitle.text = "$ "+(item!["price"] as! String)

            } else if item!["price"] is Int {
              
                print("It is Integer")
              
                let x2 : Int = (item!["price"] as! Int)
                let myString2 = String(x2)
                cell.lblSubTitle.text = "$ "+myString2
              
            } else {
            //some other check
              print("i am ")
              
                let temp:NSNumber = item!["price"] as! NSNumber
                let tempString = temp.stringValue
                cell.lblSubTitle.text = "$ "+tempString
              
            }
            
            cell.lblTitle.font = UIFont.init(name: "Avenir Next Regular", size: 14)
            cell.lblTitle.textColor = .lightGray
           
            cell.lblSubTitle.font = UIFont.init(name: "Avenir Next Bold", size: 22)
            cell.lblSubTitle.textColor = .black
            cell.lblSubTitle.textAlignment = .center
            
            
            
        }
        
        cell.viewBlackBG.backgroundColor = .white
        cell.viewBlackBG.layer.borderWidth = 0.8
        cell.viewBlackBG.layer.borderColor = UIColor.white.cgColor
        
        return cell
    }
    
    // MARK:- WEBSERVICE ( CATEGORIES ) -
    @objc func ProductListWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        
        /*let x : Int = (dictGetCanabbiesItemDetails["id"] as! Int)
        let myString = String(x)*/
        
        let params = BrandList(action: "productlist",
                               brandId: String(self.brandIdIs))
        print(params as Any)
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            // ERProgressHud.sharedInstance.hide()
                           
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            self.clView.delegate = self
                            self.clView.dataSource = self
                            self.clView.reloadData()
                            
                            self.totalItemsInCart()
                            
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
        
    }
    
    
    
    // MARK:- WEBSERVICE ( TOTAL ITEMS IN CART ) -
    @objc func totalItemsInCart() {
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
            
        let params = CartList(action: "getcarts",
                              userId: String(myString))
        
        AF.request(BASE_URL_ALIEN_BROCCOLI,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            // var ar : NSArray!
                            // ar = (JSON["data"] as! Array<Any>) as NSArray
                            // self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // var strCartCount : String!
                            // strCartCount = JSON["TotalCartItem"]as Any as? String
                            // print(strCartCount as Any)
                            
                            // cart
                            
                            let x : Int = JSON["TotalCartItem"] as! Int
                            let myString = String(x)
                            
                            if myString == "0" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else if myString == "" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else {
                                self.lblCartCount.isHidden = false
                                self.lblCartCount.text = String(myString)
                                
                                self.btnCart.isHidden = false
                                self.btnCart.setImage(UIImage(systemName: "cart.fill"), for: .normal)
                            }
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            self.clView.delegate = self
                            self.clView.dataSource = self
                            self.clView.reloadData()
                            
                            /*
                            if JSON["TotalCartItem"] is String {
                                print("Yes, it's a String")

                                self.lblCartCount.text = ""

                            } else if JSON["TotalCartItem"] is Int {
                                print("It is Integer")
                                            
                                let x2 : Int = (JSON["TotalCartItem"] as! Int)
                                let myString2 = String(x2)
                                self.lblCartCount.text = myString2
                                            
                            } else {
                                print("i am number")
                                            
                                let temp:NSNumber = JSON["TotalCartItem"] as! NSNumber
                                let tempString = temp.stringValue
                                self.lblCartCount.text = tempString
                            }
                            */
                            
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        self.indicatorr.stopAnimating()
                        self.indicatorr.hidesWhenStopped = true
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
         }
    }
    
}

extension CannabiesProducts: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        // print(item as Any)
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CSelectedOrderDetailsId") as? CSelectedOrderDetails
        push!.dictGetProductDetails = item as NSDictionary?
        push!.productCategoryName = (item!["productName"] as! String)
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- DASHBOARD PUSH -
    @objc func ordersListingPage() {
         let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "COrderDetailsId") as? COrderDetails
         self.navigationController?.pushViewController(push!, animated: true)
    }
    
    // MARK:- DISMISS KEYBOARD -
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.clView {
            self.view.endEditing(true)
        }
    }
    
}

extension CannabiesProducts: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var sizes: CGSize
                    
        if self.headerName == "CBD Stores" {
            sizes = CGSize(width: 190, height: 320)
        } else {
            sizes = CGSize(width: 190, height: 252)
        }
        
        
        return sizes
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        let result = UIScreen.main.bounds.size
        if result.height == 667.000000 { // 8
            return 2
        } else if result.height == 812.000000 { // 11 pro
            return 4
        } else {
            return 10
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        // var sizes: CGSize
                
        /*let result = UIScreen.main.bounds.size
        if result.height == 667.000000 { // 8
            return UIEdgeInsets(top: 20, left: 4, bottom: 10, right: 4)
        } else if result.height == 736.000000 { // 8 plus
            return UIEdgeInsets(top: 2, left: 10, bottom: 10, right: 20)
        } else if result.height == 896.000000 { // 11 plus
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 20)
        } else if result.height == 812.000000 { // 11 pro
            return UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
        } else {*/
            return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        // }
        
        
            
    }
    
}
