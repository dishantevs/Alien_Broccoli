//
//  CUploadDocumentsRegTableCell.swift
//  Alien Broccoli
//
//  Created by Apple on 09/11/20.
//

import UIKit

class CUploadDocumentsRegTableCell: UITableViewCell {

    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var viewCellBG2:UIView! {
        didSet {
            viewCellBG2.layer.cornerRadius = 8
            viewCellBG2.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var viewCellBG3:UIView! {
        didSet {
            viewCellBG3.layer.cornerRadius = 8
            viewCellBG3.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel! {
        didSet {
            lblTitle.textColor = .white
            lblTitle.text = "Photo Id"
        }
    }
    
    @IBOutlet weak var lblTitle2:UILabel! {
        didSet {
            lblTitle2.textColor = .white
            lblTitle2.text = "Address Proof"
        }
    }
    
    @IBOutlet weak var lblTitle3:UILabel! {
        didSet {
            lblTitle3.textColor = .white
            lblTitle3.text = "Face Picture Id"
        }
    }
    
    @IBOutlet weak var imgUploadImage:UIImageView!
    @IBOutlet weak var imgUploadImage2:UIImageView!
    @IBOutlet weak var imgUploadImage3:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
