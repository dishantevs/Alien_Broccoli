//
//  CAfterRegistration.swift
//  Alien Broccoli
//
//  Created by Apple on 09/11/20.
//

import UIKit

// MARK:- LOCATION -
import CoreLocation

class CAfterRegistration: UIViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_BACKGROUND_COLOR
        }
    }

    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "REGISTRATION"
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            // btnBack.setTitle("|||", for: .normal)
        }
    }
    @IBOutlet weak var txtCity:UITextField! {
        didSet {
            txtCity.layer.cornerRadius = 6
            txtCity.clipsToBounds = true
            txtCity.setLeftPaddingPoints(40)
            txtCity.layer.borderColor = UIColor.clear.cgColor
            txtCity.layer.borderWidth = 0.8
        }
    }
    @IBOutlet weak var txtZipcode:UITextField! {
        didSet {
            txtZipcode.layer.cornerRadius = 6
            txtZipcode.clipsToBounds = true
            txtZipcode.setLeftPaddingPoints(40)
            txtZipcode.layer.borderColor = UIColor.clear.cgColor
            txtZipcode.layer.borderWidth = 0.8
            txtZipcode.keyboardType = .emailAddress
        }
    }
    @IBOutlet weak var btnSaveAndContinue:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        self.btnSaveAndContinue.addTarget(self, action: #selector(btnSaveAndContinueClickMethod), for: .touchUpInside)
        
        self.txtCity.text = ""
        self.txtZipcode.text = ""
        
        self.iAmHereForLocationPermission()
    }

    @objc func btnSaveAndContinueClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CUploadDocumentsRegId") as? CUploadDocumentsReg
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        // let indexPath = IndexPath.init(row: 0, section: 0)
        // let cell = self.tbleView.cellForRow(at: indexPath) as! PDBagPurchaseTableCell
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
           
            self.txtCity.text = String(self.strSaveStateName)
            self.txtZipcode.text = String(self.strSaveZipcodeName)
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
        }
    }
    
}
