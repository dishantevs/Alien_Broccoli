
//

import UIKit
import CoreLocation

// MARK:- BASE URL -
let BASE_URL_ALIEN_BROCCOLI = "http://demo2.evirtualservices.com/HoneyBudz/site/services/index"

let APP_BUTTON_COLOR = UIColor.init(red: 138.0/255.0, green: 194.0/255.0, blue: 87.0/255.0, alpha: 1)


let NAVIGATION_BACKGROUND_COLOR = UIColor.init(red: 120.0/255.0, green: 210.0/255.0, blue: 73.0/255.0, alpha: 1)



let ALERT_MESSAGE       = "Alert!"


// SERVER ISSUE
let SERVER_ISSUE_TITLE          = "Server Issue."
let SERVER_ISSUE_MESSAGE        = "Server Not Responding. Please check your Internet Connection."
let SERVER_ISSUE_MESSAGE_TWO    = "Please contact to Server Admin."



class Utils: NSObject {
  

    class func showAlert(alerttitle :String, alertmessage: String,ButtonTitle: String, viewController: UIViewController) {
        
        let alertController = UIAlertController(title: alerttitle, message: alertmessage, preferredStyle: .alert)
        let okButtonOnAlertAction = UIAlertAction(title: ButtonTitle, style: .default)
        { (action) -> Void in
            //what happens when "ok" is pressed
            
        }
        alertController.addAction(okButtonOnAlertAction)
        alertController.show(viewController, sender: self)
        
    }
    
// MARK:- UI -
    
    // MARK:- LOGIN WITH -

    class func GET_VIEW_CONTROLLER(identifier:NSString)-> UIViewController {
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier as String) as UIViewController
        return viewController
    }
    
    // button
    class func loginWithButtonUI(button:UIButton,text:String,backgroundColor:UIColor,textColor:UIColor,cornerRadius:Int) {
        button.setTitleColor(textColor, for: .normal)
        button.clipsToBounds = true
        button.backgroundColor = backgroundColor
        button.layer.shadowColor = UIColor.clear.cgColor
        button.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 2.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = CGFloat(cornerRadius)
        button.setTitle(text, for: .normal)
    }
    
    
    // MARK:- REGISTRATION SCREEN UI -
    class func textFieldGloballyUI(textfield:UITextField,placeholder:String,backgroundColor:UIColor,bottomLineColor:UIColor) {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: textfield.frame.height - 1, width: textfield.frame.width, height: 1)
        bottomLine.backgroundColor = bottomLineColor.cgColor
        textfield.borderStyle = UITextField.BorderStyle.none
        textfield.layer.addSublayer(bottomLine)
        textfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: textfield.frame.height))
        textfield.placeholder = placeholder
        textfield.leftViewMode = .always
    }
}


extension CLLocation {
    func fetchCityAndCountry(completion: @escaping (_ city: String?,_ country: String?, _ zipcode:  String?, _ localAddress:  String?, _ locality:  String?, _ subLocality:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality,$0?.first?.country, $0?.first?.postalCode,$0?.first?.subAdministrativeArea,$0?.first?.locality,$0?.first?.subLocality, $1) }
    }
    
    func countryCode(completion: @escaping (_ countryCodeIs:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.isoCountryCode, $1) }
    }
    
    func fullAddressFull(completion: @escaping (_ city: String?,_ country: String?, _ zipcode:  String?, _ localAddress:  String?, _ locality:  String?, _ subLocality:  String?,_ countryCodeIs:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality,$0?.first?.country, $0?.first?.postalCode,$0?.first?.administrativeArea,$0?.first?.locality,$0?.first?.subLocality,$0?.first?.isoCountryCode, $1) }
    }
}

/*
 {

     @IBAction func didStringsPicker() {
         
         let values = ["Value 1", "Value 2", "Value 3", "Value 4"]
         DPPickerManager.shared.showPicker(title: "Strings Picker", selected: "Value 1", strings: values) { (value, index, cancel) in
             if !cancel {
                 // TODO: you code here
                 debugPrint(value as Any)
             }
         }
         
     }
     
     @IBAction func didDatePicker() {
         let min = Date()
         let max = min.addingTimeInterval(31536000) // 1 year
         DPPickerManager.shared.showPicker(title: "Date Picker", selected: Date(), min: min, max: max) { (date, cancel) in
             if !cancel {
                 // TODO: you code here
                 debugPrint(date as Any)
             }
         }
     }
     
     @IBAction func didTimePicker() {
         DPPickerManager.shared.showPicker(title: "Time Picker", picker: { (picker) in
             picker.date = Date()
             picker.datePickerMode = .time
         }) { (date, cancel) in
             if !cancel {
                 // TODO: you code here
                 debugPrint(date as Any)
             }
         }
     }

 }
 */
