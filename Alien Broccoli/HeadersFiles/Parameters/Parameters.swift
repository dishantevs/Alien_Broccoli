//
//  Parameters.swift
//  KamCash
//
//  Created by Apple on 16/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit





// MARK:- LOGIN PARAMS -
struct LoginParam: Encodable {
    let action: String
    let email: String
    let password: String
    let deviceToken: String
    let device: String
}


// MARK:- USER -
struct ShopNowParam: Encodable {
    let action: String
}

struct GamesWB: Encodable {
    let action: String
}

struct CannabisWB: Encodable {
    let action: String
}


struct HelpWB: Encodable {
    let action: String
}

// product details
struct ShopProductDetails: Encodable {
    let action: String
    let categoryId: String
    let keyword: String
}

// address list
struct AddressList: Encodable {
    let action: String
    let userId: String
    
}

// carts
struct CartList: Encodable {
    let action: String
    let userId: String
    
}



// multiple annotation pins on map
struct ShowMultiplePinsOnMap: Encodable {
    let action: String
    let Storefronts: String
    let Delivery: String
    let Doctors: String
    let Recreational: String
    let Medical: String
    let CBDStores: String
    let OrderOnline: String
    let CurbsidePickup: String
    let SocialEquity: String
    let BrandVerified: String
    let BestofWeedmaps: String
    let Labtested: String
    let ATM: String
    let Videos: String
    let AccessibleD: String
    let twentyone: String
    let Security: String
    let eighteen: String
    let ninteen: String
    
}


struct DeliveriesData: Encodable {
    let action: String
    let Delivery: String
    let Medical: String
    let OrderOnline: String
}


// carts
struct BrandList: Encodable {
    let action: String
    let brandId: String
    
}

// all brands
struct AllBrandList: Encodable {
    let action: String
    // let userId: String
}

// all brands
struct OnlyBrands: Encodable {
    let action: String
    
}

// shop of that brand
struct ShopOfBrand: Encodable {
    let action: String
    let brandId: String
}

struct OnlyStoresList: Encodable {
    let action: String
    
}

struct StoreDetails: Encodable {
    let action: String
    let storeId:String
}

struct ViewMoreSection: Encodable {
    let action: String
    let Storefronts:String
    let CBDStores:String
    let deals:String
}

/*
 action: productlist
 storeId:
 deal:  //1= yes, 2=No
 pageNo:
 */

struct DispensariesStoresProduct: Encodable {
    let action: String
    let storeId:String
    let deal:String
    let pageNo:String
}

// add to cart
 struct AddToCart: Encodable {
    let action: String
    let userId: String
    let quantity: String
    let productId: String
 }

// delete cart
struct DeleteCart: Encodable {
   let action: String
   let userId: String
   let productId: String
}

// category list
struct CategoryList: Encodable {
   let action: String
}

// get product from product id and category id
struct CategoryListViaBrand: Encodable {
    let action: String
    let categoryId: String
    let brandId: String
}


// follow brand
struct BrandFollow: Encodable {
    let action: String
    let userId: String
    let brandId: String
    let status: String
}




// make payment
struct MakePaymentOfOrders: Encodable {
    let action: String
    let userId: String
    let productDetails: String
    let totalAmount: String
    let ShippingName: String
    let ShippingAddress: String
    let ShippingCity: String
    let ShippingState: String
    let ShippingZipcode: String
    let ShippingPhone: String
    let transactionId: String
    let latitude: String
    let longitude: String
    let cardType: String
}

// order history
struct OrderHistory: Encodable {
   let action: String
   let userId: String
}

// order history details
struct OrderHistoryDetails: Encodable {
   let action: String
   let purcheseId: String
}

// clear all cart
struct DeleteAllCartItems: Encodable {
   let action: String
   let userId: String
}

// add new address
struct AddNewAddress: Encodable {
    let action: String
    let userId: String
    let firstName: String
    let lastName: String
    let mobile: String
    let address: String
    let addressLine2: String
    let City: String
    let state: String
    let country: String
    let Zipcode: String
    let deliveryType: String
}

// edit address
struct EditAddress: Encodable {
    let action: String
    let userId: String
    let addressId: String
    let firstName: String
    let lastName: String
    let mobile: String
    let address: String
    let addressLine2: String
    let City: String
    let state: String
    let country: String
    let Zipcode: String
    let deliveryType: String
}

// delete this address
struct DeleteThisAddress: Encodable {
    let action: String
    let userId: String
    let addressId: String
}

// edit user without image
struct EditUserWithoutImage: Encodable {
    let action: String
    let userId: String
    let fullName: String
    let contactNumber: String
    let address: String
}

// edit user without image
struct EditUserWithImage: Encodable {
    let action: String
    let userId: String
    let fullName: String
    let contactNumber: String
    let address: String
}

// change password
struct ChangePasswordW: Encodable {
    let action: String
    let userId: String
    let oldPassword: String
    let newPassword: String
}

// locations
struct Locations: Encodable {
    let action: String
}



// upload documen
struct UploadDocumentPhotoId: Encodable {
    let action: String
    let userId: String
}

// DRIVER
struct DriverPuchaseList: Encodable {
    let action: String
    let userId: String
    let userType: String
    let status: String
}

// DRIVER success fully delivered
struct SuccessfullyDelivered: Encodable {
    let action: String
    let driverId: String
    let bookingId: String
    let status: String
    let message: String
}

// DRIVER booking history
struct BookingHistory: Encodable {
    let action: String
    let userId: String
    let userType: String
    let status: String
}


// driver registration order search
struct DriverRegistration: Encodable {
    let action: String
    let fullName: String
    let email: String
    let password: String
    let contactNumber: String
    let latitude: String
    let longitude: String
    let address: String
    let device: String
    let deviceToken: String
    let role: String
}


// customer order search
struct CustomerSearch: Encodable {
    let action: String
    let categoryId: String
    let keyword: String
}


// logout
struct LogoutFromApp: Encodable {
    let action: String
    let userId: String
}

class Parameters: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
